import React from 'react'
import { Col, Row } from 'antd'
import ScenarioComponent from './step4-scenario/ScenarioComponent'
import { getImageResources } from '../../utils/function-utils'
import BackgroundFilledContainer from '../common/BackgroundFilledContainer'

const Step4 = () => {
  const backGroundImageUrl = getImageResources('rawpixel-com-580218-unsplash')

  return (
    <div className="scenario-main-container">
      <BackgroundFilledContainer Content={ScenarioComponent}/>
    </div>
  )
}

export default Step4

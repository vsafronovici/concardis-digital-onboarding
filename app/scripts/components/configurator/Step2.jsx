import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Button, Row, Col } from 'antd'

import OptionCard from '../common/OptionCard'
import {
  cardOptionValueSelector,
  productsSelector,
  step2MetaSelector,
  step2SummarySelector
} from '../../selectors/configurator-selector'
import { changeFieldValue, goToStep } from '../../actions/configurator-action'
import { ConfiguratorPageStep } from '../../utils/constants'
import { Loader } from '../Loader'
import { translate } from './../../i18n/i18n'
import { valuesFormatter } from './../../transformers/configurator-transformer'
import { isInternalUser } from '../../utils/vf-utils'
import Sidebar from '../common/SIdebar'
import { creditCardDevider, checkInternalUserGrid } from '../../utils/function-utils'
import { routeSelector } from '../../selectors/router-selectors'
import { StaticIcon } from "../common/Icon";

export class Step2 extends React.Component {
  onChooseOption = id => {
    this.props.changeFieldValue({ name: 'cardOption', value: id, step: ConfiguratorPageStep.STEP2 })
  }

  render() {
    const { cardOption, summary, productsQ, goToStep, route } = this.props
    const [f1, f2, f3 ] = summary
    return !productsQ.length
      ? <Loader />
      : (
        <div className="step2-container">
          <Row>
            <Col xxl={6} xl={6} lg={8} md={24} sm={24} className="sidebar-sticky">
              <Sidebar
                sidebarTitle={'configurator.step2.sidebar.title'}
                backButton={true}
                renderComponent={
                  <div className="white summary-container">
                    {summary.map((field, index) => {
                      return field.value && (
                        <div key={index} className="summary-field">
                          <div className="summary-field-label text-body-1 a-lighter">{field.label && translate(field.label)}</div>
                          <div className="summary-field-value text-body-2 bold">
                            {field.name === 'page1.f3' ? creditCardDevider(field.value) : field.valueLabel && translate(field.valueLabel)}
                          </div>
                        </div>
                      )
                    })}
                  </div>
                }
                showTAC={false}
                showTitle={true}
              />
            </Col>
            <Col xxl={18} xl={17} lg={16} md={24} sm={24} xs={24}>
              <Row className="step2-top-container">
                <Col span={22} offset={1}>
                  <Row>
                    <Col xxl={{span: 3, offset: 0}} xl={{span: 4, offset: 0}} span={22} offset={1}>
                      <h6 className="bold d-grey step2-top-title">
                        {translate('configurator.step2.contentTitle')}
                      </h6>
                    </Col>
                    <Col xxl={{span: 20, offset: 0}} xl={{span: 20, offset: 0}} span={22} offset={1}>
                      <div className="flex-row">
                        <div className="step2-top-icon">
                          <StaticIcon name="ic-info-2" styling="icon-24x-24x"/>
                        </div>
                        <div className="text-body-1 d-grey">
                          {translate('configurator.step2.contentDescription')}
                        </div>
                      </div>
                    </Col>
                  </Row>
                </Col>
              </Row>
              <Row type="flex" justify="space-between" style={{minHeight: '100%'}}>
                {
                  productsQ.map(p => (
                    <Col
                         span={20} offset={2}
                         xxl={checkInternalUserGrid(productsQ, 'xxl')}
                         xl={checkInternalUserGrid(productsQ, 'xl')}
                         key={p.product2Id}>
                      <OptionCard
                        id={p.product2Id}
                        name={p.name}
                        description={p.description}
                        // price={p.price}
                        features={p.features}
                        active={cardOption === p.product2Id}
                        onChooseOption={this.onChooseOption}
                        includedItems={p.includedItems}
                        goToStep={goToStep}
                      />
                    </Col>
                  ))
                }
              </Row>
              <Row type="flex" justify="center">
                <Col span={20} offset={2} md={{span: 22, offset: 0}} sm={{span: 20, offset: 1}} xs={{span:24, offset: 0}}>
                  <div className="step2-footer-container">
                    <div className="align-center">
                      <h6 className="d-grey">
                        {translate('configurator.step2.buttonLabel')}
                      </h6>
                    </div>
                    <div className="align-center">
                      <Button >
                        {translate('configurator.step2.buttonText')}
                      </Button>
                    </div>
                  </div>
                </Col>
              </Row>
            </Col>
          </Row>
        </div>
      )
  }
}

Step2.propTypes = {
  cardOption: PropTypes.string,
  summary: PropTypes.array,
  productsQ: PropTypes.array,
  changeFieldValue: PropTypes.func,
  goToStep: PropTypes.func,
}

const mapStateToProps = state => ({
  cardOption: cardOptionValueSelector(state),
  summary: step2SummarySelector(state),
  products: productsSelector(state),
  productsQ: step2MetaSelector(state),
  route: routeSelector(state),

})

const mapDispatchToProps = ({
  changeFieldValue,
  goToStep
})

export default connect(mapStateToProps, mapDispatchToProps)(Step2)

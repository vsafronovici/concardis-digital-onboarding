import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Row, Col, InputNumber, Button, Input } from 'antd'
import cn from 'classnames'

import { translate } from './../../../i18n/i18n'
import {
  applyDiscountSelector,
  discountCodeSelector, quantitySelector, submittingSelector, totalCostPerMonthSelector,
  totalPriceWithDiscountSelector, validDiscountCodeSelector
} from '../../../selectors/package-configure-selector'
import { changePackageQnty, changeDiscountCode, applyDiscount } from '../../../actions/package-configure-action'
import { format, generalFormatNumber, isNotNilOrEmpty, objectToArrayKeyValue } from '../../../utils/function-utils'
import { RESPONSE_STATUS_CODE } from '../../../utils/constants'
import { isAlphaNumeric, isInteger } from '../../../utils/regexps'

import InputCounter from '../../common/InputNumber'
import { FieldTooltip } from '../../common/FieldTooltip'
import { getNotRequired } from '../../../utils/application-form-utils'

const MAX_VALUE = 10

class EditQuote extends Component {
  onChangeQty = value => {
    if (isInteger(value) && (value <= MAX_VALUE)) {
      this.props.changePackageQnty({ qty: value })
    } else if (isInteger(value) && (value > MAX_VALUE)) {
      this.props.changePackageQnty({ qty: MAX_VALUE })
    }
  }

  isApplyDiscountBtnDisabled = () => {
    const { submitting, discountCode } = this.props
    return submitting || !(discountCode && isAlphaNumeric(discountCode))
  }

  render() {
    const {
      quote: { unitPrice, totalPriceBeforeDiscount, pricePerTransactionMap },
      quantity,
      discountCode,
      totalPriceWithDiscount,
      applyDiscountStatus,
      validDiscountCode,
      totalCostPerMonth
    } = this.props

    const { code: applyDiscountCode, message: applyDiscountMsg } = applyDiscountStatus

    const totalPriceWithDiscountValue = (discountCode && discountCode === validDiscountCode && applyDiscountCode && applyDiscountCode === RESPONSE_STATUS_CODE.OK)
      ? totalPriceWithDiscount.valuePerMonth
      : totalCostPerMonth

    return (
      <div className="eq-container">
        <div className="eq-wrapper">
          <div className="eq-container-top">
            <Row>
              <Col span={24}>
                <h6 className="d-indigo bold eq-title">
                  {translate('orderConfig.editQuote.costPerMonth')}
                </h6>
                <div className="eq-container-white flex-row-space-between">
                  <h6 className="d-indigo">{translate('orderConfig.editQuote.numberOfTerminals')}</h6>
                  <div className="flex-row flex-mobile-column">
                    <InputCounter
                      min={1}
                      max={MAX_VALUE}
                      defaultValue={quantity}
                      value={quantity}
                      onChange={value => this.onChangeQty(value)}
                    />
                    <h6 className="eq-top-price d-grey">
                      {generalFormatNumber(unitPrice.valuePerMonth)} {unitPrice.currencySymbol}
                    </h6>
                  </div>
                </div>
              </Col>
              
              <Col span={24}>
                <h6 className="d-indigo bold eq-title">{translate('orderConfig.editQuote.discount.label')}</h6>
                <div className="text-body-caption d-grey">{translate('orderConfig.editQuote.discount.label.help')}</div>
                <div className="flex-row-space-between discount-container">
                  <div className="flex-row">
                    <div className={cn('eq-bottom-input-disc', { error: applyDiscountCode && applyDiscountCode !== RESPONSE_STATUS_CODE.OK })}>
                      <Input
                        className="white small eq-input-discount"
                        placeholder={translate('configurator.packagePage.field.Discount.placeholder')}
                        value={discountCode}
                        onChange={e => this.props.changeDiscountCode(e.target.value)}
                      />
                      <div>{applyDiscountMsg}</div>
                    </div>
                    <div className="eq-bottom-button-disc">
                      <Button
                        className="medium"
                        onClick={this.props.applyDiscount}
                        disabled={this.isApplyDiscountBtnDisabled()}
                      >
                        {translate('btn.applyDiscount')}
                      </Button>
                    </div>
                  </div>

                  <div className="flex-row eq-total-price-container flex-mobile-column">
                    <div className="d-indigo bold text-body-2 eq-total-price-label">
                      {translate('orderConfig.editQuote.totalBundle')}
                    </div>
                    <h4 className="d-indigo bold">
                      {generalFormatNumber(totalCostPerMonth)} {totalPriceBeforeDiscount.currencySymbol}
                    </h4>
                  </div>
                </div>
              </Col>

              <Col span={24}>
                <h6 className="d-indigo bold eq-title-big">
                  <div style={{ display: 'flex', direction: 'row' }}>
                    {translate('orderConfig.editQuote.costPerTransaction')} <FieldTooltip title={'orderConfig.editQuote.costPerTransaction.info'} />
                  </div>
                </h6>
                {isNotNilOrEmpty(pricePerTransactionMap) && objectToArrayKeyValue(pricePerTransactionMap).map((item, idx) => (
                  <div key={idx} className="eq-cost-per-tr-item">
                    <div className="eq-container-white flex-row-space-between">
                      <h6 className="d-indigo bold">{translate(item.key)}</h6>
                      <h6 className="d-indigo bold">{item.value}</h6>
                    </div>
                  </div>
                  )
                )}
              </Col>
            </Row>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  quantity: quantitySelector(state),
  discountCode: discountCodeSelector(state),
  totalCostPerMonth: totalCostPerMonthSelector(state),
  totalPriceWithDiscount: totalPriceWithDiscountSelector(state),
  applyDiscountStatus: applyDiscountSelector(state),
  validDiscountCode: validDiscountCodeSelector(state),
  submitting: submittingSelector(state)
})

const mapDispatchToProps = ({
  changePackageQnty,
  changeDiscountCode,
  applyDiscount
})

EditQuote.propTypes = {
  quantity: PropTypes.number,
  discountCode: PropTypes.string,
  totalPriceWithDiscount: PropTypes.object,
  applyDiscountStatus: PropTypes.object,
  submitting: PropTypes.bool,
  changePackageQnty: PropTypes.func,
  changeDiscountCode: PropTypes.func,
  applyDiscount: PropTypes.func,
  quote: PropTypes.object,
  validDiscountCode: PropTypes.string,
  totalCostPerMonth: PropTypes.number
}

export default connect(mapStateToProps, mapDispatchToProps)(EditQuote)


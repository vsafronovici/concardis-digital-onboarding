import React from 'react'
import StartYourApplication from './StartYourApplication'
import { PAGE_SITE_SETTINGS } from '../../../utils/constants'

export const StartApplication = props => <StartYourApplication statePageType={PAGE_SITE_SETTINGS.START_APP}/>

import React from 'react'
import { Row, Col, Button, Icon } from 'antd'
import { translate } from './../../../i18n/i18n'
import { getImageResources } from '../../../utils/function-utils'
import BackgroundFilledContainer from '../../common/BackgroundFilledContainer'

const expiredQuoteImage = getImageResources('group-3', 'svg', 'svg')

const ExpiredQuoteContent = () => {
  return (
    <div className="scenario-container flex-row-space-between">
      <div className="scenario-content">
        <div className="scenario-title text-scenario-title d-indigo bold">
          {translate('configurator.quoteExpired.title')}
        </div>
        <div className="scenario-container-content flex-row regular text-body-2">

          <div className="scenario-content">
            <p>
              {translate('configurator.quoteExpired.description1')}
            </p>
            <p>
              {translate('configurator.quoteExpired.description2')}
            </p>
            <p className="reminder text-body-1">
              {translate('configurator.quoteExpired.reminder')}
            </p>
          </div>
        </div>
        <Button onClick={() => console.log('quote has been expired')}>
           {translate('configurator.quoteExpired.button.getQuote')}
        </Button>
      </div>
      <div className="scenario-image">
        <img src={expiredQuoteImage} alt={'group-3'}/>
      </div>
    </div>
  )
}

export const ExpiredQuote = () => <BackgroundFilledContainer Content={ExpiredQuoteContent}/>




import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Row, Col } from 'antd'
import { selectedProductSelector } from '../../../selectors/configurator-selector'
import EditQuote from './EditQuote'
import Extras from './Extras'
import PackageFooter from './PackageFooter'

import OptionCard from "../../common/OptionCard";
import Sidebar from "../../common/SIdebar"
import { quoteSelector } from '../../../selectors/package-configure-selector'
import { Loader } from '../../Loader'

class PackageConfigure extends Component {
  render() {
    const { quote } = this.props
    console.log('PackageConfigure', this.props)
    return !quote ? <Loader/> : (
      <div className="package-configure" style={{ background: '#f1f1f4', minHeight: '100vh' }}>
        <Row className="flex-md-column sidebar-relative-eq">
          <Col xs={24} sm={24} md={24} lg={8} xl={6} xxl={6} className="sidebar-sticky">
            <Sidebar
              renderComponent={
                <div className='oc-sidebar'>
                  {
                    quote && <OptionCard
                    readonly={true}
                    name={quote.name}
                    description={quote.description}
                    includedItems={quote.includedItems}
                    features={quote.features}
                    {...this.props}
                  />
                  }
                </div>
              }
              showTAC={false}
              showTitle={true}
              sidebarTitle={'configurator.startYourApp.selectedBundle'}
            />
          </Col>
          <Col xs={24} sm={24} md={24} lg={{span: 14, offset: 1}} xl={{span: 16, offset: 1}} xxl={{span: 16, offset: 1}} offset={0}>
            <div className="eq-content">
              <EditQuote quote={quote} />
              <Extras extras={quote.extraItems} />
            </div>
            <PackageFooter quote={quote} />
          </Col>
        </Row>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  selectedProduct: selectedProductSelector(state),
  quote: quoteSelector(state),
})

PackageConfigure.propTypes = {
  quote: PropTypes.object
}

export default connect(mapStateToProps)(PackageConfigure)

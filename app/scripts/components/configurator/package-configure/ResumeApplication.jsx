import React from 'react'
import StartYourApplication from './StartYourApplication'
import { PAGE_SITE_SETTINGS } from '../../../utils/constants'

export const ResumeApplication = props => <StartYourApplication statePageType={PAGE_SITE_SETTINGS.RESUME_APP}/>

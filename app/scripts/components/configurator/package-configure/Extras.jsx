import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { InputNumber, Row, Col, Checkbox } from 'antd'

import { changeExtraQnty } from '../../../actions/package-configure-action'
import { extraFieldsSelector, extraFieldsTotalSelector } from '../../../selectors/package-configure-selector'
import { translate } from '../../../i18n/i18n'
import { generalFormatNumber } from './../../../utils/function-utils'
import { isInteger } from '../../../utils/regexps'
import { screenTypeSelector } from "../../../selectors/screen-selectors";
import { ScreenType } from "../../../utils/constants";

import InputCounter from '../../common/InputNumber'

const MAX_VALUE = 10

const onChangeExtraQty = (action, { id, value }) => {
  if (isInteger(value) && (value <= MAX_VALUE)) {
    action({ id, value })
  } else if (isInteger(value) && (value > MAX_VALUE)) {
    action({ id, value: MAX_VALUE })
  }
}

const Extras = (props) => {
  const onChangeCheckboxQty = (action, { id, value }) => {
    const { extraFields } = props
    
    if(!value && !extraFields[id]) {
      action({ id, value: 1 })
    }
  }

  const { extras, extraFields, extraFieldsTotal, screenType } = props
  return (
    <div className="ex-container">
      <div className="ex-wrapper">
        <h6 className="d-indigo bold">{translate('configurator.packagePage.extras')}</h6>
        <Row>
          { extras.map((item, index) =>
            <Col span={24} key={index} className={`eq-container-white flex-row-space-between flex-mobile-column extras-item ${ !!extraFields[item.quoteItemId] ? 'active': '' }`} style={{ marginTop: '20px' }}>
              <div className="flex-row">
                <Checkbox
                  className="extras-item-control"
                  checked={!!extraFields[item.quoteItemId]}
                  onChange={e => onChangeCheckboxQty(props.changeExtraQnty, { id: item.quoteItemId, value: e.target.value })}
                  value={!!extraFields[item.quoteItemId]}
                  />
                <div className="extras-item-control-descriptions">
                  <h6 className="extras-item-control d-indigo bold add-item">{translate('orderConfig.extras.addOn')}</h6>
                  <div className="extras-item-control" style={{ fontSize: '14px' }}>{`${item.name} ${item.description}`}</div>
                </div>
              </div>
              <div className="flex-row flex-mobile-column" style={(screenType === ScreenType.XS || ScreenType.SM) && {marginTop: '10px'} }>
                { !!extraFields[item.quoteItemId] &&
                  <InputCounter
                    screenType={screenType}
                    min={0}
                    max={MAX_VALUE}
                    defaultValue={extraFields[item.quoteItemId]}
                    value={extraFields[item.quoteItemId]}
                    onChange={value => onChangeExtraQty(props.changeExtraQnty, { id: item.quoteItemId, value })}
                  />
                }
                <div className="ex-price">
                  <h6 className="d-indigo bold">+ {generalFormatNumber(item.price.value)} {item.price.currencySymbol}</h6>
                  <span className="subtitle">({translate('configurator.packagePage.OneOffCost')})</span>
                </div>
              </div>
            </Col>
          )}
        </Row>
        <Row>
          <div className="flex-row-justify-end" style={{ marginTop: '50px' }}>
            <div>
              <span className="d-indigo bold text-body-2">{translate('orderConfig.extras.totalExtras')}</span>
              <span className="d-indigo bold ex-total-cost">{generalFormatNumber(extraFieldsTotal)} EUR</span>
              <span className="d-indigo bold text-body-2">+VAT</span>
            </div>
          </div>
        </Row>
      </div>
    </div>
  )
}

const mapStateToProps = state => ({
  extraFields: extraFieldsSelector(state),
  extraFieldsTotal: extraFieldsTotalSelector(state),
  screenType: screenTypeSelector(state),
})

const mapDispatchToProps = ({
  changeExtraQnty
})

Extras.propTypes = {
  extraFields: PropTypes.object,
  extraFieldsTotal: PropTypes.number,
  extras: PropTypes.array,
  changeExtraQnty: PropTypes.func,
}

export default connect(mapStateToProps, mapDispatchToProps)(Extras)

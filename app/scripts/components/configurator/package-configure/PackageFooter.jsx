import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Button, Row, Col } from 'antd'

import { translate } from './../../../i18n/i18n'
import { applyDiscountPayloadSelector, submittingSelector } from '../../../selectors/package-configure-selector'
import { submitQuote, confirmOffer } from '../../../actions/package-configure-action'

class PackageFooter extends Component {
  static propTypes = {
    quote: PropTypes.object,
    applyDiscountPayload: PropTypes.object,
    submitQuoteAction: PropTypes.func,
    confirmOfferAction: PropTypes.func,
    submitting: PropTypes.bool
  }

  render() {
    const { quote: { quoteValidTill }, applyDiscountPayload, submitQuoteAction, confirmOfferAction, submitting } = this.props
    return (
      <div className="eq-footer">
        <Row>
          <Col span={24} className="eq-container-white flex-row-space-between flex-mobile-column">
            <div className="flex-row">
              <h6 className="d-indigo bold eq-offer-valid">
                <span>{translate('configurator.packagePage.OfferValid')}</span>
                <span className="r-orange">{quoteValidTill}</span>
              </h6>
            </div>
            <Button onClick={() => confirmOfferAction(applyDiscountPayload)} disabled={submitting}>
              {translate('orderConfig.footer.button')}
            </Button>
          </Col>
        </Row>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  applyDiscountPayload: applyDiscountPayloadSelector(state),
  submitting: submittingSelector(state),
})

const mapDispatchToProps = ({
  submitQuoteAction: submitQuote,
  confirmOfferAction: confirmOffer
})

export default connect(mapStateToProps, mapDispatchToProps)(PackageFooter)

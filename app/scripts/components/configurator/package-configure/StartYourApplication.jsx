import React from 'react'
import { connect } from 'react-redux'
import { Row, Col, Icon, Button } from 'antd'
import Sidebar from "../../common/SIdebar"
import OptionCard from "../../common/OptionCard"
import { PAGE_SITE_SETTINGS, Routes } from "../../../utils/constants"
import { translate } from "../../../i18n/i18n"
import { StaticIcon } from "../../common/Icon"
import { quoteSelector } from '../../../selectors/package-configure-selector'
import { Loader } from '../../Loader'
import { setRoute } from '../../../actions/route-action'
import OrderDetailsContainer from '../../common/OrderDetails'

const FEATURES = [
  {
    icon: <i className="icon-timer-10 feature-icon d-grey"></i>,
    description: 'configurator.packagePersonalise.card.description1',
  },
  {
    icon: <i className="icon-gesture-tap-clock feature-icon d-grey"></i>,
    description: 'configurator.packagePersonalise.card.description2',
  },
  {
    icon: <i className="icon-time-stopwatch-quarter feature-icon d-grey"></i>,
    description: 'configurator.packagePersonalise.card.description3',
  }]

const INFO_FEATURES = ['configuratpr.startYourApp.info.feature1' , 'configurator.startYourApp.info.feature2' , 'configurator.startYourApp.info.feature3']
const isStartYourAppPage = type => type === PAGE_SITE_SETTINGS.START_APP

const getDataLabels = (TYPE) => {
  switch (TYPE) {
    case PAGE_SITE_SETTINGS.START_APP: return ({
      title: translate('configurator.startYourApp.title'),
      btnLabel: translate('configurator.startYourApp.button.startApp')
    })
    case PAGE_SITE_SETTINGS.RESUME_APP: return ({
      title: translate('configurator.applicationResume.title'),
      btnLabel:translate('configurator.applicationResume.btn.applicationResume')
    })
    default: return ({
      title: translate('configurator.startYourApp.title'),
      btnLabel: translate('configurator.startYourApp.button.startApp')
    })
  }
}



const StartYourApplication = props => {

  const { quote, statePageType, setRoute } = props

  if (!quote)
    return <Loader />

  const { includedItems, description, name, features, quoteValidTill } = quote
  const { title, btnLabel } = getDataLabels(statePageType)
  const showResumeSections = isStartYourAppPage(statePageType)
  return (
    <div className="white-grey-back-ground-wrapper start-app-sidebar-container horizontal-option-card">
      <Row className="flex-md-column">
        <Col xs={24} sm={24} md={24} lg={8} xl={6} className="sidebar-sticky">
          <Sidebar renderComponent={name &&
            <OptionCard
              includedItems={includedItems}
              name={name}
              description={description}
              features={features}
              onChooseOption={() => console.log('test')}
              {...props} />
          }
              showTitle={true}
              sidebarTitle={'configurator.startYourApp.selectedBundle'}
          />
        </Col>
        <Col xs={24} sm={24} md={24} lg={16} xl={18} offset={0} style={{height: '100%'}}>
          <div className="start-app-container">
            <Row>
              <Col  xl={16}>
                <div className="title d-indigo bold">
                  <h2>{title}</h2>
                </div>
                <div className="description d-grey">
                  <h6 className="regular">{translate('configurator.startYourApp.description1')}</h6>
                  <h6 className="regular">{translate('configurator.startYourApp.description2')}</h6>
                </div>

              </Col>
              <Col xl={7} offset={1}>
                <div className="info flex-row">
                  <StaticIcon name="ic-info-2" styling="icon-24x-24x"/>
                  <div className="info-header text-body-1 d-grey regular">
                    <div>{translate('configurator.startYourApp.info.title')}</div>
                    {INFO_FEATURES.map((feature, index) => {
                      return (
                        <div key={index}>
                          {translate(feature)}
                        </div>
                      )
                    })}
                  </div>
                </div>
              </Col>
            </Row>
            <Row>
              <Col xl={16}>
                <div className="features-container">
                  {FEATURES.map(({ icon, description }, index) => {
                    return (
                      <div key={index} className="feature-item">
                        <div className="icon">
                          {icon}
                        </div>
                        <div className="description font-size-body2">
                          {translate(description)}
                          <br/>
                          {index === 2 && <div className="text-body-2 r-orange bold">
                            {quoteValidTill}
                          </div>}
                        </div>
                      </div>
                    )
                  })}
                </div>
                { showResumeSections &&  <OrderDetailsContainer />}
                <div className="buttons-container">
                  {
                    showResumeSections &&
                    <div className="edit-order">
                      <Button className="secondary" onClick={() => setRoute(Routes.EDIT_PACKAGE)}>{translate('configurator.startYourApp.button.edit')}</Button>
                    </div>
                  }
                  <div className="start-app-form">
                    <Button onClick={() => setRoute(Routes.APP_FORM_SWITCHER)}>{btnLabel}</Button>
                  </div>
                </div>
              </Col>
            </Row>
          </div>
        </Col>
      </Row>
    </div>
  )
}

const mapStateToProps = state => ({
  quote: quoteSelector(state),
})

const mapDispatchToProps = ({
  setRoute
})

export default connect(mapStateToProps, mapDispatchToProps)(StartYourApplication)

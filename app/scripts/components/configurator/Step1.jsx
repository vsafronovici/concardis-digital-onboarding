import React from 'react'
import { connect } from 'react-redux'
import { Row, Col, Button } from 'antd'
import PropTypes from 'prop-types'
import { prop } from 'ramda'

import { changeFieldValue, goToStep } from '../../actions/configurator-action'
import { step1MetaSelector, step1FieldsSelector } from '../../selectors/configurator-selector'
import { ConfiguratorPageStep, FieldType } from '../../utils/constants'
import { Loader } from '../Loader'
import { translate } from './../../i18n/i18n'
import { notEmptyValues } from './../../utils/function-utils'
import { screenTypeSelector } from '../../selectors/screen-selectors'
import Sidebar from '../../components/common/SIdebar'
import SelectableItems from './SelectableItems'
import RenderSlider from './slider/RenderSlider'

const getSelectableFields = (fieldType, fields = []) => {
  return fields.reduce((acc, item) => {
    if (item.type === fieldType) {
       acc.push(item)
    }

    return acc
  }, [])
}

const getSliderItems = (options) => {
  return options.map(({ value }) => Number(value))
}

const Step1 = props => {
  const { step1MetaData, values, changeFieldValue } = props

  const isShowSlider = prop('page1.f2')(values);
  return (
    !step1MetaData
      ? <Loader />
      : (
        <Row style={{ background: '#fff' }}>
          <Col xxl={6} xl={6} lg={8} md={24} sm={24} xs={24} className="sidebar-sticky">
            <Sidebar
              sidebarTitle={'page1.sidebar.title'}
              renderComponent={
                <p className="regular white">
                  {translate('page1.sidebar.description')}
                </p>
              }
              showTAC={false}
              showTitle={true}
            />
          </Col>
          <Col xxl={16} xl={16} lg={15} md={22} sm={22} offset={1} xs={22}>
            { 
              getSelectableFields(FieldType.SELECTABLE_ITEMS, step1MetaData)
                .map(({ title, options, optionType, name, defaultValue, label }, index) => (
                  <div key={index} style={{ marginTop: '70px' }}>
                    <h5 className="d-grey">{translate(label)}</h5>
                    {title && <div className="regular d-grey text-body-1">{translate(title)}</div>}
                    <SelectableItems
                      options={options}
                      name={name}
                      type={optionType}
                      defaultValue={values[name] || defaultValue}
                      handleChangeField={changeFieldValue}
                    />
                  </div>
                ))
            }
            <Row>
              <Col xxl={10} xl={10} lg={10}>
                {
                  !!Number(isShowSlider) &&
                  <div>
                    {
                      getSelectableFields(FieldType.SLIDER, step1MetaData)
                        .map(({ title, name, options, defaultValue, label }, index) => {
                          const checkOptions = getSliderItems(options)
                          return (
                            <div key={index} style={{ marginTop: '70px' }}>
                              <h5 className="d-grey">{translate(label)}</h5>
                              <RenderSlider
                                sliderItems={checkOptions}
                                name={name}
                                title={translate(label)}
                                defaultValue={translate(defaultValue)}
                                handleChangeField={changeFieldValue}
                              />
                            </div>
                          )
                        })
                    }
                    <div className="flex-row-center" style={{ marginTop: '60px' }}>
                      <div className="flex-row">
                        <span className="slider-description rusty"></span>
                        GIRO
                      </div>
                      <div className="flex-row" style={{ marginLeft: '20px' }}>
                        <span className="slider-description indigo"></span>
                        CREDIT CARD
                      </div>
                    </div>
                  </div>
                }
                <div className="sc-wrapper-button">
                  <Button onClick={() => props.goToStep(ConfiguratorPageStep.STEP2)} disabled={notEmptyValues(values)}>
                    {translate('btn.ShowProducts')}
                  </Button>
                </div>
              </Col>
            </Row>
          </Col>
        </Row>
      )
  )
}

const mapStateToProps = state => ({
  step1MetaData: step1MetaSelector(state),
  values: step1FieldsSelector(state),
  screenType: screenTypeSelector(state),
})

const mapDispatchToProps = ({
  changeFieldValue,
  goToStep
})

Step1.propTypes = {
  values: PropTypes.object,
  step1MetaData: PropTypes.array,
  goToStep: PropTypes.func,
  changeFieldValue: PropTypes.func
}

export default connect(mapStateToProps, mapDispatchToProps)(Step1)

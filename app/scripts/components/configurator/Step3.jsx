import React, { Component } from 'react'
import PropTypes from 'prop-types'

import { connect } from 'react-redux'
import { Row, Col, Input, Checkbox, Button } from 'antd'
import { Field, getFormValues, reduxForm } from 'redux-form'
import cn from 'classnames'

import { selectedProductSelector, step1FieldsSelector } from '../../selectors/configurator-selector'
import { translate } from '../../i18n/i18n'
import { ConfiguratorPageStep } from '../../utils/constants'
import { email as emailReg } from '../../utils/regexps'
import TermsAndConditions from '../common/modal/TermsAndConditions'
import VoidLink from '../common/VoidLink'
import { signupReq, goToStep, getConfiguratorQuote } from '../../actions/configurator-action'
import { StaticIcon } from "../common/Icon";
import Sidebar from "../common/SIdebar";
import StepsBar from "../application-form/StepsBar";
import OptionCard from "../common/OptionCard";
import { ConfirmModal } from "../common/Confirm";

export const EMAIL = 'email'


export const Validator = values => {
  const err = {}
  const email = values[EMAIL]

  if (!emailReg.test(email)) {
    err[EMAIL] = 'err.email.invalidFormat'
  }
  return err
}

// eslint-disable-next-line react/prop-types
const createRenderer = render => ({ input, meta, label, placeholder }) => (
  <div className="form-field">
    <div className={cn({ error: meta.error && meta.touched, active: meta.active })}>
      <div className="form-label text-body-1 regular">
        {translate(label)}
      </div>
      <div className="form-input">
        { render(input, meta, label, placeholder) }
      </div>
      <div>{ meta.error && meta.touched && <span>{translate(meta.error)}</span> }</div>
    </div>
  </div>
)

const RenderInput = createRenderer((input, meta, label, placeholder) =>
  <Input {...input} onChange={(event) => input.onChange(event)} placeholder={translate(placeholder)} value={input.value} />
)

const constructTADLabel = (str, onClick) => {
  const idx1 = str.indexOf('${')
  const idx2 = str.indexOf('}')
  return (
    <div className="gdpr-label d-grey text-body-1">
      {str.substring(0, idx1)}
      <VoidLink onClick={onClick} className="bold">{str.substring(idx1 + 2, idx2)}</VoidLink>
      {str.substring(idx2 + 1)}
    </div>
  )
}


class Step3 extends Component {

  state = {
    showModal: false,
    gdprAccepted: false,
    showConfirmModal: false
  }

  toggleConfirmModal = () => {
    this.setState({
      ...this.state,
      showConfirmModal: !this.state.showConfirmModal
    })
  }

  toggleShowTermsAndConditions = showModal => () => {
    this.setState({ ...this.state, showModal })
  }

  onChangeGDPR = e => {
    this.setState({ ...this.state, gdprAccepted: e.target.checked })
  }

  isDisabledBtn = () => {
    const { invalid } = this.props
    const { gdprAccepted } = this.state
    return invalid || !gdprAccepted
  }

  signup = () => {
    const { formValues, selectedProduct, step1Fields } = this.props
    this.props.signupReq({
      productId: selectedProduct.product2Id,
      email: formValues[EMAIL],
      filters: {
        ...step1Fields
      }
    })
    this.props.goToStep(ConfiguratorPageStep.STEP4)
  }

  render() {
    const { selectedProduct: { name, description, features, includedItems } } = this.props
    return (
      <div className="sign-up-container">
        <Row>
          <Col xs={24} sm={24} md={24} lg={8} xl={6} style={{position: 'relative'}}>
            <Sidebar renderComponent={<OptionCard
              readonly={true}
              name={name}
              description={description}
              features={features}
              includedItems={includedItems}
              {...this.props}
            />
            }
             sidebarTitle={'configurator.step3.sideBar.title'}
             showTitle={true}/>
          </Col>
          <Col xs={24} sm={24} md={24} lg={{span: 14, offset: 1}} xl={12}>
            <div className="con-signup-container">
              <div className="con-signup-title"><h6 className="d-grey">{translate('configurator.step3.title')}</h6></div>
              <div className="con-signup">
                <div className="gdpr-container">
                  <div>
                    <Field
                      name={EMAIL}
                      component={RenderInput}
                      label="configurator.signupPage.email.label"
                      placeholder="configurator.signupPage.email.placeholder"
                    />
                  </div>
                    <div className="gdpr">
                      <Checkbox onChange={this.onChangeGDPR} className="blue-checkbox"/>
                      <div className="text-body-1 regular">{constructTADLabel(translate('configurator.signupPage.TAC.label'), this.toggleShowTermsAndConditions(true))}</div>
                    </div>
                    <div>

                      <Button disabled={this.isDisabledBtn()} onClick={this.toggleConfirmModal} className="small-padding">{translate('btn.signup')}</Button>
                    </div>
                    <div className="info-container flex-row">
                      <div className="icon-container">
                        <StaticIcon name="ic-info-2"/>
                      </div>
                      <div className="text-body-1 regular">
                        {translate('configurator.step3.info.text')}
                      </div>
                    </div>
                </div>
              </div>
            </div>
          </Col>
        </Row>
        <TermsAndConditions show={this.state.showModal} onClose={this.toggleShowTermsAndConditions(false)} />
        <ConfirmModal
          onOk={this.signup}
          onCancel={this.toggleConfirmModal}
          visible={this.state.showConfirmModal}
        />
      </div>
    )
  }
}

Step3.propTypes = {
  formValues: PropTypes.object,
  selectedProduct: PropTypes.object,
  step1Fields: PropTypes.object,
  invalid: PropTypes.bool,
  signupReq: PropTypes.func,
  goToStep: PropTypes.func
}

const Form = reduxForm({
  form: ConfiguratorPageStep.STEP3,
  validate: Validator,
})(Step3)

const mapStateToProps = state => ({
  step1Fields: step1FieldsSelector(state),
  selectedProduct: selectedProductSelector(state),
  formValues: getFormValues(ConfiguratorPageStep.STEP3)(state),
})

const mapDispatchToProps = ({
  signupReq,
  goToStep,
  getConfiguratorQuote
})

export default connect(mapStateToProps, mapDispatchToProps)(Form)

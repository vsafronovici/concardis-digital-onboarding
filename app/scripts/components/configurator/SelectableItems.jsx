import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Radio } from 'antd'

import { stepSelector } from '../../selectors/configurator-selector'
import { OptionType } from '../../utils/constants'
import { translate } from './../../i18n/i18n'

const RadioButton = Radio.Button;
const RadioGroup = Radio.Group;

const RadiotListOption = (props) => {
  const { option, arr, index, value } = props
  return(<RadioButton className="selectable_item" value={option.value}>{translate(option.label)}</RadioButton>)
}

const RadioImageOption = (props) => {
  const { option, arr, index, value } = props
  const getCalssName = () => value === arr[index].value ? 'checked-field' : ''
  
  return (
    <div className={`radio-container ${getCalssName()}`}>
      <i className={`image-label ${option.image}`}></i>
      <RadioButton value={option.value}>{translate(option.label)}</RadioButton>
    </div>
  )
}

class SelectableItems extends Component {
  state = {
    value: '' || this.props.defaultValue
  }

  componentDidMount() {
    const { name, step, defaultValue, handleChangeField} = this.props
    !!defaultValue && handleChangeField({ value: defaultValue, name, step })
  }

  onChange = (e) => {
    const { name, step } = this.props
    this.setState({ value: e.target.value })

    this.props.handleChangeField({ value: e.target.value, name, step })
  }

  render() {
    const { defaultValue, options, type } = this.props
    const { value } = this.state

    return (
      <div style={{ marginTop: '30px' }}>
        <RadioGroup className="selectable-items-container" onChange={this.onChange} defaultValue={defaultValue}>
          { 
            options.map((option, index, arr) => {
              const props = { option, arr, index, value, defaultValue }
              return (type === OptionType.IMAGE_LABEL ) ? <RadioImageOption key={index} {...props} /> : <RadiotListOption key={index} {...props} />
            })
          }
        </RadioGroup>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  step: stepSelector(state)
})

export default connect(mapStateToProps)(SelectableItems)

import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Steps, Popover } from 'antd'
import { translate } from './../../i18n/i18n'
import { isSmallScreenSelector, isMobileSelector } from '../../selectors/screen-selectors'
import { SectionStatusType, StepBarProgress } from '../../utils/constants'
import { currentSelector, chaptersSelector, reviewModeSelector } from '../../selectors/application-form-selector'

const { Step } = Steps
const { WAIT, FINISH, PROCESS } = StepBarProgress
const renderStep = (props) => {
  const { section, stepIndex, currentIndex, reviewMode } = props
  const checkStepStatus = () => {
    switch (section.status) {
      case (stepIndex === currentIndex):
        return PROCESS
      case SectionStatusType.WAITING:
        return WAIT
      case SectionStatusType.FINISHED:
        return FINISH
      default:
        return WAIT
    }
  }
  const checkNewCurrent = ((stepIndex === currentIndex) ? PROCESS : checkStepStatus(section.status)) ||
    ((stepIndex !== currentIndex) ? WAIT : checkStepStatus(section.status))
  return <Step
    key={stepIndex}
    status={reviewMode ? FINISH : checkNewCurrent}
    disabled={true}
    title={<span className="field-step">{translate(props.section.title)}</span>}
  />
}

renderStep.propTypes = {
  section: PropTypes.any,
  stepIndex: PropTypes.any,
  currentIndex: PropTypes.any,
  reviewMode: PropTypes.any
}
const CustomDot = (dot, { status, index, title }) => {
  return (
    status === PROCESS &&
    <Popover
      placement="right"
      trigger="click"
      content={<span>{title}</span>}
    >
      {dot}
    </Popover>
  )
}

export class StepsBar extends Component  {

  state = {
    isOpen: false,
  }

  toggleMobileSideBar = () => {
    this.setState({
      ...this.state,
      isOpen: !this.state.isOpen
    })
  }

  render() {
    const { sections = [], current, reviewMode, isMobile, isSmallScreen } = this.props
    const { isOpen } = this.state
    return (
      <div className="sf-container">
        <div className="steps-wrapper">
          <Steps direction="vertical" size="small" current={current} progressDot={isSmallScreen && CustomDot}>
            { sections && (((!isMobile || isOpen) && [...sections.map((section, index) => renderStep({ section, stepIndex: index, currentIndex: current, reviewMode })),
              <Step
                key={sections.length}
                status={reviewMode ? PROCESS : WAIT}
                disabled={true}
                title={<span className="field-step">{translate('review_application_title')}</span>}
              />]) ||
              ((isMobile || !isOpen) && ((!reviewMode && renderStep({ section: sections[current], stepIndex: current, currentIndex: current, reviewMode })) ||
                <Step
                  key={'7989'}
                  status={reviewMode && PROCESS}
                  disabled={true}
                  title={<span className="field-step">{translate('review_application_title')}</span>}
                />))
            )}
          </Steps>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  sections: chaptersSelector(state),
  current: currentSelector(state),
  reviewMode: reviewModeSelector(state),
  isSmallScreen: isSmallScreenSelector(state),
  isMobile: isMobileSelector(state),
})

export default connect(mapStateToProps)(StepsBar)

StepsBar.propTypes = {
  sections: PropTypes.array,
  current: PropTypes.any,
  reviewMode: PropTypes.any,
  isSmallScreen: PropTypes.bool,
  isMobile: PropTypes.bool
}

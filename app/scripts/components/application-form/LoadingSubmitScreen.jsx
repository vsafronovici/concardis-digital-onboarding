import React from 'react'
import LoadingPage from './LoadingPage'
import BackgroundFilledContainer from '../common/BackgroundFilledContainer'

export const LoadingSubmitScreen = props => (
  <BackgroundFilledContainer Content={LoadingPage} />
)

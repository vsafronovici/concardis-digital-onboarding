import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Row, Col, Button } from 'antd'
import { translate } from '../../i18n/i18n'
import { submit, openTACModal } from '../../actions/application-form-action'
import ReviewChapters from './ReviewChapters'
import {
  currentSelector,
  reviewModeSelector
} from '../../selectors/application-form-selector'
import { quoteSelector } from '../../selectors/package-configure-selector'
import VoidLink from "../common/VoidLink";
import OptionCard from "../common/OptionCard"
import { Routes } from '../../utils/constants'
import Sidebar from '../common/SIdebar'
import StepsBar from './StepsBar'
import { isInternalUser } from '../../utils/vf-utils'
import { setRoute } from '../../actions/route-action'

const openTAC = ({ openTACModalAction }, readOnly) => () => {
  openTACModalAction(readOnly)
}


const ReviewYourApplication = props => {

  const { quote, current, reviewMode, setRoute } = props

  const reviewPageMode = current === -1
  const backGround = reviewPageMode ? 'grey-back-ground' : 'white-back-ground'
  return (
    <div className={`review-container ${backGround}`}>
      <Row>
        <Col xxl={6} xl={6} lg={8} md={3} sm={3} xs={24} className="sidebar-sticky">
          <Sidebar renderComponent={<StepsBar {...props}/>} showTAC={true} sidebarTitle="steps_applicationForm_title" showTitle={true} />
        </Col>
        <Col xxl={16} xl={16} lg={14} md={19} sm={19} offset={1} xs={22}>
          <div className={`review-sections-container ${backGround}`}>
            <Row>
              <Col span={24} >
                <div className="field-review-application">
                  <div className={reviewPageMode ? "flex-row-space-between" : "flex-row-justify-end"}>
                    {
                      reviewPageMode &&
                    <div className="title">
                      <h5 className="bold">{translate('appForm.reviewAppPage.title')}</h5>
                    </div>
                    }
                    <div className="review-close-container text-body-caption a-lighter">
                      <VoidLink onClick={() => {
                        setRoute(Routes.RESUME_APPLICATION)
                      }}>{translate('appForm.review.close').toUpperCase()}</VoidLink>
                    </div>
                  </div>
                  {reviewPageMode && <div className="description-review d-grey text-body-1">
                    {translate('appForm.reviewAppPage.description')}
                  </div>}
                  <Row>
                    <div className="review-content-container">
                      <div className="review-content-sections">
                        <Col span={24} >
                          <ReviewChapters />
                        </Col>
                      </div>
                      {reviewPageMode && <div className="review-content-quote">
                        <Col span={24} >
                          <div className="bundle-container">
                            <div className="quote-container">
                              {quote && <OptionCard
                                includedItems={quote.includedItems}
                                description={quote.description}
                                name={quote.name}
                                features={quote.features} />}
                            </div>
                          </div>
                        </Col>
                      </div>}
                    </div>
                  </Row>
                </div>
              </Col>
            </Row>
          </div>
          {reviewPageMode && !isInternalUser && <Row>
            <Col span={24}>
              <div className="review-footer-container">
                <div className="review-buttons-container">
                  <Button onClick={openTAC(props, false)} disabled={reviewMode && current !== -1}>{translate('btn_applicationForm_submitApplication')}</Button>
                  <Button className="tac" onClick={openTAC(props, true)}>{translate('btn_applicationForm_termsAndConditions')}</Button>
                </div>
              </div>
            </Col>
          </Row>}
        </Col>
      </Row>

    </div>
  )
}

const mapStateToProps = (state) => ({
  current: currentSelector(state),
  reviewMode: reviewModeSelector(state),
  quote: quoteSelector(state)
})

const mapDispatchToProps = ({
  openTACModalAction: openTACModal,
  submitAction: submit,
  setRoute: setRoute
})

export default connect(mapStateToProps, mapDispatchToProps)(ReviewYourApplication)

ReviewYourApplication.propTypes = {
  submitAction: PropTypes.func,
}

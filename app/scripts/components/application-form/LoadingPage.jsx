import React, { Component } from 'react'
import { CometD } from 'cometd'
import { Row, Col } from 'antd'
import  { connect } from 'react-redux'
import { Loader } from '../Loader'
import { getSessionReq, submitRes } from '../../actions/application-form-action'
import { sessionIdSelector } from '../../selectors/application-form-selector'
import { RESPONSE_STREEMING_STATUS, RESPONSE_STATUS } from '../../utils/constants'
import { getImageResources } from '../../utils/function-utils'
import BackgroundFilledContainer from '../common/BackgroundFilledContainer'

const transormToResponseStatus = (STATUS) => STATUS === RESPONSE_STREEMING_STATUS.SUCCESS ? RESPONSE_STATUS.OK : RESPONSE_STATUS.ERR;

class LoadingPage extends Component {
  constructor(props) {
    super(props)
    // this.cometd = new CometD()
  }

  componentDidMount() {
    //this.props.getSessionReq()
  }

  componentWillReceiveProps({ sessionId }) {
    /*if (!!sessionId) {
      this.initCometD(sessionId)
      this.setSubscriptionToCometD()
    }*/
  }

  setSubscriptionToCometD() {
    this.cometd.handshake((h) => {
      if (h.successful) {
        this.cometd.subscribe('/topic/ApplicationsUpdates', ({ data: { sobject: { Status__c, Email__c } } }) => {
            const status = transormToResponseStatus(Status__c)
            this.props.submitRes({ status })
        });
      }
    });
  }

  initCometD(sessionId) {
    this.cometd.websocketEnabled = false
    this.cometd.configure({
      url: window.location.protocol+'//'+window.location.hostname+ (null != window.location.port ? (':'+window.location.port) : '') +
      '/cometd/44.0',
      requestHeaders: {
        Authorization: 'OAuth ' + sessionId,
      },
      appendMessageTypeToURL : false
    })
  }

  render() {
    return (
      <div className="container-loading-page">
        <div className="loader">
          <Loader />
        </div>
        <div>
          <div className="title d-indigo bold">
              Just a moment...
          </div>
          <div className="description d-indigo regular text-body-2">
              We`re submitting your application. it shouldn`t take a long time.
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  sessionId: sessionIdSelector(state)
})

const mapDispatchToProps = ({
  getSessionReq,
  submitRes,
})

export default connect(mapStateToProps, mapDispatchToProps)(LoadingPage)

import React from 'react'
import { Col, Row } from 'antd'
import { translate } from '../../i18n/i18n'

export const ErrorSubmitPage = props => (
  <Row>
    <Col span={16} offset={4}>
        <div className="error-page-container">
          <div className="title">
            {translate('appForm.review.errorPage.title')}
          </div>
          <div className="subtitle">
            {translate('appForm.review.errorPage.subTitle')}
          </div>
          <div className="description">
            {translate('appForm.review.errorPage.description')}
          </div>
        </div>
    </Col>
  </Row>
)

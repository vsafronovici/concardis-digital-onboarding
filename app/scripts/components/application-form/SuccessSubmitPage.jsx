import React from 'react'
import { Row, Col, Button } from 'antd'
import { getImageResources } from '../../utils/function-utils'
import BackgroundFilledContainer from '../common/BackgroundFilledContainer'

const successImagePreview = getImageResources('group-5', 'svg', 'svg')
const goToConcardisPage = () => window.location.href = 'https://mysit.concardis.com/web/?update-session=true'

const SuccessSubmitContent = props => {
  return (
    <div className="scenario-container flex-row-space-between">
      <div className="scenario-content" style={{ maxWidth: '50%' }}>
        <div className="scenario-title text-scenario-title d-indigo bold">
          Application Submitted
        </div>
        <div className="scenario-container-content flex-row regular text-body-2">

          <div className="scenario-content">
            <p>
              Please check your email and click on the invitation link to the Concardis Merchant portal.
            </p>
            <p>
            You will be able to use the portal to proceed with your application and monitor its progress in real time
            </p>
          </div>
        </div>
        <Button onClick={goToConcardisPage}>CHECK APPLICATION STATUS</Button>
      </div>
      <div className="scenario-image">
        <img src={successImagePreview} alt={'group-5'}/>
      </div>
    </div>
  )
}

export const SuccessSubmitPage = () => <BackgroundFilledContainer Content={SuccessSubmitContent} />

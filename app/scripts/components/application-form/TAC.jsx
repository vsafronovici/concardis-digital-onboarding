import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Button } from 'antd'

import { openTACModal, toggleTACReadonly } from '../../actions/application-form-action'
import { translate } from './../../i18n/i18n'

const openTAC = ({ openTACModalAction }) => () => {
  openTACModalAction(true)
}

const TAC = props => {

  return (
    <div className="TAC-container">
      <Button className="steps-btn secondary medium" onClick={openTAC(props)}>{translate('appForm.btn.TAD')}</Button>
    </div>
  )
}

const mapDispatchToProps = ({
  openTACModalAction: openTACModal,
})

export default connect(null, mapDispatchToProps)(TAC)

TAC.propTypes = {
  openTACModalAction: PropTypes.func,
}

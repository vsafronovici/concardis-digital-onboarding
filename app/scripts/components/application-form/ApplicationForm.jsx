import React from 'react'
import { connect } from 'react-redux'
import { Row, Col } from 'antd'

import StepsBar from './StepsBar'
import TAC from './TAC'
import SectionForm from './ChapterForm'
import { translate } from '../../i18n/i18n'
import Sidebar from "../common/SIdebar";
import { chaptersSelector, reviewModeSelector } from '../../selectors/application-form-selector'
import { Loader } from '../Loader'

const ApplicationForm = props => {
  console.log('ApplicationForm', props)
  return (
    <Row>
      <Col xs={24} sm={3} md={3} lg={8} xl={6} className="sidebar-sticky app-form-sidebar-container">
        <Sidebar renderComponent={<StepsBar {...props} />} showTAC={true} sidebarTitle="steps_applicationForm_title" showTitle={true}/>
      </Col>
      <Col xs={22} sm={19} md={19} lg={14} xl={16} offset={1}>
        <div className="section-form-container">
          <SectionForm />
        </div>
      </Col>
    </Row>
  )
}

const mapStateToProps = state => ({
  chapters: chaptersSelector(state),
  reviewMode: reviewModeSelector(state),
})

const mapDispatchToProps = ({

})

export default connect(mapStateToProps, mapDispatchToProps)(ApplicationForm)

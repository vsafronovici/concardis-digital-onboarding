import React from 'react'
import { connect } from 'react-redux'
import moment from 'moment'
import { values, pipe, slice, contains, omit } from 'ramda'
import { translate } from "../../i18n/i18n";
import { DATE_TO_DISPLAY, AccountCard } from "../../utils/constants";
import {
  receiptChapterBoldSelector,
  receiptChapterFormValuesSelector
} from "../../selectors/application-form-selector";
import { INPUT_BOLD_PREFIX } from '../../utils/application-form-utils'
import { findObjKeys } from '../../utils/function-utils'

const renderValue = (value, index, isBold) => {
    return (
          index !== 3
            ?
              <div key={index} className={index === 0 && isBold ? 'bold' : 'regular'}>
                {value}
              </div>
            :
              <div key={index} className="phone-number ">
                  {value}
              </div>
          )
    }

const filterReceiptItems = pipe(
  obj => omit(findObjKeys(contains(INPUT_BOLD_PREFIX), obj), obj),
  values
)

const Receipt = props => {
  const { formValues, isBold } = props
  const filteredReceiptItems = filterReceiptItems(formValues)
  const items = slice(0, filteredReceiptItems.length - 2, filteredReceiptItems)
  const lastItem = filteredReceiptItems.length > 1 ? filteredReceiptItems[filteredReceiptItems.length - 2] : ''

  const now = moment()
  const time = now.format(DATE_TO_DISPLAY.TIME)
  const date = now.format(DATE_TO_DISPLAY.DATE)

  return (
    <div className="receipt-container">
      {formValues && items.map((value, index) => renderValue(value , index, isBold))}
      <div className="card-holder bold text-body-2">
        CardHolder Receipt Payment
      </div>
      <div className="bold text-body-1">
         Product1
      </div>
      <div className="price flex-row flex-row-justify-end bold">
        <h4>10.00 EUR</h4>
      </div>
      <div className="card-account-container text-body-1">
        {AccountCard.map((value, index) => {
          return (
            <div className="flex-row flex-justify-space-between">
              <div>
                {value.title}:
              </div>
              <div>
                {value.values}
              </div>
            </div>
          )
        })}
      </div>
      <div className="message text-body-2">
        <div className="custom-message">{lastItem}</div>
        <div>Please retain receipt !</div>
      </div>
      <div className="date-time-container flex-row flex-row-space-between text-body-1">
        <div className="time">
          {time}
        </div>
        <div className="date">
          {date}
        </div>
      </div>
    </div>
  )
}

const mapStateToProps = state => ({
  formValues: receiptChapterFormValuesSelector(state),
  isBold: receiptChapterBoldSelector(state)
})

export default connect(mapStateToProps)(Receipt)

import React from 'react'
import PropTypes from 'prop-types'

import { translate } from '../../i18n/i18n'
import {CONDITIONS, FieldType} from '../../utils/constants'
import { checkPublicOwnerField, getValidityName, toBoolean } from '../../utils/application-form-utils'

const renderFieldComponent = ({ idx, field, value }) => {
  const { type, validityType } = field

  let label = translate(field.label)
  let valueToDisplay
  switch (type) {
    case FieldType.BOXED_RADIO_BTNS: {
      valueToDisplay = translate(value)
      break
    }
    case FieldType.TEXT: {
      valueToDisplay = value
      label = value && label
      break
    }
    case FieldType.VERTICAL_RADIO_BTNS: {
      valueToDisplay = translate(value)
      break
    }
    case FieldType.BOXED_CHECKBOX: {
      valueToDisplay = checkPublicOwnerField(field.label, value,  translate) || null
      label = checkPublicOwnerField(field.label, value,  translate) ? translate('appForm.review.statusPublicOwner') : null
      break
    }
    case FieldType.CHECKBOX: {
      label = null
      break
    }
    case FieldType.DROPDOWN: {
      label = value ? label : null
      valueToDisplay = value ? translate(value) : null
      break
    }
    case FieldType.BOXED_CHECKBOX_GROUP: {
      label = null
      valueToDisplay = null
      break
    }
    case FieldType.HORIZONTAL_RADIO_BTNS: {
      valueToDisplay = translate(value)
      break
    }
    case FieldType.DATE: {
      valueToDisplay = value
      break
    }
    case FieldType.TEXT_BOLD: {
      valueToDisplay = value
      break
    }
    case FieldType.TITLE: {
      valueToDisplay = label.toUpperCase()
      break
    }
    case FieldType.WARNING: {
      valueToDisplay = null
      label = null
      break
    }
    case FieldType.VALIDATE: {
      valueToDisplay = toBoolean(value) ? CONDITIONS.YES : CONDITIONS.NO
      label = getValidityName(validityType)
      break
    }
    case FieldType.TEXT_BOLD_STATUS: {
      return
    }
    
    default: {
      valueToDisplay = value
    }
  }

  return (
    <div className="read-field-row">
      <div ></div>
      {type !== FieldType.TITLE && label && <div className="label d-grey text-body-1 regular">{label}</div>}
      <div className="value bold text-body-2">{valueToDisplay}</div>
    </div>
  )
}

renderFieldComponent.propTypes = {
  idx: PropTypes.number,
  field: PropTypes.object,
  value: PropTypes.any,
}

export const ReadFieldRow = ({ idx, field, value }) => (
  <div className={field.type === FieldType.TITLE ? 'text-body-1 bold a-lighter' : 'form-field-row-read'}>
    { renderFieldComponent({ idx, field, value }) }
  </div>
)

ReadFieldRow.propTypes = {
  idx: PropTypes.number,
  field: PropTypes.object,
  value: PropTypes.any,
}


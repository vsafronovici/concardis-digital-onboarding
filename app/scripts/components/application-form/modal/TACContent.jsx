import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { resources } from '../../../utils/vf-utils'

const ID = 'TAC-iframe'

export class TACContent extends Component {
  static propTypes = {
    enableBtn: PropTypes.func,
    show: PropTypes.bool,
    height: PropTypes.number,
    lang: PropTypes.string,
  }

  componentDidMount() {
    const { enableBtn, show } = this.props
    const iframe = document.getElementById(ID).contentWindow

    iframe.onscroll = function(e) {
      const height = document.getElementById(ID).offsetHeight
      const iframeHeight = document.querySelector('iframe').contentWindow.document.querySelector("html").scrollHeight

      if (show && (height + iframe.pageYOffset) === iframeHeight) {
        enableBtn()
      }
    }
  }

  render() {
    const { height, lang } = this.props
    return (
      <iframe
        src={resources.TAD[lang]}
        title={ID}
        id={ID}
        height={height}
        width="100%"
        scrolling="true"
        frameBorder="0"
      >Loading...
      </iframe>
    )
  }
}

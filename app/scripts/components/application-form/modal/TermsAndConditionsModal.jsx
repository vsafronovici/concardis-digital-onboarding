import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Modal, Button } from 'antd'
import PropTypes from 'prop-types'
import { translate } from '../../../i18n/i18n'
import { i18nLangSelector } from '../../../selectors/i18n-selector'
import { TACContent } from './TACContent'
import { FieldCheckbox } from '../../common/FieldCheckbox'
import { tacSelector } from '../../../selectors/application-form-selector'
import { closeTACModal, agreeTAC, setSEPATAC } from '../../../actions/application-form-action'
import { isInternalUser } from '../../../utils/vf-utils'

const Footer = ({ disabled, onClose, onOk, setSEPATACAction, TAC: { sepa, readOnly } }) => {

  return (
    <div>
      {!readOnly && (
        <div className="tac_sepa">
          <FieldCheckbox label="TAC.sepa.label" onChange={setSEPATACAction} value={sepa} disabled={isInternalUser} />
        </div>
      )}
      <div className="tac-footer">
        {!readOnly && <div className="tac-footer-help">{translate('appForm.TAD.modal.footer.help')}</div>}
        <div className="tac-footer-btns">
          {!readOnly && <Button className="medium" onClick={onOk} disabled={disabled}>{translate('appForm.TAD.modal.footer.ok.btn')}</Button>}
          <Button className="medium" onClick={onClose}>{!readOnly ? translate('appForm.TAD.modal.footer.cancel.btn') : 'Close'}</Button>
        </div>
      </div>
    </div>
  )
}

Footer.propTypes = {
  onOk: PropTypes.func,
  disabled: PropTypes.bool,
}

export class TermsAndConditionsModal extends Component {
  state = {
    seen: false
  }

  static propTypes = {
    id: PropTypes.string,
    onOk: PropTypes.func,
    lang: PropTypes.string,
    show: PropTypes.bool,
    onClose: PropTypes.func
  }

  enableBtn = () => {
    this.setState({ seen: true })
  }

  render() {
    const { TAC: { sepa, show }, onClose, lang } = this.props
    console.log('TermsAndConditionsModal', this.props)

    const height = window.innerHeight - 400

    return (
      <Modal
        title={translate('appForm.TAD.modal.title')}
        visible={show}
        onCancel={onClose}
        footer={<Footer disabled={!(this.state.seen && sepa)} { ...this.props } />}
        width="90%"
        style={{ top: 50 }}
        bodyStyle={{ height, padding: 0 }}
        centered
      >
        <TACContent
          height={height} enableBtn={this.enableBtn} lang={lang}
          show={show}
        />
      </Modal>
    )
  }
}

const mapStateToProps = state => ({
  lang: i18nLangSelector(state),
  TAC: tacSelector(state),
})

const mapDispatchToProps = ({
  onOk: agreeTAC,
  onClose: closeTACModal,
  setSEPATACAction: setSEPATAC
})

export default connect(mapStateToProps, mapDispatchToProps)(TermsAndConditionsModal)

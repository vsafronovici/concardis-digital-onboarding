import React, { Component } from 'react'
import { Icon, Input } from 'antd'
import { translate } from '../../../i18n/i18n'

export const ValidateIBANContent = ({ apiValidatedFields, status, validateFields }) => {
  return (
    <div className="modal-validate-content">
      {
        status && apiValidatedFields ? (
          <div className="modal-success-message">
            <Icon type="check-circle" />
            <div>{"IBAN is valid"}</div>
          </div>
        ): (
          <div className="modal-error-message">
            <div><Icon type="exclamation-circle" /></div>
            <div>{"IBAN is invalid"}</div>
          </div>
        )
      }
    </div>
  )
}

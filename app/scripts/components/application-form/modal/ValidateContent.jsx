import React, { Component } from 'react'
import { Icon, Input } from 'antd'
import { translate } from '../../../i18n/i18n'
import { RESPONSE_STATUS } from "../../../utils/constants";

export const ValidateContent = ({ apiValidatedFields, status, validateFields, apiValidationStatus }) => {
  const getLabel = fieldName => `${fieldName.split('Section')[0]}_label`
  const getValue = value => translate(value).indexOf('missing') === -1 ? translate(value) : value
  return (
    <div className="modal-validate-content">
      {
        status && apiValidatedFields && apiValidationStatus === RESPONSE_STATUS.OK ? (
          <div className="modal-success-message">
            <div className="flex-row">
              <Icon type="check-circle" theme="filled" style={{ marginRight: "1rem" }}/>
              <label className="big bold success">{'The address is valid'}</label>
            </div>
            {
              Object.keys(apiValidatedFields).map((key, index) => {
                return (
                  <div className="modal-item" key={index}>
                    <div className="label">{translate(getLabel(key))}</div>
                    <div className="value">{getValue(apiValidatedFields[key])}</div>
                  </div>
                )
              })
            }
          </div>
        ): (
          <div className="modal-success-message error">
            <div className="flex-row">
              <Icon type="close-circle" theme="filled" style={{ marginRight: "1rem" }}/>
              <label className="big bold error">{'The address is invalid'}</label>
            </div>
            {
              Object.keys(apiValidatedFields).map((key, index) => {
                return (
                  <div className="modal-item" key={index}>
                    <div className="label">{translate(getLabel(key))}</div>
                    <div className="value">{getValue(apiValidatedFields[key])}</div>
                  </div>
                )
              })
            }
          </div>
        )
      }
    </div>
  )
}

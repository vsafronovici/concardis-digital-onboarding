import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Modal, Button } from 'antd'
import PropTypes from 'prop-types'
import { i18nLangSelector } from '../../../selectors/i18n-selector'
import { apiValidationSelector } from '../../../selectors/application-form-selector'
import { ValidateContent } from './ValidateContent'
import { ValidateIBANContent } from './ValidateIBANContent'
import { Loader } from "../../Loader"
import { translate } from "../../../i18n/i18n";

const isIBAN = type => type === 'IBAN'

const Footer = ({ status, onOk }) => {
  return (
    <div className="tac-footer">
      <div className="tac-footer-r">
        <Button onClick={() => onOk()} className="medium">{status ? translate('appForm.modal.button.confirm') : translate('appForm.modal.button.reVerify') }</Button>
      </div>
    </div>
  )
}

Footer.propTypes = {
  onOk: PropTypes.func,
  disabled: PropTypes.bool,
}

export class ValidateModal extends Component {
  static propTypes = {
    id: PropTypes.string,
    onOk: PropTypes.func,
    lang: PropTypes.string,
    show: PropTypes.bool,
    onClose: PropTypes.func
  }

  render() {
    const {
      id, show, onClose, onOk, lang, status,
      apiValidatedFields, validateFields, apiValidationStatus, type } = this.props
    const height = window.innerHeight - 200
    return (
      <Modal
        className="modal-valid"
        visible={show}
        onCancel={onClose}
        footer={[<Footer key={id} onOk={onOk} status={status}/>]}
        style={{ top: 50 }}
        bodyStyle={{ height, padding: 0 }}
        centered
      >
        {!apiValidatedFields && <Loader/>}
        {apiValidatedFields && !isIBAN(type) &&
          <ValidateContent
            apiValidationStatus={apiValidationStatus}
            apiValidatedFields={apiValidatedFields}
            status={status}
            validateFields={validateFields}
            apiValidationStatus={apiValidationStatus}
          />
        }
        {apiValidatedFields && isIBAN(type) &&
          <ValidateIBANContent
            apiValidationStatus={apiValidationStatus}
            apiValidatedFields={apiValidatedFields}
            status={status} validateFields={validateFields}
            apiValidationStatus={apiValidationStatus}
          />
        }
      </Modal>
    )
  }
}

const mapStateToProps = state => ({
  lang: i18nLangSelector(state)
})

export default connect(mapStateToProps)(ValidateModal)

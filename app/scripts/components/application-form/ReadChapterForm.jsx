import React from 'react'
import PropTypes from 'prop-types'
import { Button, Icon } from 'antd'
import { createInitialValues, fieldsToShow } from '../../utils/application-form-utils'
import { StaticIcon } from '../common/Icon';
import { ReadFieldRow } from './ReadFieldRow'
import { translate } from '../../i18n/i18n'

export const ReadChapterForm = ({ chapter, index, editSection }) => {
  const serverValues = createInitialValues(chapter)
  const fieldsToDisplay = fieldsToShow(chapter, serverValues)

  return (
    <div className="read-chapter-form">
      <div className="read-field-row-container">
        <div className="chapter-title flex-row justify-content-between">
          <div className='flex-row'>
            <h6 className="bold">{translate(chapter.title)}</h6>
            <Button onClick={() => editSection(index)} className="image-btn">
              <i className="icon-ic-edit d-indigo"></i>
              Edit
            </Button>
          </div>
          <div >
            <Icon type="check-circle" theme="filled" />
          </div>
        </div>
        { fieldsToDisplay.map((field, idx) => <ReadFieldRow key={field.name + idx} field={field} idx={idx} value={serverValues[field.name]} />) }
      </div>
    </div>
  )
}

ReadChapterForm.propTypes = {
  chapter: PropTypes.any,
  index: PropTypes.number,
  editSection: PropTypes.func,
}

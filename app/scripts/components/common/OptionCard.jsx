import React from 'react'
import PropTypes from 'prop-types'
import cn from 'classnames'
import { Button, Tooltip } from 'antd'
import { connect } from 'react-redux'

import { translate } from './../../i18n/i18n'
import {checkFeaturesIcon, getImageResources} from '../../utils/function-utils'
import { StaticIcon } from './Icon'
import { resources } from '../../utils/vf-utils'
import { ConfiguratorPageStep, Routes } from '../../utils/constants'
import { routeSelector } from "../../selectors/router-selectors";
import OrderDetailsContainer from './OrderDetails'
import { reviewModeSelector } from "../../selectors/application-form-selector";

const AccesoriesTooltip = ({ includedItems, title }) => {

  const content = () => {
    return (
      <div className="white">
        {includedItems && includedItems.map((item, index) => (
          <div key={index} className="text-body-caption d-grey" style={{letterSpacing: '0.5'}}>
            {item.quantity.value > 1 && <span>{item.quantity.value}x</span>} {item.description}
          </div>
        ))}
      </div>
    )
  }

  return (
    <div>
        <Tooltip
          trigger="hover"
          className="bold d-grey text-body-caption hover-pointer"
          title={content()}
        >
          <div style={{marginBottom: '10px'}}><i className="icon-ic-forward"/>{translate(title)}</div>
        </Tooltip>
    </div>
  )
}

class OptionCard extends React.Component {
  static propTypes = {
    readonly: PropTypes.bool,
    active: PropTypes.bool,
    id: PropTypes.string,
    name: PropTypes.string,
    description: PropTypes.string,
    price: PropTypes.string,
    items: PropTypes.array,
    onChooseOption: PropTypes.func,
    features: PropTypes.array,
    includedItems: PropTypes.array
  }

  static defaultProps = {
    readonly: false
  }

  chooseOption = e => {
    const {id, onChooseOption} = this.props
    onChooseOption(id)
  }

  isSelectPackage = () => {
    const { route } = this.props

    return route === Routes.STEP2
  }

  render() {
    const { active, name, description, features, readonly, includedItems, goToStep, route, reviewMode } = this.props
    const elProps = {
      className: cn('option-card', {active, readonly})
    }

    if (!readonly) {
      elProps.onClick = this.chooseOption
    }
    return (
      <div {...elProps} >
        <div className="oc-subtitle bold text-body-1">{description}</div>
        <div className="oc-all-content-container">
          <div className="oc-top">
            <div className="container-image">
              <img src={getImageResources('bitmap')} alt={'bitmap'}/>
            </div>
          </div>
          <div className="content-footer-container">
            <div className="oc-content">
              <div className="oc-title bold"><h5>{name}</h5></div>
              <div className="oc-items">
                <h6 className="bold r-orange">{translate('configurator.step2.bundle.cardsTitle').toUpperCase()}</h6>
                {
                  features && features.map((item, idx) => (
                    <div className="oc-item" key={idx} style={{display: 'inline-block'}}>
                      <span>{item.description ? <StaticIcon name={checkFeaturesIcon(item.description)}/> :
                        <StaticIcon name={checkFeaturesIcon(item.name)}/>}</span>
                    </div>
                  ))
                }
              </div>
              <div className="oc-footer">
                <AccesoriesTooltip
                  title={'configurator.optionCards.accIncluded'}
                  includedItems={includedItems}
                />
                {this.isSelectPackage() &&
                <Button className="medium oc-button" onClick={() => goToStep(ConfiguratorPageStep.STEP3)}>
                  {translate('configurator.step2.select')}
                </Button>
                }
              </div>

            </div>

          </div>
        </div>
        {reviewMode && <OrderDetailsContainer />}
      </div>
    )
  }
}

const mapStateToProps = state => ({
  route: routeSelector(state),
  reviewMode: reviewModeSelector(state),
})

export default connect(mapStateToProps, null)(OptionCard)

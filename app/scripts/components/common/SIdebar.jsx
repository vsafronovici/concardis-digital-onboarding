import React from 'react'
import { connect } from 'react-redux'
import TAC from "../application-form/TAC"
import { translate } from "../../i18n/i18n"
import { isMobileSelector, isSmallScreenSelector} from "../../selectors/screen-selectors";
import { getImageResources, isMobileQuote, isMobileContent } from '../../utils/function-utils'
import { routeSelector, isAppFormSelector } from "../../selectors/router-selectors";
import { ConfiguratorPageStep } from '../../utils/constants'
import { goToStep } from "../../actions/configurator-action";


const sitebarBg = getImageResources('rawpixel-com-small')

class Sidebar extends React.Component {
  state = {
    isOpen: false
  }

  toggleBundle = () => {
    this.setState({
      isOpen: !this.state.isOpen
    })
  }

  render() {
    const { renderComponent, showTAC, showTitle, sidebarTitle, route, backButton, goToStep, isMobile, isAppForm, isSmallLogo } = this.props
    const { isOpen } = this.state
    return (
      <div className="sidebar-container" style={{backgroundImage: `url(${sitebarBg})`}}>
        <div className="sidebar-content">
          <div className="sidebar-component">
            <div className="sidebar-logo">
              <i className={`icon-concardis-logo${!(isAppForm && isSmallLogo) ? '' : '-small'}`} />
            </div>
            {backButton && (
              <div className="white text-body-1 back-button" onClick={() => goToStep(ConfiguratorPageStep.STEP1)}>
                <i className="icon-ic-diagram-arrow-left back-arrow" />{translate('configurator.step2.sidebar.back')}
              </div>
            )}
            {((!isMobile && showTitle) || (isMobile && isMobileContent(route))) &&  (
              <div className="sidebar-title r-orange bold flex-row">
                <h5>{translate(sidebarTitle)}</h5>
              </div>
            )}
            {isMobileQuote(route) && !isAppForm && isMobile && (
                <div onClick={this.toggleBundle} className="sidebar-toggle-bundle r-orange">
                  <i className={`icon-ic-${isOpen ? 'down' : 'forward'}`} /> View Selected bundle
                </div>
            )}
            {(!isMobile || isMobile && (isOpen || isMobileContent(route))) && renderComponent}
            <div className="sidebar-tac">{showTAC && <TAC />}</div>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  isSmallLogo: isSmallScreenSelector(state),
  route: routeSelector(state),
  isMobile: isMobileSelector(state),
  isAppForm: isAppFormSelector(state),
})

const mapDispatchToProps = ({
  goToStep
})

export default connect(mapStateToProps, mapDispatchToProps)(Sidebar)

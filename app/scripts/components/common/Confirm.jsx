import React from 'react'
import { Modal, Button } from 'antd'
import { StaticIcon } from "./Icon";
import { translate } from "../../i18n/i18n";

export const ConfirmModal = props => {

  const { title, content, onOk, onCancel, visible } = props

  return (
    <div className="d-grey">
      <Modal
        title={null}
        content={content}
        onOk={onOk}
        onCancel={onCancel}
        visible={visible || false}
        confirmLoading={true}
        footer={null}
        centered={true}
        closable={false}
      >
        <div className="flex-row">
          <div className="icon-container">
            <StaticIcon name="ic-info-2"/>
          </div>
          <div className="content-container">
            <div className="confirm-title bold"><h5>{translate('configurator.modal.confirm.title')}</h5></div>
            <div className="confirm-content text-body-2 regular">{translate('configurator.modal.confirm.content')}</div>
          </div>
        </div>
        <div className="flex-row justify-content-end">
          <Button className="small secondary" onClick={onCancel}>{translate('configurator.modal.button.cancel')}</Button>
          <Button className="small" onClick={onOk}>{translate('configurator.modal.button.proceed')}</Button>
        </div>
      </Modal>
    </div>
  )
}

import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Input } from 'antd'
import { connect } from 'react-redux'
import { translate } from './../../i18n/i18n'
import { FieldTooltip } from './FieldTooltip'
import { getNotRequired, INPUT_BOLD_PREFIX, InputTextBoldStatus } from '../../utils/application-form-utils'
import { receiptChapterBoldSelector } from "../../selectors/application-form-selector";

const RulesBold = {
  MAX_BOLD: 12,
  MAX_REGULAR: 24,
  WEIGHT_BOLD: '800',
  WEIGHT_REGULAR: '400'
}

const isTextBold = str => str && str.length <= RulesBold.MAX_BOLD


class FieldTextBold extends Component {

  state = {
    cachedValue: undefined
  }

  componentDidMount() {
    const { value, isBold } = this.props
    this.changeCachedvalue(value)

    if (isBold === undefined) {
      this.toggleBoldStatus(isTextBold(value))
    }
  }

  changeCachedvalue = value => {
    this.setState({
      ...this.state,
      cachedValue: value
    })
  }

  toggleBoldStatus = isBold => {
    const { input: { name }, changeFormValue } = this.props
    changeFormValue(`${INPUT_BOLD_PREFIX}${name}`, isBold ? InputTextBoldStatus.BOLD : InputTextBoldStatus.NORMAL)
  }

  handleBoldClick = () => {
    const { onChange, isBold, value } = this.props
    const { cachedValue } = this.state

    this.toggleBoldStatus(!isBold)

    switch (true) {
      case cachedValue && isBold: {
        onChange(cachedValue)
        break
      }
      case !isBold && !isTextBold(value): {
        const subValue = value.substring(0, RulesBold.MAX_BOLD)
        onChange(subValue)
        break
      }
      default: //do nothing
    }
  }

  handleInputChange = value => {
    const { isBold, onChange } = this.props
    const isTBold = isTextBold(value)

    onChange(value)

    this.changeCachedvalue(value)

    if (isBold !== isTBold) {
      this.toggleBoldStatus(isTBold)
    }
  }

  render() {
    const { label, helpText, value, hint, description, onBlur, validationRules, isBold } = this.props

    return (
      <div className="field-text-bold">
        <div className="container-labels flex-row">
          <label>{translate(label)}</label> {getNotRequired(validationRules) && <span>{translate('(optional)')}</span>} {helpText && <div><FieldTooltip title={helpText} /></div>}
        </div>
        <div className="help">
          {description && translate(description)}
        </div>
        <div className="field flex-row">
            <div className="field-input">
              <Input
                style= { { fontWeight: isBold ? RulesBold.WEIGHT_BOLD : RulesBold.WEIGHT_REGULAR } }
                value={value}
                placeholder={hint && translate(hint)}
                onBlur={onBlur}
                onChange={event => this.handleInputChange(event.target.value)}
              />
            </div>
            <div className="field-button">
              <span onClick={this.handleBoldClick}>B</span>
            </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  isBold: receiptChapterBoldSelector(state)
})

export default connect(mapStateToProps)(FieldTextBold)

FieldTextBold.propTypes = {
  label: PropTypes.string,
  onChange: PropTypes.func,
  helpText: PropTypes.string,
  value: PropTypes.any,
  hint: PropTypes.string,
  description: PropTypes.string,
  onBlur: PropTypes.func,
  validationRules: PropTypes.array
}

import React from 'react'
import PropTypes from 'prop-types'
import { Icon, Tooltip } from 'antd'
import { translate } from '../../i18n/i18n'

export const FieldTooltip = ({ title }) =>
  <Tooltip trigger="hover" title={<div className="d-grey regular">{translate(title)}</div>}>
    <div className="tooltip-field"><Icon type="info-circle" theme="filled" /></div>
  </Tooltip>

FieldTooltip.propTypes = {
  title: PropTypes.string
}

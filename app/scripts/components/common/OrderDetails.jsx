import React from 'react'
import { connect } from 'react-redux'
import { translate } from "../../i18n/i18n";
import { extraFieldsTotalSelector} from "../../selectors/package-configure-selector";
import { generalFormatNumber, objectToArrayKeyValue } from "../../utils/function-utils";
import { quoteSelector } from "../../selectors/package-configure-selector";

const OrderDetailsContainer = props => {

  const { extraFieldsTotal, quote: { pricePerTransactionMap, terminal } } = props

  return (
    <div className="order-container">
      <div className="order-title"><h5 className="d-grey bold">{translate('configurator.orderDetails.title')}</h5></div>
      <div className="order-item">
        <div className="order-item-title">{translate('configurator.orderDetails.recurring')}</div>
        <div className="order-item-price">{terminal.price.valuePerMonth} EUR</div>
      </div>
      {pricePerTransactionMap && objectToArrayKeyValue(pricePerTransactionMap).map(({ key, value }, index) => {
        return (
          <div className="order-item" key={index}>
            <div className="order-item-title">{translate(key)}</div>
            <div className="order-item-price">{value}</div>
          </div>
        )
      })}
      <div className="order-item">
        <div className="order-item-title">{translate('configurator.orderDetails.oneOff')}</div>
        <div className="order-item-price">{generalFormatNumber(extraFieldsTotal)} EUR</div>
      </div>
    </div>
  )
}

const mapStateToProps = state => ({
  extraFieldsTotal: extraFieldsTotalSelector(state),
  quote: quoteSelector(state),
})

export default connect(mapStateToProps)(OrderDetailsContainer)

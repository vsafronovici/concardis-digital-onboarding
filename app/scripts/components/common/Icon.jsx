import React from 'react';
import { resourceIcons } from '../../utils/vf-utils'

export const StaticIcon = ({ name, styling = ''}) => {
  return <img className={styling} src={`${resourceIcons}/media/icons/${name}.png`} alt={name}/>
}

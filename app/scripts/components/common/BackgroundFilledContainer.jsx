import React from 'react'
import { Row, Col } from 'antd'
import { getImageResources } from '../../utils/function-utils'

const BackgroundFilledContainer = ({ Content }) => {
  const backGroundImageUrl = getImageResources('rawpixel-com-580218-unsplash')

  const styles = {
    backgroundImage: `url(${backGroundImageUrl})`,
  }

  return (
    <div  className='scenario-main-container' style={{...styles}}>
      <div className="scenario-page-logo">
        <i className="icon-concardis-logo"></i>
      </div>
      <Row className="content-wrapper">
        <Col xl={{span:16, offset: 4}} md={{span: 22, offset: 1}} xs={{span: 22, offset: 1}} className="container-white">
          <div style={{ width: '80%', margin: '70px auto' }}>
            <Content />
          </div>
        </Col>
      </Row>
    </div>
  )
}

export default BackgroundFilledContainer

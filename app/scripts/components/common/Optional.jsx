import React from 'react'
import { translate } from '../../i18n/i18n'

export const Optional = props => <span className="optional">{translate('field_optional')}</span>

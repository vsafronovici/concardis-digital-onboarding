import React, { Component } from 'react'
import { connect } from 'react-redux'
import { equals } from 'ramda'
import { Icon, Button } from 'antd'

import { change as changeFieldValue, getFormValues, getFormSyncErrors, touch } from 'redux-form'
import { toBoolean } from '../../utils/application-form-utils'
import { apiValidateFields, setValidationStatus, toggleValidationModal, initModal } from '../../actions/application-form-action'
import {
  modalsSelector,
  apiValidationSelector,
  validityModalSelector,
  apiValidationStatusSelector,
  findFieldsForValidateSelector,
} from '../../selectors/application-form-selector'
import ValidateModal from '../application-form/modal/ValidateModal'
import { translate } from './../../i18n/i18n'

const isSuccessStatus = (status) => {
  switch(status) {
    case 'true': return {
      iconClassName: 'success',
      icon: 'check-circle',
    };
    case 'false': return {
      iconClassName: 'error',
      icon: 'exclamation-circle',
    }
    default: return {
      iconClassName: 'disabled',
      icon: '',
    }
  }
}

const isValidStatus = status => status === "ok"
const isIBAN = type => type === 'IBAN'
const RenderControlIcon = ({ status }) => {
  return (
    <div className={`validity-status ${isSuccessStatus(status) && isSuccessStatus(status).iconClassName}`}>
      <Icon type={isSuccessStatus(status) && isSuccessStatus(status).icon} theme="filled"/>
    </div>
  ) 
}

const getValuesforValidate = (fieldsToValidate = [], formValues = {}) =>
  fieldsToValidate.reduce((acc, { name }) => ({ ...acc, [name]: formValues[name] }), {})

class FieldCheckBoxValidate extends Component {
  state = {
    disableButton: undefined
  }

  componentDidMount() {
    const { setValidationStatusAction, input, initModalAction, validityType } = this.props
    initModalAction(validityType)

    if (toBoolean(input.value)) {
      this.setState({ disableButton: true })
    }
  }

  componentWillReceiveProps({ formValues }) {
    if (((toBoolean(this.props.input.value)) && !equals(getValuesforValidate(this.props.validateFields, formValues), getValuesforValidate(this.props.validateFields, this.props.formValues)))) {
      this.handleStatus(false)
    }
  }

  handleStatus = (value) => {
    // if true button need to be disabled
    this.setState({ disableButton: value })
    this.props.input.onChange(value.toString())
  }

  toggleModal = () => {
    const { toggleValidationModalAction, validityType } = this.props
    toggleValidationModalAction(validityType)
  }

  onOk = () => {
    const {
      validityType,
      validateFields,
      apiValidatedFields,
      apiValidationStatus,
      changeFieldValueAction,
      setValidationStatusAction,
      toggleValidationModalAction,
      meta: { form }
    } = this.props

    if (isValidStatus(apiValidationStatus)) {
      !isIBAN(validityType) && Object.keys(apiValidatedFields)
        .map(field => changeFieldValueAction(form, field, apiValidatedFields[field]))
      
      this.handleStatus(true)
      this.toggleModal()
    } else {
      this.handleStatus(false)
      this.toggleModal()
    }
  }

  validateApiSction = () => {
    const { syncErrors, validateFields, meta: { form }, apiValidateFieldsAction, touchFieldAction, formValues, validityType } = this.props

    const isValid = this.isValidFields(syncErrors, validateFields)

    if (isValid) {
      apiValidateFieldsAction({ validateFields, formValues, type: validityType })
    } else {
      touchFieldAction(form, ...validateFields)
    }
  }

  isValidFields = (errors, validateFields) => validateFields.every((filed, index) => !errors[filed.name])

  render() {
    const { disableButton } = this.state
    const {
      show,
      formValues,
      validityType,
      validateFields,
      apiValidatedFields,
      apiValidationStatus,
      apiValidateFieldsAction,
      label,
      input: { value }
    } = this.props
    
    return (
      <div className="flex-row control-container">
        <div className="flex-row checkbox-validate">
          <RenderControlIcon status={value} />
          <label className="bold">{translate(label)}</label>
        </div>
        {/* TODO: change label for button in further future */}
        {
          disableButton ? null : (
            <Button
              className="small"
              onClick={() => this.validateApiSction()}
              disabled={disableButton}
            >{'Validate'}</Button>
          )
        }
        <ValidateModal
          id={validityType}
          show={show}
          status={isValidStatus(apiValidationStatus)}
          onClose={this.toggleModal}
          apiValidatedFields={apiValidatedFields}
          onOk={this.onOk}
          type={validityType}
          validateFields={validateFields}
          apiValidationStatus={apiValidationStatus}
        />
      </div>
    )
  }
}

const mapStateToProps = (state, props) => ({
  show: modalsSelector(state)[props.validityType],
  formValues: getFormValues(props.meta.form)(state),
  syncErrors: getFormSyncErrors(props.meta.form)(state),
  apiValidationStatus: apiValidationStatusSelector(state),
  validateFields: findFieldsForValidateSelector(props.validityType, state),
  apiValidatedFields: apiValidationSelector(state)
})

const mapDispatchToProps = ({
  apiValidateFieldsAction: apiValidateFields,
  initModalAction: initModal,
  touchFieldAction: touch,
  setValidationStatusAction: setValidationStatus,
  toggleValidationModalAction: toggleValidationModal,
  changeFieldValueAction: changeFieldValue
})

export default connect(mapStateToProps, mapDispatchToProps)(FieldCheckBoxValidate)

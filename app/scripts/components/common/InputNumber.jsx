import React, { Component } from 'react'
import { ScreenType } from "../../utils/constants";

class InputCounter extends Component {
  state = {
    value: 0 || this.props.defaultValue,
    disabled: false
  }

  incrementCount = () => {
    const { max } = this.props

    if (this.state.value >= max) {
      this.setState(state => ({ ...state, disabled: true }))
      return
    }
    
    this.setState((state, { onChange }) => {
      onChange(state.value + 1)

      return { ...state, value: state.value + 1, disabled: false }
    })
  }

  decrementValue = () => {
    const { min } = this.props 

    if (this.state.value <= min) {
      this.setState(state => ({ ...state, disabled: true }))
      return
    }

    this.setState((state, { onChange }) => {
      onChange(state.value - 1)

      return { ...state, value: this.state.value - 1, disabled: false }
    })
  }

  render() {
    const { screenType } = this.props
    return (
      <div className={screenType === ScreenType.XS || ScreenType.SM ? `flex-row` :`flex-row-space-between`}>
        <button className={`count count-button ${this.state.disabled ? 'disabled': ''}`} onClick={() => this.decrementValue()}>-</button>
        <div className="count count-value">{this.props.value || this.state.value}</div>
        <button className={`count count-button ${this.state.disabled ? 'disabled': ''}`} onClick={() => this.incrementCount()}>+</button>
      </div>
    )
  }
}

export default InputCounter

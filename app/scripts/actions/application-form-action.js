import { APPLICATION_FORM } from './types'

export const openTACModal = payload => ({
  type: APPLICATION_FORM.OPEN_TAC_MODAL,
  payload
})

export const closeTACModal = () => ({
  type: APPLICATION_FORM.CLOSE_TAC_MODAL
})

export const toggleValidationModal = payload => ({
  type: APPLICATION_FORM.TOGGLE_VALIDATION_MODAL,
  payload
})

export const agreeTAC = () => ({
  type: APPLICATION_FORM.AGREE_TAC
})

export const setSEPATAC = payload => ({
  type: APPLICATION_FORM.SET_SEPA_TAC,
  payload
})

export const updateCommercialsTCReq = payload => ({
  type: APPLICATION_FORM.UPDATE_COMMERCIALS_TC_REQ,
  payload
})

export const updateCommercialsTCRes = payload => ({
  type: APPLICATION_FORM.UPDATE_COMMERCIALS_TC_RES,
  payload
})

export const getFormMetaReq = () => ({
  type: APPLICATION_FORM.GET_FORM_META_REQ
})

export const getFormMetaRes = payload => ({
  type: APPLICATION_FORM.GET_FORM_META_RES,
  payload
})

export const goToSection = payload => ({
  type: APPLICATION_FORM.GO_TO_SECTION,
  payload
})

export const goToNextSection = payload => ({
  type: APPLICATION_FORM.GO_TO_NEXT_SECTION,
  payload
})

export const goToReviewMode = () => ({
  type: APPLICATION_FORM.GO_TO_REVIEW_MODE
})

export const save = payload => ({
  type: APPLICATION_FORM.SAVE,
  payload
})

export const saveReq = payload => ({
  type: APPLICATION_FORM.SAVE_REQ,
  payload
})

export const saveRes = payload => ({
  type: APPLICATION_FORM.SAVE_RES,
  payload
})

export const saveAndClose = payload => ({
  type: APPLICATION_FORM.SAVE_AND_CLOSE,
  payload
})

export const saveAndCloseReq = payload => ({
  type: APPLICATION_FORM.SAVE_AND_CLOSE_REQ,
  payload
})

export const saveAndCloseRes = payload => ({
  type: APPLICATION_FORM.SAVE_AND_CLOSE_RES,
  payload
})

export const submit = () => ({
  type: APPLICATION_FORM.SUBMIT
})

export const submitReq = () => ({
  type: APPLICATION_FORM.SUBMIT_REQ
})

export const submitRes = payload => ({
  type: APPLICATION_FORM.SUBMIT_RES,
  payload
})

export const setValidationStatus = payload => ({
  type: APPLICATION_FORM.SET_VALIDATION_STATUS,
  payload
})
export const apiValidateFieldsReq = () => ({
  type: APPLICATION_FORM.API_VALIDATE_FIELDS_REQ
})

export const apiValidateFieldsRes = payload => ({
  type: APPLICATION_FORM.API_VALIDATE_FIELDS_RES,
  payload
})

export const apiValidateFields = payload => ({
  type: APPLICATION_FORM.API_VALIDATE_FIELDS,
  payload
})

export const apiValidateLoading = payload => ({
  type: APPLICATION_FORM.API_VALIDATE_LOADING,
  payload
})

export const initModal = payload => ({
  type: APPLICATION_FORM.INIT_VALIDATION_MODAL,
  payload,
})

export const finishPaymentDetails = payload => ({
  type: APPLICATION_FORM.FINISH_PAYMENT_DETAILS,
  payload
})

export const changedStatusEditing = payload => ({
  type: APPLICATION_FORM.CHANGED_STATUS_EDITING,
  payload
})

export const getSessionReq = () => ({
  type: APPLICATION_FORM.GET_SESSTION_ID_REQ,
})
export const getSessionRes = payload => ({
  type: APPLICATION_FORM.GET_SESSTION_ID_RES,
  payload
})

export const getIntegrationStatusReq = () => ({
  type: APPLICATION_FORM.GET_INTEGRATION_STATUS_REQ,
})

export const getIntegrationStatusRes = payload => ({
  type: APPLICATION_FORM.GET_INTEGRATION_STATUS_RES,
  payload
})

export const startIntegrationStatusPolling = () => ({
  type: APPLICATION_FORM.START_INTEGRATION_STATUS_POLLING,
})

export const stopIntegrationStatusPolling = payload => ({
  type: APPLICATION_FORM.STOP_INTEGRATION_STATUS_POLLING,
  payload
})

export const finalSubmitionStatus = payload => ({
  type: APPLICATION_FORM.FINAL_SUBMITION_STATUS,
  payload
})

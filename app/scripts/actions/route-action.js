import { ROUTE } from './types'

export const initRouter = payload => ({
  type: ROUTE.INIT_ROUTER,
  payload
})

export const setRoute = payload => ({
  type: ROUTE.SET_ROUTE,
  payload
})

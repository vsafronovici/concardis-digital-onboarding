import { SCREEN } from './types'

export const setSize = payload => ({
  type: SCREEN.SET_SIZE,
  payload
})

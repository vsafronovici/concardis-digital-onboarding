import { CONFIGURATOR } from './types'

export const getMetaStep1Req = () => ({
  type: CONFIGURATOR.GET_META_STEP1_REQ
})

export const getMetaStep1Res = payload => ({
  type: CONFIGURATOR.GET_META_STEP1_RES,
  payload
})

export const getMetaStep2Req = payload => ({
  type: CONFIGURATOR.GET_META_STEP2_REQ,
  payload
})

export const getMetaStep2Res = payload => ({
  type: CONFIGURATOR.GET_META_STEP2_RES,
  payload
})

export const changeFieldValue = (payload) => ({
  type: CONFIGURATOR.CHANGE_FIELD_VALUE,
  payload

})

export const goToStep = payload => ({
  type: CONFIGURATOR.GO_TO_STEP,
  payload
})

export const validateDiscountCode = payload => ({
  type: CONFIGURATOR.VALIDATE_DISCOUNT_CODE,
  payload
})

export const recalculateQuote = payload => ({
  type: CONFIGURATOR.RECALCULATE_QUOTE_REQ,
  payload
})

export const recalculateQuoteRes = payload => ({
  type: CONFIGURATOR.RECALCULATE_QUOTE_RES,
  payload
})

export const signupReq = payload => ({
  type: CONFIGURATOR.SIGNUP_REQ,
  payload
})

export const signupRes = payload => ({
  type: CONFIGURATOR.SIGNUP_RES,
  payload
})

export const getConfiguratorQuote = () => ({
  type: CONFIGURATOR.GET_QUOTE
})

export const setConfiguratorQuote = payload => ({
  type: CONFIGURATOR.SET_CONFIGURATOR_QUOTE,
  payload
})

import { createNamespace } from '../utils/namespace-util'

export const SCREEN = createNamespace('APP.SCREEN', {
  SET_SIZE: undefined,
})

export const ROUTE = createNamespace('APP.ROUTE', {
  INIT_ROUTER: undefined,
  SET_ROUTE: undefined,
})

export const APP = createNamespace('APP', {
  INIT_PAGE: undefined,
  CHANGE_LANGUAGE: undefined,
  LOAD_TRANSLATIONS_REQ: undefined,
  LOAD_TRANSLATIONS_RESP: undefined,
  SHOW_ALERT: undefined,
  HIDE_ALERT: undefined,
  API_FETCH_FAILED: undefined,
  CHANGE_ROOT_ON_SUBMIT_REQ: undefined
})


export const CONFIGURATOR = createNamespace('APP.CONFIGURATOR', {
  GET_META_STEP1_REQ: undefined,
  GET_META_STEP1_RES: undefined,
  CHANGE_FIELD_VALUE: undefined,
  GO_TO_STEP: undefined,
  GET_META_STEP2_REQ: undefined,
  GET_META_STEP2_RES: undefined,
  VALIDATE_DISCOUNT_CODE: undefined,
  RECALCULATE_QUOTE_REQ: undefined,
  RECALCULATE_QUOTE_RES: undefined,
  SIGNUP_REQ: undefined,
  SIGNUP_RES: undefined,
  GET_QUOTE: undefined,
  SET_CONFIGURATOR_QUOTE: undefined,
})

export const PACKAGE_CONFIGURE = createNamespace('APP.PACKAGE_CONFIGURE', {
  GET_QUOTE_REQ: undefined,
  GET_QUOTE_RES: undefined,
  CHANGE_PACKAGE_QNTY: undefined,
  CHANGE_DISCOUNT_CODE: undefined,
  APPLY_DISCOUNT: undefined,
  VALIDATE_DISCOUNT_CODE_REQ: undefined,
  VALIDATE_DISCOUNT_CODE_RES: undefined,
  APPLY_DISCOUNT_REQ: undefined,
  APPLY_DISCOUNT_RES: undefined,
  CHANGE_EXTRA_QNTY: undefined,
  SUBMIT_QUOTE: undefined,
  SUBMIT_QUOTE_REQ: undefined,
  SUBMIT_QUOTE_RES: undefined,
  CONFIRM_OFFER: undefined,
})

export const APPLICATION_FORM = createNamespace('APP.APPLICATION_FORM', {
  OPEN_TAC_MODAL: undefined,
  INIT_VALIDATION_MODAL: undefined,
  TOGGLE_VALIDATION_MODAL: undefined,
  API_VALIDATE_FIELDS: undefined,
  CLOSE_TAC_MODAL: undefined,
  AGREE_TAC: undefined,
  SET_SEPA_TAC: undefined,
  API_VALIDATE_FIELDS_REQ: undefined,
  SET_VALIDATION_STATUS: undefined,
  API_VALIDATE_FIELDS_RES:  undefined,
  UPDATE_COMMERCIALS_TC_REQ: undefined,
  UPDATE_COMMERCIALS_TC_RES: undefined,
  GET_FORM_META_REQ: undefined,
  GET_FORM_META_RES: undefined,
  GO_TO_SECTION: undefined,
  GO_TO_NEXT_SECTION: undefined,
  GET_REVIEW: undefined,
  SAVE: undefined,
  SAVE_REQ: undefined,
  SAVE_RES: undefined,
  SAVE_AND_CLOSE: undefined,
  SAVE_AND_CLOSE_REQ: undefined,
  SAVE_AND_CLOSE_RES: undefined,
  GO_TO_REVIEW_MODE: undefined,
  SUBMIT_REQ: undefined,
  SUBMIT_RES: undefined,
  SUBMIT: undefined,
  API_VALIDATE_LOADING: undefined,
  FINISH_PAYMENT_DETAILS: undefined,
  CHANGED_STATUS_EDITING: undefined,
  GET_SESSTION_ID_REQ: undefined,
  GET_SESSTION_ID_RES: undefined,
  GET_INTEGRATION_STATUS_REQ: undefined,
  GET_INTEGRATION_STATUS_RES: undefined,
  START_INTEGRATION_STATUS_POLLING: undefined,
  STOP_INTEGRATION_STATUS_POLLING: undefined,
  FINAL_SUBMITION_STATUS: undefined,
})



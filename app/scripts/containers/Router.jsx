import React from 'react'
import { connect } from 'react-redux'

import { PageContainers, Routes } from '../utils/constants'

import Step1 from './../components/configurator/Step1'
import Step2 from './../components/configurator/Step2'
import Step3 from './../components/configurator/Step3'
import Step4 from './../components/configurator/Step4'
import { routeSelector } from '../selectors/router-selectors'
import PackageConfigure from '../components/configurator/package-configure/PackageConfigure'
import ApplicationForm from '../components/application-form/ApplicationForm'
import ReviewYourApplicaiton from '../components/application-form/ReviewYourApplicaiton'
import ApplicationFormWrapper from './ApplicationFormWrapper'
import { ConfiguratorWrapper } from './ConfiguratorWrapper'
import { Loader } from '../components/Loader'
import { StartApplication } from '../components/configurator/package-configure/StartApplication'
import { ResumeApplication } from '../components/configurator/package-configure/ResumeApplication'
import { ExpiredQuote } from '../components/configurator/package-configure/ExpiredQuote'
import { SuccessSubmitPage } from '../components/application-form/SuccessSubmitPage'
import { ErrorSubmitPage } from '../components/application-form/ErrorSubmitPage'
import { LoadingSubmitScreen } from '../components/application-form/LoadingSubmitScreen'


const DefaultScreenWrapper = ({ Screen }) =>
  <div>
    <Screen />
  </div>

const getScreen = route => {
  switch (route) {
    case Routes.STEP1:
      return Step1
    case Routes.STEP2:
      return Step2
    case Routes.STEP3:
      return Step3
    case Routes.STEP4:
      return Step4
    case Routes.QUOTE_EXPIRED:
      return ExpiredQuote
    case Routes.EDIT_PACKAGE:
      return PackageConfigure
    case Routes.START_APPLICATION:
      return StartApplication
    case Routes.RESUME_APPLICATION:
      return ResumeApplication
    case Routes.APP_FORM:
      return ApplicationForm
    case Routes.REVIEW_APP_FORM:
      return ReviewYourApplicaiton
    case Routes.SUBMIT_APP_FORM_LOADER:
      return LoadingSubmitScreen
    case Routes.SUBMIT_APP_FORM_SUCCESS:
      return SuccessSubmitPage
    case Routes.SUBMIT_APP_FORM_ERROR:
      return ErrorSubmitPage
    default:
      return Loader
  }
}

const getScreenWrapper = (rootId, route) => {
  if (rootId === PageContainers.CONFIGURATOR_PAGE) {
    return ConfiguratorWrapper
  }

  switch (route) {
    case Routes.APP_FORM:
    case Routes.REVIEW_APP_FORM:
      return ApplicationFormWrapper
    default:
      return DefaultScreenWrapper
  }
}

class Router extends React.Component {
  render() {
    const { rootId, route} = this.props
    const ScreenWrapper = getScreenWrapper(rootId, route)
    const Screen = getScreen(route)
    return !route ? <Loader/> : <ScreenWrapper Screen={Screen} />
  }
}

const mapStateToProps = state => ({
  route: routeSelector(state)
})

export default connect(mapStateToProps)(Router)



import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { debounce } from 'lodash'

import { detectPageRootId } from '../utils/page-utils'
import { initPage } from './../actions/app-action'
import Router from './Router'
import { setSize } from '../actions/screen-action'
import { getScreenDimension } from '../utils/function-utils'
import { routeSelector } from '../selectors/router-selectors'
import { i18nSelector } from '../selectors/i18n-selector'
import { Loader } from '../components/Loader'

export class App extends React.Component {
  static propTypes = {
    initPage: PropTypes.func,
  }

  constructor() {
    super()
    this.rootId = detectPageRootId()
  }

  onScreenResized = debounce(e => {
    this.props.setScreenSize(getScreenDimension(e.target))
  }, 300)

  componentDidMount() {
    console.log('App componentDidMount this.rootId', this.rootId)
    this.props.setScreenSize(getScreenDimension(window))
    this.props.initPage({ rootId: this.rootId })
    window.addEventListener('resize', this.onScreenResized)
  }

  render() {
    return this.props.i18n ? <Router rootId={this.rootId}/> : <Loader/>
  }
}

const mapStateToProps = state => ({
  i18n: i18nSelector(state)
})


const mapDispatchToProps = ({
  initPage,
  setScreenSize: setSize
})

export default connect(mapStateToProps, mapDispatchToProps)(App)

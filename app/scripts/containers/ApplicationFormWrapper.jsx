import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { Loader } from '../components/Loader'
import { chaptersSelector } from '../selectors/application-form-selector'
import TermsAndConditionsModal from '../components/application-form/modal/TermsAndConditionsModal'

class ApplicationFormWrapper extends Component {

  render() {
    console.log('ApplicationFormWrapper', this.props)
    const { chapters, Screen } = this.props
    return !chapters
      ? <Loader />
      : (
        <div>
          <TermsAndConditionsModal />
          <Screen />
        </div>
      )
  }
}

const mapStateToProps = state => ({
  chapters: chaptersSelector(state),
})

ApplicationFormWrapper.propTypes = {
  chapters: PropTypes.array,
}

export default connect(mapStateToProps)(ApplicationFormWrapper)

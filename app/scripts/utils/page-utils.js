import { PageContainers } from './constants'

export const detectPageRootId = () => {
  switch (true) {
    case (document.getElementById(PageContainers.CONFIGURATOR_PAGE) !== null):
      return PageContainers.CONFIGURATOR_PAGE

    case (document.getElementById(PageContainers.PACKAGE_CONFIGURE_PAGE) !== null):
      return PageContainers.PACKAGE_CONFIGURE_PAGE

    default:
      return undefined
  }
}


export const generateFonts = path => `
  @import url('https://fonts.googleapis.com/css?family=Montserrat:300,700');
  @import url('//fonts.googleapis.com/css?family=Lato:400,700');
  
  @font-face {
    font-family: 'UniNeueRegular';
    src: url('${path}/fonts/UniNeueRegular.woff') format('woff'),
    url('${path}/fonts/UniNeueRegular.ttf') format('truetype'),
    url('${path}/fonts/UniNeueRegular.eot') format('embedded-opentype');
    font-weight: normal;
    font-style: normal;
  }
  
  @font-face {
    font-family: 'UniNeueBold';
    src: url('${path}/fonts/UniNeueBold.woff') format('woff'),
    url('${path}/fonts/UniNeueBold.ttf') format('truetype'),
    url('${path}/fonts/UniNeueBold.eot') format('embedded-opentype');
    font-weight: normal;
    font-style: normal;
  }

  @font-face {
    font-family: 'icomoon';
    src: url('${path}/fonts/icon-fonts.woff?nj4ix0') format('woff'),
    url('${path}/fonts/icon-fonts.ttf?nj4ix0') format('truetype'),
    url('${path}/fonts/icon-fonts.eot?nj4ix0#iefix') format('embedded-opentype');
    font-weight: normal;
    font-style: normal;
  }
`


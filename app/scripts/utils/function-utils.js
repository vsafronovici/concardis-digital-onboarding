import { addIndex, toPairs, values, isNil, equals, all, isEmpty, propOr, curry, anyPass, map, compose, not, keys, filter } from 'ramda'
import numeral from 'numeral'
import moment from 'moment'
import { DATE_FORMAT, FeaturesType, Routes } from './constants'
import { translate } from '../i18n/i18n'
import { resourceIcons } from './vf-utils'

export const isNilOrEmpty = anyPass([isNil, isEmpty])
export const isNotNilOrEmpty = compose(not, isNilOrEmpty)
export const propOrEmptyObj = propOr({})
export const propOrEmptyArr = propOr([])
export const mapIndexed = addIndex(map)

export const getImageResources = (name, folder = 'images', ext = 'png') =>
  `${resourceIcons}/media/${folder}/${name}.${ext}`

export const objectToArray = (obj, prop) =>
  toPairs(obj).map(
    ([key, value]) => (prop ? { [prop]: key, ...value } : value)
  )

export const objectToArrayKeyValue = obj =>
  toPairs(obj).map(
    ([key, value]) => ({ key, value })
  )

export const findObjKeys = curry((predicate, obj) => compose(filter(predicate), keys)(obj))

export const notEmptyValues = obj => {
  const detectedKeys = values(obj).map(key => !isNil(key) || (isNil(key) && isNaN(key) && !isEmpty(key)))
  const isTrue = equals(true)

  return !all(isTrue)(detectedKeys)
}

// TODO remove use contains() instead
export const checkCurrentValue = (items, currentValue) => {
  const checkedCurrentValue = items.indexOf(currentValue)

  return checkedCurrentValue
}

export const format = (text, ...vals) =>
  vals.reduce((acc, obj, idx) => acc.replace(`{${idx}}`, obj), text)

export const delayResponse = resp =>
  new Promise((resolve, reject) =>
    setTimeout(() => resolve({ data: resp }), 1000)
  )

export const formatNumber = (formatter, value) => {
  const number = numeral(value)
  const formattedNumber = number.format(formatter)
  return formattedNumber
}

export const generalFormatNumber = curry(formatNumber)('00.00')

export const checkDate = (date) => moment(date, DATE_FORMAT, true).isValid()

export const parseCheckBoxValues = vals => {
  if (!vals) {
    return null
  }
  const parsedValues = vals.split(';').map(value => ` ${translate(value)} ${translate('vat_option_1')} `)
  return parsedValues
}

export const goToExternalPage = pageLink => {
  window.location.href = pageLink
}

export const checkFeaturesIcon = (name) => {
  switch (name) {
    case FeaturesType.MASTER_CARD:
    case FeaturesType.MC:
      return 'mastercard-2'
    case FeaturesType.MAESTRO:
      return 'maestro-2'
    case FeaturesType.VISA:
      return 'visa'
    case FeaturesType.VPAY:
      return 'v-pay'
    case FeaturesType.VISA_ELECTRON:
      return ''
    case FeaturesType.GIRO_CARD:
    case FeaturesType.GIRO:
      return 'girocard'
    case FeaturesType.APPLE_PAY:
      return 'apple-pay'
    case FeaturesType.G_PAY:
      return 'g-pay'
    default:
      return null
  }
}

export const getScreenDimension = window => ({
  height: window.innerHeight,
  width: window.innerWidth

})

export const goToPage = page => window.location.href = page

export const isAppForm = () => {
  return window.location.hash === "#/application-form"
}

export const creditCardDevider = value => {
  const giroCard = value
  const creditCard = 100 - value

  return `${translate('configurator.giro')}: ${giroCard}% - ${translate('configurator.creditCard')}: ${creditCard}%`
}

export const checkInternalUserGrid = (products, gridType) => {
  if (products.length === 4) {
    switch (gridType) {
      case 'xxl':
        return {'span': 5, 'offset': 1}
      case 'xl':
        return {'span': 8, 'offset': 1}
      default:
        return {'span': 5, 'offset': 1}
    }
  } else {
      switch (gridType) {
        case 'xxl':
          return {'span': 6, 'offset': 1}
        case 'xl':
          return {'span': 6, 'offset': 1}
        default:
          return {'span': 7, 'offset': 1}
      }
  }
}

export const isMobileQuote = route => {
  switch (route) {
    case Routes.STEP3:
    case Routes.EDIT_PACKAGE:
    case Routes.START_APPLICATION:
    case Routes.RESUME_APPLICATION:
      return true
    default:
      return null
  }
}

export const isMobileContent = route => {
  switch (route) {
    case Routes.STEP1:
    case Routes.STEP2:
    case Routes.APP_FORM:
    case Routes.REVIEW_APP_FORM:
      return true
    default:
      return null
  }
}

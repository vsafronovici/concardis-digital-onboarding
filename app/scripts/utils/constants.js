import { createEnum } from './namespace-util'

export const NodeProcess = {
  DEV: 'development',
  PROD: 'production'
}

export const ContainerPaths = {
  root: '/',
  applicationForm: '/application-form'
}

export const PageContainers = {
  APPLICATION_FORM_PAGE: 'application-form-page',
  CONFIGURATOR_PAGE: 'configurator-page',
  PACKAGE_CONFIGURE_PAGE: 'package-configure-page'
}

export const LanguageType = {
  EN: 'en_US',
  DE: 'de'
}

export const API_ERROR_TITLE = 'ERROR'

export const RESPONSE_STATUS = {
  OK: 'ok',
  ERR: 'err'
}

export const RESPONSE_STREEMING_STATUS = {
  SUCCESS: 'Success',
  FAILURE: 'Failure'
}

export const RESPONSE_INTEGRATION_API_STATUS = {
  PENDING: 'Pending',
  COMPLETED: 'Completed',
  ERROR: 'Error'
}

export const RESPONSE_STATUS_CODE = {
  OK: '0',
  ERR: '1'
}

export const PAGE_SITE_SETTINGS = {
  START_APP: 0,
  RESUME_APP: 1,
  QUOTE_EXPIRED: 2,
}

export const SectionType = {
  SIMPLE: 'Simple',
  CONDITIONAL: 'Conditional'
}

export const FieldType = {
  TEXT: 'text',
  TEXT_BOLD: 'text_bold',
  DATE: 'date',
  DROPDOWN: 'drop_down',
  VALIDATE: 'validate',
  SLIDER: 'Slider',
  SELECTABLE_ITEMS: 'selectable_items',
  HORIZONTAL_RADIO_BTNS: 'horizontal_radio_btns',
  VERTICAL_RADIO_BTNS: 'vertical_radio_btns',
  BOXED_RADIO_BTNS: 'boxed_radio_btns',
  CHECKBOX: 'checkbox',
  BOXED_CHECKBOX: 'boxed_checkbox',
  BOXED_CHECKBOX_GROUP: 'boxed_checkbox_group',
  TITLE: 'title',
  WARNING: 'warning',
  TEXT_BOLD_STATUS: 'text_bold_active'
}

export const OptionType = {
  LIST: 'list',
  IMAGE_LABEL: 'imageLabel',
}

export const SectionStatusType = {
  FINISHED: 'FINISHED',
  IN_PROGRESS: 'IN_PROGRESS',
  WAITING: 'WAITING',
  PAUSED: 'PAUSED'
}

export const ConfiguratorPageStep = {
  STEP1: 'Landing_Page_1',
  STEP2: 'Landing_Page_2',
  STEP3: 'Landing_Page_3',
  STEP4: 'Landing_Page_4'
}

export const SignUpCode = {
  SCEN1: '1',
  SCEN2: '2',
  SCEN3: '3',
  SCEN4: '4',
  SCEN5: '5'
}

export const PackageRoutes = {
  ROUTE_1: 'Package_Configure',
  ROUTE_2: 'Package_Personalise'
}

export const EXTERNAL_LINKS = {
  APPLICATION_FORM: '/OndoardingSME/apex/ApplicationFormPage',
  PACKAGE_CONFIGURE: '/OndoardingSME/apex/ccSMEOnboardingAuth'
}

export const INTERNAL_LINKS = {
  PACKAGE_CONFIGURE: 'ccSMEOnboardingAuth'
}

export const ApplicationFormStepsTitles = {
  STEP_1: 'ABOUT_YOU',
  STEP_2: 'COMPANY_LEGAL_DETAILS',
  STEP_3: 'COMPANY_CONTACT_DETAILS',
  STEP_4: 'COMPANY_BUSINESS_MODEL',
  STEP_5: 'CUSTOMISE_YOUR_PRODUCT',
  STEP_6: 'PAYMENT_DETAILS'
}

export const DATE_FORMAT = 'YYYY-MM-DD'

export const FEATURES = [
  'configurator.packagePersonalise.list.Feature1',
  'configurator.packagePersonalise.list.Feature2',
  'configurator.packagePersonalise.list.Feature3',
]

export const DATE_TO_DISPLAY = {
  TIME: 'LTS',
  DATE: 'DD/MM/YYYY'
}

export const CONDITIONS = {
  YES: 'Yes',
  NO: 'No'
}

export const ValiditiType = {
  CORRESPONDENCE_ADDRESS: 'correspondence_address',
  BUSINESS_ADRESS: 'business_address',
  OUTLET_ADRESS: 'outlet_address',
  IBAN: 'IBAN'
}


//FIXME: (remove this hard code constant)
export const AccountCard = [
  {
    title: 'Card',
    values: 'VISA'
  },
  {
    title: 'Account',
    values: '**** **** **** 0119'
  },
  {
    title: 'AID',
    values: 'A00000000310109999'
  },
  {
    title: 'Entry Mode',
    values: 'Contactless'
  }
]

export const FeaturesType = {
  MASTER_CARD: 'Mastercard',
  MAESTRO: 'Maestro',
  VISA: 'Visa',
  VPAY: 'VPAY',
  VISA_ELECTRON: 'Visa Electron',
  GIRO_CARD: 'Girocard',
  APPLE_PAY: 'Other Cards',
  G_PAY: 'g-pay',
  GIRO: 'Giro',
  MC: 'MC',
}

export const ScreenSize = {
  SM: 576,
  MD: 768,
  LG: 992,
  XL: 1200,
  XXL: 1600,
}

export const ScreenType = createEnum({
  XS: undefined,
  SM: undefined,
  MD: undefined,
  LG: undefined,
  XL: undefined,
  XXL: undefined,

})

export const Routes = createEnum({
  STEP1: undefined,
  STEP2: undefined,
  STEP3: undefined,
  STEP4: undefined,
  QUOTE_EXPIRED: undefined,
  EDIT_PACKAGE: undefined,
  START_APPLICATION: undefined,
  RESUME_APPLICATION: undefined,
  APP_FORM_SWITCHER: undefined,
  APP_FORM: undefined,
  REVIEW_APP_FORM: undefined,
  SUBMIT_APP_FORM_LOADER: undefined,
  SUBMIT_APP_FORM_SUCCESS: undefined,
  SUBMIT_APP_FORM_ERROR: undefined,
})

export const StepBarProgress = {
  WAIT: 'wait',
  PROCESS: 'process',
  FINISH: 'finish'
}

import { path, prop } from 'ramda'

export const configSettings = prop('configSettings', window)
export const remoteActions = prop('remoteActions', configSettings)
export const resources = prop('resources', configSettings)
export const resourceIcons = path(['imgs', 'icons'], resources)
export const resourceFonts = prop('fonts', resources)
export const isInternalUser = prop('isInternalUser', configSettings)
export const statePageSetting = prop('statePageSetting', configSettings)


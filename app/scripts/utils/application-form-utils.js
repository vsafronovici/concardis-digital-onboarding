import { pluck, prop, sortBy, compose, filter, curry } from 'ramda'
import { isNilOrEmpty, isNotNilOrEmpty } from './function-utils'
import { SectionStatusType, ValiditiType } from './constants'

import { translate } from '../i18n/i18n'

export const DYNAMIC_FORM_PREFIX = 'dynamicForm_'
export const MULTIPLE_OPTIONS_SEPARATOR = ';'
export const INPUT_BOLD_PREFIX = 'bold_'

export const CONDITIONAL_OPERATORS = {
  EQUAL: '=',
  NOT_EQUAL: '!=',
  INCLUDES: '<=',
  AND: 'AND',
}

const PREFIXES = {
  SECTION: 'Section'
}

export const InputTextBoldStatus = {
  NORMAL: 'bold_option_1',
  BOLD: 'bold_option_2'
}

const { EQUAL, NOT_EQUAL, INCLUDES, AND } = CONDITIONAL_OPERATORS

const getConditionalOperands = curry((condition, values, operator) => {
  const [cFieldName, cValue] = condition.trim().split(operator)
  const value = values[cFieldName.trim()]
  return {
    cFieldName,
    cValue: cValue.trim(),
    value
  }
})

export const checkSectionCondition = (conditions, values) => conditions.split(AND).reduce((acc, condition) => {
  const conditionalOperands = getConditionalOperands(condition, values)

  switch (true) {
    case condition.includes(NOT_EQUAL): {
      const { cValue, value } = conditionalOperands(NOT_EQUAL)
      return acc && (value === undefined || value.toString() !== cValue)
    }

    case condition.includes(INCLUDES): {
      const { cValue, value } = conditionalOperands(INCLUDES)
      return acc && value && value.split(MULTIPLE_OPTIONS_SEPARATOR).includes(cValue)
    }

    case condition.includes(EQUAL): {
      const { cValue, value } = conditionalOperands(EQUAL)
      return acc && value && value.toString() === cValue
    }

    default:
      return false
  }

}, true)

export const createInitialValues = chapter => chapter.sections.reduce((acc, section) => {
  if (isNilOrEmpty(section.serverValues)) {
    return acc
  }

  return section.serverValues.reduce((acc2, serverValue) => ({ ...acc2, [serverValue.fieldCode]: serverValue.fieldValue }), acc)
}, {})

export const fieldsToShow = (chapter, formValues = {}) => {
  if (isNilOrEmpty(chapter.sections)) {
    return []
  }

  return chapter.sections.reduce((acc, section) => {
    const { condition, fields } = section

    if (isNilOrEmpty(fields)) {
      return acc
    }

    if (isNilOrEmpty(condition) || checkSectionCondition(condition, formValues)) {
      acc.push(...fields)
    }

    return acc
  }, [])
}

const buildServerValues = (section, formValues) => {
  const { condition, fields } = section

  if (isNilOrEmpty(fields) || isNilOrEmpty(formValues) || isNotNilOrEmpty(condition) && !checkSectionCondition(condition, formValues)) {
    return []
  }

  return fields.reduce((acc, { name }) => {
    const value = formValues[name]
    if (value) {
      acc.push({ fieldCode: name, fieldValue: value })
    }
    return acc
  }, [])

}

const hasNoFormErrors = formErrors => serverValue => !formErrors[serverValue.fieldCode]

export const buildSaveRequest = ({ formValues, chapters, currentChapterIdx }) => {
  const chapter = chapters[currentChapterIdx]
  chapter.status = SectionStatusType.IN_PROGRESS

  chapter.sections.forEach(section => {
    section.serverValues = buildServerValues(section, formValues)
  })

  return chapters
}

export const buildApiValidationRequest = (fieldsToValidate, formValues) =>
  fieldsToValidate.reduce((acc, { name }) =>
  ({ ...acc, [name.split('Section')[0]]: formValues[name] }), {})

export const buildSaveAndCloseRequest = ({ chapter, formValues, formErrors }) => {
  chapter.status = SectionStatusType.IN_PROGRESS

  chapter.sections.forEach(section => {
    section.serverValues = compose(filter(hasNoFormErrors(formErrors)), buildServerValues)(section, formValues)
  })

  return chapter
}


export const getNotRequired = validationRules =>
  isNilOrEmpty(validationRules) || !pluck('required', validationRules)[0]

export const sortBySequence = sortBy(prop('sequence'))

export const toBoolean = value => value && value.toString() === 'true'

export const onChangeCheckBox = (event, input, touch) => {
  input.onChange(event)
  touch(input.name)
}

export const checkPublicOwnerField = (label, value, translate) =>
  value === 'true' && label === 'ch2_s2_field8_label' ? translate(label) : null

export const getSectionPrefix = (array) => {
  const itemName = array[0].name
  const index = itemName.indexOf(PREFIXES.SECTION)
  const prefix = itemName.substring(index, itemName.length)
  return prefix
}

export const setSectionPrefix = (prefix, obj) => {
  const withPrefix = {}
  for (let key in obj) {
    withPrefix[key + prefix] = obj[key]
  }
  return withPrefix
}

export const getValidityName = validityType => {
    switch (validityType) {
        case ValiditiType.BUSINESS_ADRESS:
          case ValiditiType.CORRESPONDENCE_ADDRESS:
          case ValiditiType.OUTLET_ADRESS: {
            return translate('appForm.review.validate.address')
          }
          case ValiditiType.IBAN: {
            return translate('appForm.review.validate.IBAN')
          }
          default:
            return null
          }
  }

export const parseValidityValue = value => {
    console.log('VALUE', value === 'false')
    switch (value) {
        case 'true': {
            return CONDITIONAL_OPERATORS.YES
            }
          case 'false': {
            return CONDITIONAL_OPERATORS.NO
            }
          default:
            return null
     }
}

import { createSelector } from 'reselect'
import { compose, contains, prop, isEmpty } from 'ramda'
import { findObjKeys, isNilOrEmpty } from '../utils/function-utils'
import { getFormValues } from 'redux-form'
import { DYNAMIC_FORM_PREFIX, INPUT_BOLD_PREFIX, InputTextBoldStatus } from '../utils/application-form-utils'
import { RESPONSE_INTEGRATION_API_STATUS, RESPONSE_STATUS } from '../utils/constants'

export const applicationFormSelector = prop('application-form')
export const applicationFormSubmittingSelector = compose(prop('submitting'), applicationFormSelector)
export const chaptersSelector = compose(prop('chapters'), applicationFormSelector)
export const modalsSelector = compose(prop('modals'), applicationFormSelector)
export const currentSelector = compose(prop('current'), applicationFormSelector)
export const sessionIdSelector = compose(prop('sessionId'), applicationFormSelector)
export const apiValidationStatusSelector = compose(prop('apiValidationStatus'), applicationFormSelector)
export const apiValidationSelector = compose(prop('apiValidation'), applicationFormSelector)
export const tacSelector = compose(prop('TAC'), applicationFormSelector)
export const showTacSelector = compose(prop('show'), tacSelector)
export const validityModalSelector = compose(prop('toggleValidationModal'), applicationFormSelector)
export const nrOfChaptersSelector = compose(prop('length'), chaptersSelector)
export const reviewModeSelector = compose(prop('reviewMode'), applicationFormSelector)
export const finalSubmitSelector = compose(prop('finalSubmit'), applicationFormSelector)
export const integrationApiPollingSelector = compose(prop('integrationApiPolling'), applicationFormSelector)
export const validateLoadingSelector = compose(prop('validateLoading'), applicationFormSelector)

export const currentChapterSelector = createSelector(
  chaptersSelector,
  currentSelector,
  (chapters = [], current) => chapters[current]
)

export const findFieldsForValidateSelector = (parentValidationName, state) => {
  return createSelector(
    currentChapterSelector,
    ({ sections = [] }) => {
      const validateFields = sections.reduce((acc, section, index) => {

        const fields = section.fields.filter(({ validationRules }) => {
          if (validationRules && validationRules[0] && validationRules[0].parentValidation) {
            return validationRules[0].parentValidation === parentValidationName
          }
        })
        
        if(fields.length) {
          acc.push(...fields)
        }

        return acc 
      }, [])

      return validateFields
    },
  )(state)
}

export const fieldsSelector = createSelector(
  currentChapterSelector,
  (chapter = {}) => {
    if (!chapter.sections) {
      return []
    }

    return chapter.sections.reduce((acc, section) => {
      const { fields } = section

      if (isNilOrEmpty(fields)) {
        return acc
      }

      acc.push(...fields)

      return acc
    }, [])
  }
)

export const receiptChapterFormValuesSelector = getFormValues(`${DYNAMIC_FORM_PREFIX}5`)

export const receiptChapterBoldSelector = createSelector(
  receiptChapterFormValuesSelector,
  formValues => compose(
      keys => isEmpty(keys) || isNilOrEmpty(formValues[keys[0]]) ? undefined : formValues[keys[0]] === InputTextBoldStatus.BOLD,
      findObjKeys(contains(INPUT_BOLD_PREFIX))
  )(formValues)
)



export const submitAndIntegrationApiSelector = createSelector(
  finalSubmitSelector,
  integrationApiPollingSelector,
  (fs, iApi) => {
    const fsErr = fs.status === RESPONSE_STATUS.ERR
    let status
    switch (true) {
      case fsErr || iApi.status === RESPONSE_INTEGRATION_API_STATUS.ERROR:
        status = RESPONSE_STATUS.ERR
        break
      case fs.status === RESPONSE_STATUS.OK && iApi.status === RESPONSE_INTEGRATION_API_STATUS.COMPLETED:
        status = RESPONSE_STATUS.OK
        break
      case !fs.status || !iApi.status:
        status = undefined
        break

      default:
        status = RESPONSE_INTEGRATION_API_STATUS.PENDING
    }

    return {
      submitting: fsErr ? false : fs.submitting || iApi.polling,
      status: status
    }
  }
)






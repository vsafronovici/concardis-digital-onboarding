import { compose, prop, map } from 'ramda'
import { createSelector } from 'reselect'

import { ConfiguratorPageStep } from '../utils/constants'
import { propOrEmptyObj, propOrEmptyArr } from '../utils/function-utils'

export const configuratorSelector = prop('configurator')
export const step1MetaSelector = compose(prop('step1MetaData'), configuratorSelector)
export const step2MetaSelector = compose(propOrEmptyArr('step2MetaData'), configuratorSelector)
export const fieldsSelector = compose(prop('fields'), configuratorSelector)
export const step1FieldsSelector = compose(propOrEmptyObj(ConfiguratorPageStep.STEP1), fieldsSelector)
export const step2FieldsSelector = compose(propOrEmptyObj(ConfiguratorPageStep.STEP2), fieldsSelector)
export const step3FieldsSelector = compose(propOrEmptyObj(ConfiguratorPageStep.STEP3), fieldsSelector)
export const cardOptionValueSelector = compose(prop('cardOption'), step2FieldsSelector)
export const stepSelector = compose(prop('step'), configuratorSelector)
export const productsSelector = compose(map(prop(['prod'])), step2MetaSelector)
export const recalculatedQuoteSelector = compose(prop('recalculatedQuote'), configuratorSelector)
export const signUpCodeSelector = compose(prop('signupCode'), configuratorSelector)
export const emailSelector = compose(prop('email'), step3FieldsSelector)
export const userSelector = compose(prop('user'), configuratorSelector)
// export const quoteSelector = compose(prop('quote'), configuratorSelector)


export const step2SummarySelector = createSelector(
  step1MetaSelector,
  step1FieldsSelector,
  (step1Meta, step1Fields) => step1Meta.map(meta => {

    const option = meta.options.find(opt => opt.value == step1Fields[meta.name])

    return  {
      name: meta.name,
      label: meta.label,
      value: step1Fields[meta.name],
      valueLabel: option && option.label,
    }
  })
)

export const selectedProductSelector = createSelector(
  step2MetaSelector,
  cardOptionValueSelector,
  (step2Meta, cardOption) => step2Meta.find(meta => meta.product2Id === cardOption)
)


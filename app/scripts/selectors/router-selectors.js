import { createSelector } from 'reselect'
import { compose, prop } from 'ramda'
import { Routes } from '../utils/constants'
import { contains, __ } from 'ramda'

const APP_FORM_SCREENS = [Routes.APP_FORM , Routes.REVIEW_APP_FORM]

const routerSelector = prop('router')
export const routeSelector = compose(prop('route'), routerSelector)
export const prevRouteSelector = compose(prop('prevRoute'), routerSelector)

export const isAppFormSelector = createSelector(
  routeSelector,
  contains(__, APP_FORM_SCREENS)
)

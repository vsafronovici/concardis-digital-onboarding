import { createSelector } from 'reselect'
import { compose, prop, contains, __ } from 'ramda'
import { ScreenSize, ScreenType } from "../utils/constants";

const SMALL_LOGO_SCREENS = [ScreenType.SM, ScreenType.MD]

export const screenSelector = prop('screen')
export const widthSelector = compose(prop('width'), screenSelector)

export const screenTypeSelector = createSelector(
  widthSelector,
  width => {
    switch (true) {
      case (width < ScreenSize.SM):
        return ScreenType.XS

      case (width >= ScreenSize.SM && width < ScreenSize.MD):
        return ScreenType.SM

      case (width >= ScreenSize.MD && width < ScreenSize.LG):
        return ScreenType.MD

      case (width >= ScreenSize.LG && width < ScreenSize.XL):
        return ScreenType.LG

      case (width >= ScreenSize.XL && width < ScreenSize.XXL):
        return ScreenType.XL

      case (width >= ScreenSize.XXL):
        return ScreenType.XXL

      default:
        return ScreenType.XXL
    }
  }
)

export const isSmallScreenSelector = createSelector(
  screenTypeSelector,
  contains(__, SMALL_LOGO_SCREENS)
)

export const isMobileSelector = createSelector(
  screenTypeSelector,
  screenType => screenType === ScreenType.XS
)

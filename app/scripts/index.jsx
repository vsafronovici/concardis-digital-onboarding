// Polyfills
import 'core-js/shim'
import 'isomorphic-fetch'
import 'classlist-polyfill'
import 'vendor/polyfills'

import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { AppContainer } from 'react-hot-loader'
import { PersistGate } from 'redux-persist/lib/integration/react'

import 'antd/dist/antd.css'

import { store, persistor } from './store'
import { showAlert } from './actions/app-action'

import App from './containers/App'
import '../styles/main.scss'
import { detectPageRootId, generateFonts } from './utils/page-utils'
import { resourceFonts } from './utils/vf-utils'

const isProd = process.env.NODE_ENV === 'production'

export const init = {
  cssRetries: 0,
  fetchRetries: 0,

  run() {
    this.loadFonts()
    const rootId = detectPageRootId()
    /* istanbul ignore else */
    if (!isProd) {
      this.render(rootId)
      return Promise.resolve()
    }

    this.initOfflinePlugin()

    /* istanbul ignore next */
    return Promise
      .all([this.loadCSS()])
      .then(() => this.render(rootId))
      .catch(reason => {
        if (this.fetchRetries < 3) {
          this.fetchRetries++
          this.run()
        }
      })
  },
  loadFonts() {
    const path = isProd ? resourceFonts : ''
    const css = generateFonts(path)
    const head = document.head || document.getElementsByTagName('head')[0]
    const style = document.createElement('style')

    style.type = 'text/css'
    style.appendChild(document.createTextNode(css))
    head.appendChild(style)
  },
  loadCSS() {
    /* istanbul ignore next */
    return new Promise(resolve => {
      this.retryCSS = () => {
        if (this.isCSSLoaded() || this.cssRetries > 2) {
          resolve()
        } else {
          this.cssRetries++
          setTimeout(() => {
            this.retryCSS()
          }, this.cssRetries * 500)
        }
      }

      this.retryCSS()
    })
  },
  initOfflinePlugin() {
    const OfflinePlugin = require('offline-plugin/runtime')

    /* istanbul ignore next */
    OfflinePlugin.install({
      onUpdateReady: () => {
        OfflinePlugin.applyUpdate()
      },
      onUpdated: () => {
        store.dispatch(showAlert((
          <div className="app__cache-reload">
            <p>There's a new version of this app!</p>
            <button className="btn btn-sm btn-outline-primary" onClick={() => window.location.reload()}>
              Reload
            </button>
          </div>
        ), { id: 'sw-update', type: 'primary', icon: 'i-flash', timeout: 0 }))
      }
    })
  },
  isCSSLoaded() {
    const styles = document.styleSheets

    /* istanbul ignore next */
    try {
      for (let i = 0; i < styles.length; i++) {
        if (styles[i].href && styles[i].href.match('app.*.css')) {
          if (styles[i].cssRules !== null && styles[i].cssRules.length > 0) {
            return true
          }
        }
      }
    } catch (e) {
      // error
    }

    return false
  },
  render(rootId) {
    const root = document.getElementById(rootId)

    /* istanbul ignore next */
    if (root) {
      ReactDOM.render(
        <AppContainer>
          <Provider store={store}>
            <PersistGate persistor={persistor}>
              <App />
            </PersistGate>
          </Provider>
        </AppContainer>,
        root
      )
    }
  }
}

init.run()

/* istanbul ignore next  */
if (module.hot) {
  const rootId = detectPageRootId()
  module.hot.accept(
    'containers/App',
    () => init.render(rootId)
  )
}

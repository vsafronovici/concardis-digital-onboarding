import { createReducer } from './../modules/helpers'
import { SCREEN } from './../actions/types'

const initialState = {
  height: 0,
  width: 0
}

export default {
  screen: createReducer(initialState, {
    [SCREEN.SET_SIZE](state, { payload }) {
      return {
        ...state,
        ...payload
      }
    }
  })
}

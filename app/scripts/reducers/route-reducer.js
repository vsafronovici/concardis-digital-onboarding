import { createReducer } from './../modules/helpers'
import { ROUTE } from './../actions/types'

const initialState = {
  prevRoute: undefined,
  route: undefined
}

export default {
  router: createReducer(initialState, {
    [ROUTE.SET_ROUTE](state, { payload }) {
      return {
        ...state,
        prevRoute: state.route,
        route: payload
      }
    }
  })
}

import screen from './screen-reducer'
import app from './app-reducer'
import route from './route-reducer'
import i18n from './i18n-reducer'
import configuratorReducer from './configurator-reducer'
import packageConfigureReducer from './package-configure-reducer'
import applicationFormReducer from './application-form-reducer'

export default {
  ...screen,
  ...app,
  ...route,
  ...i18n,
  ...configuratorReducer,
  ...packageConfigureReducer,
  ...applicationFormReducer
}

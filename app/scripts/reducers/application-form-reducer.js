import { createReducer } from '../modules/helpers'
import { APPLICATION_FORM } from '../actions/types'
import { SectionStatusType } from './../utils/constants'
import { toBoolean } from '../utils/application-form-utils'

const initialState = {
  TAC: {
    show: false,
    sepa: false,
    agree: false,
    readOnly: true
  },
  toggleValidationModal: false,
  current: 0,
  sessionId: null,
  chapters: undefined,
  reviewMode: false,
  finalSubmit: {
    submitting: false,
    status: undefined
  },
  integrationApiPolling: {
    polling: false,
    status: undefined
  },
  modals: {},
  apiValidation: undefined,
  apiValidationStatus: undefined,
  loadingValidate: false,

}

const finishPaymentDetails = (state, payload) => {
  const { chapters } = state
  const lastSectionToFinished = {
    ...chapters[payload],
    status: SectionStatusType.FINISHED
  }
  chapters.splice(payload, 1, lastSectionToFinished)
  return {
    ...state,
    chapters
  }
}

const changedStatusEditing = (state, payload) => {
  const { chapters } = state
  const editedSectionToFinished = {
    ...chapters[payload],
    status: SectionStatusType.FINISHED
  }
  chapters.splice(payload, 1, editedSectionToFinished)
  return {
    ...state,
    chapters
  }
}

const goToNextSection = (state, payload) => {
  const { chapters } = state
  if (payload > chapters.length) {
    return state
  }


  const sectionToFinished = {
    ...chapters[payload - 1],
    status: SectionStatusType.FINISHED
  }
  chapters.splice(payload - 1, 1, sectionToFinished)

  if (payload < chapters.length) {
    const sectionToProgress = {
      ...chapters[payload],
      status: SectionStatusType.IN_PROGRESS
    }
    chapters.splice(payload, 1, sectionToProgress)
  }

  return {
    ...state,
    current: payload,
    chapters
  }
}


export default {
  'application-form': createReducer(initialState, {
    [APPLICATION_FORM.OPEN_TAC_MODAL](state, { payload }) {
      return {
        ...state,
        TAC: {
          ...state.TAC,
          show: true,
          readOnly: payload
        }
      }
    },
    [APPLICATION_FORM.CLOSE_TAC_MODAL](state) {
      return {
        ...state,
        TAC: {
          ...state.TAC,
          show: false
        }
      }
    },
    [APPLICATION_FORM.AGREE_TAC](state) {
      return {
        ...state,
        TAC: {
          ...state.TAC,
          show: false,
          agree: true
        }
      }
    },
    [APPLICATION_FORM.SET_SEPA_TAC](state, { payload }) {
      return {
        ...state,
        TAC: {
          ...state.TAC,
          sepa: toBoolean(payload)
        }
      }
    },
    [APPLICATION_FORM.TOGGLE_VALIDATION_MODAL](state, { payload }) {
      return {
        ...state,
        modals: { ...state.modals, [payload]: !state.modals[payload] }
      }
    },
    [APPLICATION_FORM.INIT_VALIDATION_MODAL](state, { payload }) {
      return {
        ...state,
        modals: { ...state.modals, [payload]: false }
      }
    },
    [APPLICATION_FORM.GET_FORM_META_RES](state, { payload }) {
      return {
        ...state,
        chapters: payload
      }
    },
    [APPLICATION_FORM.GO_TO_NEXT_SECTION](state, { payload }) {
      return goToNextSection(state, payload)
    },
    [APPLICATION_FORM.GO_TO_SECTION](state, { payload }) {
      return {
        ...state,
        current: payload,
      }
    },
    [APPLICATION_FORM.SAVE_REQ](state, { payload }) {
      return {
        ...state,
        submitting: true
      }
    },
    [APPLICATION_FORM.SAVE_RES](state, { payload }) {
      return {
        ...state,
        submitting: false
      }
    },
    [APPLICATION_FORM.GO_TO_REVIEW_MODE](state) {
      return {
        ...state,
        current: -1,
        reviewMode: true
      }
    },
    [APPLICATION_FORM.GET_SESSTION_ID_RES](state, { payload }) {
      return {
        ...state,
        sessionId: payload,
      }
    },
    [APPLICATION_FORM.SUBMIT_REQ](state, { payload }) {
      return {
        ...state,
        finalSubmit: { submitting: true }
      }
    },
    [APPLICATION_FORM.SUBMIT_RES](state, { payload: { status, errorMessage } }) {
      return {
        ...state,
        finalSubmit: {
          submitting: false,
          status,
          errorMessage
        }
      }
    },
    [APPLICATION_FORM.START_INTEGRATION_STATUS_POLLING](state, { payload }) {
      return {
        ...state,
        integrationApiPolling: {
          polling: true
        }
      }
    },
    [APPLICATION_FORM.STOP_INTEGRATION_STATUS_POLLING](state, { payload: { status } }) {
      return {
        ...state,
        integrationApiPolling: {
          polling: false,
          status
        }
      }
    },
    [APPLICATION_FORM.SET_VALIDATION_STATUS](state, { payload }) {
      return {
        ...state,
        apiValidationStatus: payload,
      }
    },
    [APPLICATION_FORM.API_VALIDATE_FIELDS_REQ](state, { payload }) {
      return {
        ...state,
        submitting: false,
      }
    },
    [APPLICATION_FORM.API_VALIDATE_FIELDS_RES](state, { payload }) {
      return {
        ...state,
        apiValidation: payload,
        submitting: false,
      }
    },
    [APPLICATION_FORM.API_VALIDATE_LOADING](state, { payload }) {
      return {
        ...state,
        loadingValidate: payload
      }
    },
    [APPLICATION_FORM.FINISH_PAYMENT_DETAILS](state, { payload }) {
      return finishPaymentDetails(state, payload)
    },
    [APPLICATION_FORM.CHANGED_STATUS_EDITING](state, { payload }) {
      return changedStatusEditing(state, payload)
    },
  })
}


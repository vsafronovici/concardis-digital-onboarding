import { all, call, put, select, takeLatest } from 'redux-saga/effects'
import { delay } from 'redux-saga'

import { CONFIGURATOR } from '../actions/types'
import {
  changeFieldValue, getMetaStep1Res, getMetaStep2Req, getMetaStep2Res, recalculateQuoteRes, signupRes, setConfiguratorQuote
} from '../actions/configurator-action'
import { apiFetchSaga } from './app-saga'
import { ConfiguratorPageStep, INTERNAL_LINKS, NodeProcess, Routes } from '../utils/constants'
import { step1FieldsSelector } from '../selectors/configurator-selector'
import { isInternalUser, remoteActions } from '../utils/vf-utils'
import { goToPage } from '../utils/function-utils'
import { setRoute } from '../actions/route-action'

const STEP_TO_ROUTE_MAP = {
  [ConfiguratorPageStep.STEP1]: Routes.STEP1,
  [ConfiguratorPageStep.STEP2]: Routes.STEP2,
  [ConfiguratorPageStep.STEP3]: Routes.STEP3,
  [ConfiguratorPageStep.STEP4]: Routes.STEP4
}

export function* initDataSaga() {
  const action = {
    actionName: remoteActions.getFieldsMetadata,
    args: ConfiguratorPageStep.STEP1
  }
  const response = yield call(apiFetchSaga, action, { parseToJSON: true })
  yield put(getMetaStep1Res(response.data.fields))
}

function* getMetaStep2Saga({ payload }) {
  const action = {
    actionName: remoteActions.getQProducts,
    args: JSON.stringify(payload)
  }
  const response = yield call(apiFetchSaga, action, { parseToJSON: true, memoize: true })
  yield put(getMetaStep2Res(response.data))
}

function* goToStepSaga({ payload }) {
  if (payload === ConfiguratorPageStep.STEP2) {
    const fields = yield select(step1FieldsSelector)
    yield put(getMetaStep2Req(fields))
  }

  yield put(setRoute(STEP_TO_ROUTE_MAP[payload]))
}

function* signupSaga({ payload }) {
  yield put(changeFieldValue({
    name: 'email',
    value: payload.email,
    step: ConfiguratorPageStep.STEP3
  }))

  if (process.env.NODE_ENV === NodeProcess.DEV) {
    yield call(delay, 600)
    yield put(signupRes({ lastName: 'Gucci', firstName: 'Mr.Guzganovici', code: '4' }))
  } else {
    const action = {
      actionName: remoteActions.submitEmailGDPR,
      args: JSON.stringify(payload)
    }
    const response = yield call(apiFetchSaga, action, { parseToJSON: true, memoize: true })
    
    yield put(signupRes(response.data))
  }

  if (isInternalUser) {
    goToPage(INTERNAL_LINKS.PACKAGE_CONFIGURE)
  }

}

function* recalculateQuoteSaga({ payload }) {
  const action = {
    actionName: remoteActions.recalculatePrice,
    args: JSON.stringify(payload)
  }
  const response = yield call(apiFetchSaga, action, { parseToJSON: true, memoize: true })
  yield put(recalculateQuoteRes(response.data))
}

function* getConfiguratorQuoteSaga() {
  const action = {
    actionName: remoteActions.getQuote
  }
  const response = yield call(apiFetchSaga, action, { parseToJSON: true })
  yield put(setConfiguratorQuote(response.data))
}

export default function* root() {
  yield all([
    takeLatest(CONFIGURATOR.GO_TO_STEP, goToStepSaga),
    takeLatest(CONFIGURATOR.GET_META_STEP2_REQ, getMetaStep2Saga),
    takeLatest(CONFIGURATOR.RECALCULATE_QUOTE_REQ, recalculateQuoteSaga),
    takeLatest(CONFIGURATOR.SIGNUP_REQ, signupSaga),
    takeLatest(CONFIGURATOR.GET_QUOTE, getConfiguratorQuoteSaga),
  ])
}

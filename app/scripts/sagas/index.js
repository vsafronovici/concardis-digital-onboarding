import { all, fork } from 'redux-saga/effects'

import app from './app-saga'
import router from './router-saga'
import configurator from './configurator-saga'
import packageConfigure from './package-configure-saga'
import applicationForm from './application-form-saga'
import errorHandler from './error-handler-saga'

/**
 * rootSaga
 */
export default function* root() {
  yield all([
    fork(app),
    fork(router),
    fork(configurator),
    fork(packageConfigure),
    fork(applicationForm),
    fork(errorHandler)
  ])
}

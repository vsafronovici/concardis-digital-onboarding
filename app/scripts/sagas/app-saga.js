import { all, call, put, takeLatest } from 'redux-saga/effects'
import { APP } from './../actions/types'
import { loadTranslationsReq, loadTranslationsResp, failedApiFetch } from '../actions/app-action'
import { SFAction, memoizedSFAction } from '../modules/client'
import { LanguageType } from '../utils/constants'
import { configSettings, remoteActions } from '../utils/vf-utils'
import { initRouter } from '../actions/route-action'


function* initPageSaga({ payload: { rootId } }) {
  yield put(loadTranslationsReq(
    configSettings.lang || LanguageType.EN,
    remoteActions.getDictionaryMetadata
  ))

  console.log('initPageSaga', rootId)
  yield put(initRouter({ rootId }))
}

export function* apiFetchSaga(action, options = {}) {
  const { memoize } = options
  try {
    const sfAction = memoize ? memoizedSFAction : SFAction
    return yield call(sfAction, action, options)
  } catch (err) {
    return yield put(failedApiFetch(err))
  }
}

function* loadTranslationsSaga({ payload: { lang, controller } }) {
  const action = {
    actionName: controller,
    args: lang
  }
  
  const response = yield call(apiFetchSaga, action)
  yield put(loadTranslationsResp(response.data))
}


export default function* root() {
  yield all([
    takeLatest(APP.INIT_PAGE, initPageSaga),
    takeLatest(APP.LOAD_TRANSLATIONS_REQ, loadTranslationsSaga),
  ])
}

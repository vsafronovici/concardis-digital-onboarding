import { all, call, put, select, takeLatest, take, race } from 'redux-saga/effects'
import { delay } from 'redux-saga'
import { findLastIndex, propEq } from 'ramda'
import { APPLICATION_FORM } from '../actions/types'
import { apiFetchSaga } from './app-saga'
import {getSectionPrefix, setSectionPrefix} from "../utils/application-form-utils"

import {
  updateCommercialsTCReq,
  updateCommercialsTCRes,
  goToNextSection,
  getFormMetaRes,
  goToReviewMode,
  goToSection,
  submitRes,
  submitReq,
  saveReq,
  saveRes,
  saveAndCloseReq,
  saveAndCloseRes,
  toggleValidationModal,
  apiValidateFieldsRes,
  apiValidateFieldsReq,
  setValidationStatus,
  apiValidateLoading,
  finishPaymentDetails,
  changedStatusEditing,
  getSessionRes,
  getIntegrationStatusRes, stopIntegrationStatusPolling, startIntegrationStatusPolling,
  finalSubmitionStatus
} from '../actions/application-form-action'
import {
  chaptersSelector,
  nrOfChaptersSelector,
  reviewModeSelector,
} from '../selectors/application-form-selector'
import { buildSaveAndCloseRequest, buildSaveRequest, buildApiValidationRequest } from '../utils/application-form-utils'
import {
  RESPONSE_STATUS, SectionStatusType,
  RESPONSE_INTEGRATION_API_STATUS, Routes
} from '../utils/constants'
import { remoteActions } from '../utils/vf-utils'
import { setRoute } from '../actions/route-action'

function* getAppFormMetadataSaga() {
  const action = {
    actionName: remoteActions.getAppFormMetadata
  }
  const response = yield call(apiFetchSaga, action, { parseToJSON: true })
  yield put(getFormMetaRes(response.data))
}

export function* initApplicationFormDataSaga() {
  yield call(getAppFormMetadataSaga)
  const chapters = yield select(chaptersSelector)

  const lastFinishedChapterIndex = findLastIndex(propEq('status', SectionStatusType.FINISHED))(chapters)

  if (lastFinishedChapterIndex < chapters.length - 1) {
    yield put(goToSection(lastFinishedChapterIndex + 1))
  } else {
    yield put(goToReviewMode())
  }
}

function* agreeTACSaga() {
  yield put(setRoute(Routes.SUBMIT_APP_FORM_LOADER))

  const action = {
    actionName: remoteActions.updateCommercialsTC,
    args: true
  }
  yield put(updateCommercialsTCReq(true))

  const response = yield call(apiFetchSaga, action, { parseToJSON: true })
  yield put(updateCommercialsTCRes(response.data))

  yield put(submitReq())
}

function* saveSaga({ payload: { formValues, currentChapterIdx, callback } }) {
  const chapters = yield select(chaptersSelector)

  const req = buildSaveRequest({ formValues, chapters, currentChapterIdx })
  yield put(saveReq(req))
  const action = {
    actionName: remoteActions.validateForm,
    args: JSON.stringify(req)
  }

  const response = yield call(apiFetchSaga, action, { parseToJSON: true })
  yield put(saveRes(response.data))


  if (response.data.status === RESPONSE_STATUS.OK) {
    const reviewMode = yield select(reviewModeSelector)
    const nrOfChapters = yield select(nrOfChaptersSelector)

    yield call(getAppFormMetadataSaga)

    callback(response.data)
    if (reviewMode) {
      yield put(changedStatusEditing(currentChapterIdx))
      yield put(goToSection(-1))
    } else if (currentChapterIdx < nrOfChapters - 1) {
      yield put(goToNextSection(currentChapterIdx + 1))
    } else {
      yield put(finishPaymentDetails(currentChapterIdx))
      yield put(goToReviewMode())
      yield put(setRoute(Routes.REVIEW_APP_FORM))
    }
  } else {
    callback(response.data)
  }
}

function* saveAndCloseSaga({ payload: { chapter, formValues, formErrors  } }) {

  const req = buildSaveAndCloseRequest({ chapter, formValues, formErrors })
  yield put(saveAndCloseReq(req))

  const action = {
    actionName: remoteActions.saveAndClose,
    args: JSON.stringify(req)
  }

  const response = yield call(apiFetchSaga, action, { parseToJSON: true })
  yield put(saveAndCloseRes(response.data))

  yield put(setRoute(Routes.RESUME_APPLICATION))
}

function* submitReqSaga() {
  const action = {
    actionName: remoteActions.submitForm
  }

  const { data: { status } } = yield call(apiFetchSaga, action, { parseToJSON: true })

  yield put(submitRes({ status }))

  if (status === RESPONSE_STATUS.OK) {
    yield put(startIntegrationStatusPolling())
  } else {
    yield put(finalSubmitionStatus(RESPONSE_STATUS.ERR))
  }
}

function* getUserSession() {
  const action = {
    actionName: remoteActions.getSessionId
  }
  
  const { data } = yield call(apiFetchSaga, action, { parseToJSON: false })
  yield put(getSessionRes(data))
}

function* apiValidateFieldsSaga({ payload: { validateFields, formValues, type } }) {
  const addressValidationAction = remoteActions.verifyAddress
  const ibanValidationActions = remoteActions.verifyIBAN
  const req = buildApiValidationRequest(validateFields, formValues)
  const prefix = getSectionPrefix(validateFields)

  const action = {
    actionName: type === 'IBAN' ? ibanValidationActions : addressValidationAction,
    args: JSON.stringify(type === 'IBAN' ? { ...req, ch3_s1_field4: 'Germany' } : { ...req })
  }

  yield put(toggleValidationModal(type))
  yield put(apiValidateLoading(true))

  yield put(apiValidateFieldsReq())
  const { data: { status, ...res } } = yield call(apiFetchSaga, action, { parseToJSON: true })

  yield put(setValidationStatus(status))
  yield put(apiValidateLoading(false))
  yield put(apiValidateFieldsRes(setSectionPrefix(prefix, res)))
}


function* integrationStatusReqLoop() {
  const action = {
    actionName: remoteActions.getIntegrationStatus
  }

  while (true) {
    const { data: status } = yield call(apiFetchSaga, action, { parseToJSON: false })

    yield put(getIntegrationStatusRes({ status }))

    if (status !== RESPONSE_INTEGRATION_API_STATUS.PENDING) {
      yield put(finalSubmitionStatus(status === RESPONSE_INTEGRATION_API_STATUS.ERROR ? RESPONSE_STATUS.ERR : RESPONSE_STATUS.OK))
      yield put(stopIntegrationStatusPolling({ status }))
    } else {
      yield call(delay, 2000)
    }
  }
}

function* watchIntegrationStatusPolling() {
  yield race({
    task: call(integrationStatusReqLoop),
    cancel: take(APPLICATION_FORM.STOP_INTEGRATION_STATUS_POLLING)
  })
}

function* routeToFinalScreen({ payload }) {
  const route = payload === RESPONSE_STATUS.OK ? Routes.SUBMIT_APP_FORM_SUCCESS : Routes.SUBMIT_APP_FORM_ERROR
  yield put(setRoute(route))
}

export default function* root() {
  yield all([
    takeLatest(APPLICATION_FORM.AGREE_TAC, agreeTACSaga),
    takeLatest(APPLICATION_FORM.SAVE, saveSaga),
    takeLatest(APPLICATION_FORM.SAVE_AND_CLOSE, saveAndCloseSaga),
    takeLatest(APPLICATION_FORM.SUBMIT_REQ, submitReqSaga),
    takeLatest(APPLICATION_FORM.START_INTEGRATION_STATUS_POLLING, watchIntegrationStatusPolling),
    takeLatest(APPLICATION_FORM.GET_SESSTION_ID_REQ, getUserSession),
    takeLatest(APPLICATION_FORM.API_VALIDATE_FIELDS, apiValidateFieldsSaga),
    takeLatest(APPLICATION_FORM.FINAL_SUBMITION_STATUS, routeToFinalScreen),
  ])
}

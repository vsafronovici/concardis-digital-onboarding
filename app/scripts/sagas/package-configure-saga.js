import { all, call, put, select, takeLatest } from 'redux-saga/effects'

import { PACKAGE_CONFIGURE } from '../actions/types'
import {
  validateDiscountCodeReq, validateDiscountCodeRes, applyDiscountReq, applyDiscountRes,
  submitQuoteRes, getQuoteRes, submitQuoteReq
} from '../actions/package-configure-action'
import { failedApiFetch } from '../actions/app-action'
import { apiFetchSaga } from './app-saga'
import { applyDiscountPayloadSelector, saveQuoteResponseSelector } from '../selectors/package-configure-selector'
import { RESPONSE_STATUS_CODE, Routes } from '../utils/constants'
import { remoteActions } from '../utils/vf-utils'
import { setRoute } from '../actions/route-action'
import { i18nLangSelector } from '../selectors/i18n-selector'

export function* getQuoteSaga() {
  const lang = yield select(i18nLangSelector)
  const action = {
    actionName: remoteActions.getQuote,
    args: lang
  }
  const response = yield call(apiFetchSaga, action, { parseToJSON: true })
  yield put(getQuoteRes(response.data))
}

function* applyDiscountSaga() {
  const payload = yield select(applyDiscountPayloadSelector)
  yield put(validateDiscountCodeReq(payload))
}

function* validateDiscountCodeReqSaga({ payload }) {
  const action = {
    actionName: remoteActions.validateDiscount,
    args: JSON.stringify(payload)
  }

  const response = yield call(apiFetchSaga, action, { parseToJSON: true, memoize: true })
  yield put(validateDiscountCodeRes(response.data))
}

function* validateDiscountCodeResSaga({ payload }) {
  if (payload.code === RESPONSE_STATUS_CODE.OK) {
    const applyDiscountPayload = yield select(applyDiscountPayloadSelector)
    yield put(applyDiscountReq(applyDiscountPayload))
  }
}

function* applyDiscountReqSaga({ payload }) {
  const action = {
    actionName: remoteActions.applyDiscount,
    args: JSON.stringify(payload)
  }

  const response = yield call(apiFetchSaga, action, { parseToJSON: true, memoize: true })
  yield put(applyDiscountRes(response.data))
}

function* submitQuoteReqSaga(payload) {
  yield put(submitQuoteReq(payload))
  const action = {
    actionName: remoteActions.acceptQuote,
    args: JSON.stringify(payload)
  }
  const response = yield call(apiFetchSaga, action, { parseToJSON: true })
  yield put(submitQuoteRes(response.data))
}

function* confirmOfferSaga({ payload }) {
  yield call(submitQuoteReqSaga, payload)

  const { code } = yield select(saveQuoteResponseSelector)

  if (code === RESPONSE_STATUS_CODE.OK) {
    yield put(setRoute(Routes.START_APPLICATION))
  } else {
    yield put(failedApiFetch('Error when saving Quote'))
  }
}

export default function* root() {
  yield all([
    takeLatest(PACKAGE_CONFIGURE.APPLY_DISCOUNT, applyDiscountSaga),
    takeLatest(PACKAGE_CONFIGURE.VALIDATE_DISCOUNT_CODE_REQ, validateDiscountCodeReqSaga),
    takeLatest(PACKAGE_CONFIGURE.VALIDATE_DISCOUNT_CODE_RES, validateDiscountCodeResSaga),
    takeLatest(PACKAGE_CONFIGURE.APPLY_DISCOUNT_REQ, applyDiscountReqSaga),
    takeLatest(PACKAGE_CONFIGURE.CONFIRM_OFFER, confirmOfferSaga),
  ])
}

import { all, call, put, takeLatest, select } from 'redux-saga/effects'
import { ROUTE } from './../actions/types'
import { PAGE_SITE_SETTINGS, PageContainers, Routes } from '../utils/constants'
import { statePageSetting } from '../utils/vf-utils'
import { setRoute } from '../actions/route-action'
import { initDataSaga as initConfiguratorDataSaga } from './configurator-saga'
import { step1MetaSelector } from '../selectors/configurator-selector'
import { isNilOrEmpty } from '../utils/function-utils'
import { quoteSelector } from '../selectors/package-configure-selector'
import { reviewModeSelector } from '../selectors/application-form-selector'
import { initApplicationFormDataSaga } from './application-form-saga'
import { getQuoteSaga } from './package-configure-saga'
import { prevRouteSelector } from '../selectors/router-selectors'



function* initRouteSaga({ payload: { rootId } }) {
  console.log('initRouteSaga', rootId)
  let route

  if (rootId === PageContainers.CONFIGURATOR_PAGE) {
    route = Routes.STEP1
  } else {
    switch (statePageSetting) {
      case PAGE_SITE_SETTINGS.START_APP:
        route = Routes.EDIT_PACKAGE
        break
      case PAGE_SITE_SETTINGS.RESUME_APP:
        route = Routes.RESUME_APPLICATION
        break
      case PAGE_SITE_SETTINGS.QUOTE_EXPIRED:
        route = Routes.QUOTE_EXPIRED
        break
    }
  }

  yield put(setRoute(route))
}

function* fetchStep1Meta() {
  const step1Data = yield select(step1MetaSelector)
  if (isNilOrEmpty(step1Data)) {
    yield call(initConfiguratorDataSaga)
  }
}

function* fetchQuote() {
  const quote = yield select(quoteSelector)
  if (isNilOrEmpty(quote)) {
    yield call(getQuoteSaga)
  }
}

function* prepareRouteWithDataSaga({ payload: route}) {
  const prevRoute = yield select(prevRouteSelector)
  console.log('prepareRouteWithDataSaga', {prevRoute, route})

  if (prevRoute === route) {
    return
  }

  switch (route) {
    case Routes.STEP1: {
      yield call(fetchStep1Meta)
      break
    }
    case Routes.EDIT_PACKAGE:
    case Routes.START_APPLICATION:
    case Routes.RESUME_APPLICATION: {
      yield call(fetchQuote)
      break
    }
    case Routes.APP_FORM_SWITCHER: {
      yield call(fetchQuote)
      yield call(initApplicationFormDataSaga)
      const reviewMode = yield select(reviewModeSelector)
      const reRoute = reviewMode ? Routes.REVIEW_APP_FORM : Routes.APP_FORM
      yield put(setRoute(reRoute))
      break
    }
    default:
      return
  }
}

export default function* root() {
  yield all([
    takeLatest(ROUTE.INIT_ROUTER, initRouteSaga),
    takeLatest(ROUTE.SET_ROUTE, prepareRouteWithDataSaga)
  ])
}

let history

export const initHistory = _history => history = _history

/*
* Router history that could be used in sagas as well
*
* */
export const getHistory = () => history

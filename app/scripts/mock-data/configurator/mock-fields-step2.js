const productsQ = [{
    "unitPrice": {
      "valuePerMonth": 66.6,
      "value": null,
      "formattedValuePerMonth": "EUR 66.6",
      "formattedValue": null,
      "currencySymbol": "EUR "
    },
    "totalPriceWithDiscount": {
      "valuePerMonth": 66.6,
      "value": 0,
      "formattedValuePerMonth": "EUR 66.6",
      "formattedValue": "EUR 0",
      "currencySymbol": "EUR"
    },
    "totalPriceBeforeDiscount": {
      "valuePerMonth": 66.6,
      "value": 0,
      "formattedValuePerMonth": "EUR 66.6",
      "formattedValue": "EUR 0",
      "currencySymbol": "EUR "
    },
    "terminal": {
      "quoteItemId": "a1D0Q000000CZmzUAG",
      "quantity": {"value": 1, "minValue": null, "maxValue": null},
      "product2Id": "01t0Q000000SWj0QAG",
      "price": {
        "valuePerMonth": null,
        "value": null,
        "formattedValuePerMonth": null,
        "formattedValue": "EUR null",
        "currencySymbol": "EUR "
      },
      "name": "",
      "imageUrl": null,
      "imageAlternateText": null,
      "description": "Verifone V200c"
    },
    "quoteValidTill": "2018-11-01",
    "quoteItemId": "01t0Q000000SvpeQAC",
    "quoteId": "01t0Q000000SvpeQAC",
    "quoteCard": "",
    "quantity": {"value": 1, "minValue": null, "maxValue": null},
    "product2Id": "01t0Q000000SvpeQAC",
    "name": "Verifone V200c Bundle",
    "includedItems": [{
      "quoteItemId": "a1D0Q000000CZn4UAG",
      "quantity": {"value": 3, "minValue": null, "maxValue": null},
      "product2Id": "01t0Q000000SPpeQAG",
      "price": {
        "valuePerMonth": null,
        "value": null,
        "formattedValuePerMonth": null,
        "formattedValue": "EUR null",
        "currencySymbol": "EUR "
      },
      "name": "",
      "imageUrl": null,
      "imageAlternateText": null,
      "description": "Paper Roll"
    }, {
      "quoteItemId": "a1D0Q000000CZn9UAG",
      "quantity": {"value": 1, "minValue": null, "maxValue": null},
      "product2Id": "01t0Q000000SPpUQAW",
      "price": {
        "valuePerMonth": null,
        "value": null,
        "formattedValuePerMonth": null,
        "formattedValue": "EUR null",
        "currencySymbol": "EUR "
      },
      "name": "",
      "imageUrl": null,
      "imageAlternateText": null,
      "description": "Multiport (power + LAN) cable V200c 1.8 m"
    }, {
      "quoteItemId": "a1D0Q000000CZnEUAW",
      "quantity": {"value": 1, "minValue": null, "maxValue": null},
      "product2Id": "01t0Q000000SPpZQAW",
      "price": {
        "valuePerMonth": null,
        "value": null,
        "formattedValuePerMonth": null,
        "formattedValue": "EUR null",
        "currencySymbol": "EUR "
      },
      "name": "",
      "imageUrl": null,
      "imageAlternateText": null,
      "description": "Guidebook & Marketing material"
    }],
    "features": [{
      "quoteItemId": "a1D0Q000000CZnJUAW",
      "quantity": {"value": 1, "minValue": null, "maxValue": null},
      "product2Id": "01t0Q000000SPpyQAG",
      "price": {
        "valuePerMonth": null,
        "value": null,
        "formattedValuePerMonth": null,
        "formattedValue": "EUR null",
        "currencySymbol": "EUR "
      },
      "name": "",
      "imageUrl": null,
      "imageAlternateText": null,
      "description": "Giro"
    }, {
      "quoteItemId": "a1D0Q000000CZnOUAW",
      "quantity": {"value": 1, "minValue": null, "maxValue": null},
      "product2Id": "01t0Q000000SPptQAG",
      "price": {
        "valuePerMonth": null,
        "value": null,
        "formattedValuePerMonth": null,
        "formattedValue": "EUR null",
        "currencySymbol": "EUR "
      },
      "name": "",
      "imageUrl": null,
      "imageAlternateText": null,
      "description": "Visa"
    }, {
      "quoteItemId": "a1D0Q000000CZnTUAW",
      "quantity": {"value": 1, "minValue": null, "maxValue": null},
      "product2Id": "01t0Q000000SPpoQAG",
      "price": {
        "valuePerMonth": null,
        "value": null,
        "formattedValuePerMonth": null,
        "formattedValue": "EUR null",
        "currencySymbol": "EUR "
      },
      "name": "",
      "imageUrl": null,
      "imageAlternateText": null,
      "description": "MC"
    }],
    "extraItems": [{
      "quoteItemId": "a1D0Q000000CZnYUAW",
      "quantity": {"value": 0, "minValue": null, "maxValue": null},
      "product2Id": "01t0Q000000SPpjQAG",
      "price": {
        "valuePerMonth": null,
        "value": null,
        "formattedValuePerMonth": null,
        "formattedValue": "EUR null",
        "currencySymbol": "EUR "
      },
      "name": "",
      "imageUrl": null,
      "imageAlternateText": null,
      "description": "Spacepole mounting"
    }, {
      "quoteItemId": "a1D0Q000000CZndUAG",
      "quantity": {"value": 0, "minValue": null, "maxValue": null},
      "product2Id": "01t0Q000000SPpeQAG",
      "price": {
        "valuePerMonth": null,
        "value": null,
        "formattedValuePerMonth": null,
        "formattedValue": "EUR null",
        "currencySymbol": "EUR "
      },
      "name": "",
      "imageUrl": null,
      "imageAlternateText": null,
      "description": "Paper Roll"
    }],
    "dirty": null,
    "description": "Product Bundle",
    "cpqObject": null
  }, {
    "unitPrice": {
      "valuePerMonth": 66.6,
      "value": null,
      "formattedValuePerMonth": "EUR 66.6",
      "formattedValue": null,
      "currencySymbol": "EUR "
    },
    "totalPriceWithDiscount": {
      "valuePerMonth": 66.6,
      "value": 0,
      "formattedValuePerMonth": "EUR 66.6",
      "formattedValue": "EUR 0",
      "currencySymbol": "EUR "
    },
    "totalPriceBeforeDiscount": {
      "valuePerMonth": 66.6,
      "value": 0,
      "formattedValuePerMonth": "EUR 66.6",
      "formattedValue": "EUR 0",
      "currencySymbol": "EUR "
    },
    "terminal": {
      "quoteItemId": "a1D0Q000000DjO5UAK",
      "quantity": {"value": 1, "minValue": null, "maxValue": null},
      "product2Id": "01t0Q000000SWj0QAG",
      "price": {
        "valuePerMonth": null,
        "value": 800,
        "formattedValuePerMonth": null,
        "formattedValue": "EUR 800.0",
        "currencySymbol": "EUR "
      },
      "name": "",
      "imageUrl": null,
      "imageAlternateText": null,
      "description": "Verifone V200c"
    },
    "quoteValidTill": "2018-11-01",
    "quoteItemId": "01t0Q000000ZkIuQAK",
    "quoteId": "01t0Q000000ZkIuQAK",
    "quoteCard": "",
    "quantity": {"value": 1, "minValue": null, "maxValue": null},
    "product2Id": "01t0Q000000ZkIuQAK",
    "name": "Verifone V200c Bundle 2",
    "includedItems": [{
      "quoteItemId": "a1D0Q000000DjO0UAK",
      "quantity": {"value": 3, "minValue": null, "maxValue": null},
      "product2Id": "01t0Q000000SPpeQAG",
      "price": {
        "valuePerMonth": null,
        "value": 50,
        "formattedValuePerMonth": null,
        "formattedValue": "EUR 50.0",
        "currencySymbol": "EUR "
      },
      "name": "",
      "imageUrl": null,
      "imageAlternateText": null,
      "description": "Paper Roll"
    }, {
      "quoteItemId": "a1D0Q000000DjO6UAK",
      "quantity": {"value": 1, "minValue": null, "maxValue": null},
      "product2Id": "01t0Q000000SPpUQAW",
      "price": {
        "valuePerMonth": null,
        "value": 100,
        "formattedValuePerMonth": null,
        "formattedValue": "EUR 100.0",
        "currencySymbol": "EUR "
      },
      "name": "",
      "imageUrl": null,
      "imageAlternateText": null,
      "description": "Multiport (power + LAN) cable V200c 1.8 m"
    }, {
      "quoteItemId": "a1D0Q000000DjO1UAK",
      "quantity": {"value": 1, "minValue": null, "maxValue": null},
      "product2Id": "01t0Q000000SPpZQAW",
      "price": {
        "valuePerMonth": null,
        "value": 0,
        "formattedValuePerMonth": null,
        "formattedValue": "EUR 0.0",
        "currencySymbol": "EUR "
      },
      "name": "",
      "imageUrl": null,
      "imageAlternateText": null,
      "description": "Guidebook & Marketing material"
    }],
    "features": [{
      "quoteItemId": "a1D0Q000000DjO3UAK",
      "quantity": {"value": 1, "minValue": null, "maxValue": null},
      "product2Id": "01t0Q000000SPptQAG",
      "price": {
        "valuePerMonth": null,
        "value": 0,
        "formattedValuePerMonth": null,
        "formattedValue": "EUR 0.0",
        "currencySymbol": "EUR "
      },
      "name": "",
      "imageUrl": null,
      "imageAlternateText": null,
      "description": "Visa"
    }],
    "extraItems": [{
      "quoteItemId": "a1D0Q000000DjO2UAK",
      "quantity": {"value": 0, "minValue": null, "maxValue": null},
      "product2Id": "01t0Q000000SPpjQAG",
      "price": {
        "valuePerMonth": null,
        "value": 75,
        "formattedValuePerMonth": null,
        "formattedValue": "EUR 75.0",
        "currencySymbol": "EUR "
      },
      "name": "",
      "imageUrl": null,
      "imageAlternateText": null,
      "description": "Spacepole mounting"
    }, {
      "quoteItemId": "a1D0Q000000DjO7UAK",
      "quantity": {"value": 0, "minValue": null, "maxValue": null},
      "product2Id": "01t0Q000000SPpeQAG",
      "price": {
        "valuePerMonth": null,
        "value": 12,
        "formattedValuePerMonth": null,
        "formattedValue": "EUR 12.0",
        "currencySymbol": "EUR "
      },
      "name": "",
      "imageUrl": null,
      "imageAlternateText": null,
      "description": "Paper Roll"
    }],
    "dirty": null,
    "description": "Product Bundle",
    "cpqObject": null
  }, {
    "unitPrice": {
      "valuePerMonth": 66.6,
      "value": null,
      "formattedValuePerMonth": "EUR 66.6",
      "formattedValue": null,
      "currencySymbol": "EUR "
    },
    "totalPriceWithDiscount": {
      "valuePerMonth": 66.6,
      "value": 0,
      "formattedValuePerMonth": "EUR 66.6",
      "formattedValue": "EUR 0",
      "currencySymbol": "EUR "
    },
    "totalPriceBeforeDiscount": {
      "valuePerMonth": 66.6,
      "value": 0,
      "formattedValuePerMonth": "EUR 66.6",
      "formattedValue": "EUR 0",
      "currencySymbol": "EUR "
    },
    "terminal": {
      "quoteItemId": "a1D0Q000000DjOEUA0",
      "quantity": {"value": 1, "minValue": null, "maxValue": null},
      "product2Id": "01t0Q000000SWj0QAG",
      "price": {
        "valuePerMonth": null,
        "value": 800,
        "formattedValuePerMonth": null,
        "formattedValue": "EUR 800.0",
        "currencySymbol": "EUR "
      },
      "name": "",
      "imageUrl": null,
      "imageAlternateText": null,
      "description": "Verifone V200c"
    },
    "quoteValidTill": "2018-11-01",
    "quoteItemId": "01t0Q000000ZkIzQAK",
    "quoteId": "01t0Q000000ZkIzQAK",
    "quoteCard": "",
    "quantity": {"value": 1, "minValue": null, "maxValue": null},
    "product2Id": "01t0Q000000ZkIzQAK",
    "name": "Verifone V200c Bundle 3",
    "includedItems": [{
      "quoteItemId": "a1D0Q000000DjO9UAK",
      "quantity": {"value": 3, "minValue": null, "maxValue": null},
      "product2Id": "01t0Q000000SPpeQAG",
      "price": {
        "valuePerMonth": null,
        "value": 50,
        "formattedValuePerMonth": null,
        "formattedValue": "EUR 50.0",
        "currencySymbol": "EUR "
      },
      "name": "",
      "imageUrl": null,
      "imageAlternateText": null,
      "description": "Paper Roll"
    }, {
      "quoteItemId": "a1D0Q000000DjOFUA0",
      "quantity": {"value": 1, "minValue": null, "maxValue": null},
      "product2Id": "01t0Q000000SPpUQAW",
      "price": {
        "valuePerMonth": null,
        "value": 100,
        "formattedValuePerMonth": null,
        "formattedValue": "EUR 100.0",
        "currencySymbol": "EUR "
      },
      "name": "",
      "imageUrl": null,
      "imageAlternateText": null,
      "description": "Multiport (power + LAN) cable V200c 1.8 m"
    }, {
      "quoteItemId": "a1D0Q000000DjOAUA0",
      "quantity": {"value": 1, "minValue": null, "maxValue": null},
      "product2Id": "01t0Q000000SPpZQAW",
      "price": {
        "valuePerMonth": null,
        "value": 0,
        "formattedValuePerMonth": null,
        "formattedValue": "EUR 0.0",
        "currencySymbol": "EUR "
      },
      "name": "",
      "imageUrl": null,
      "imageAlternateText": null,
      "description": "Guidebook & Marketing material"
    }],
    "features": [{
      "quoteItemId": "a1D0Q000000DjOHUA0",
      "quantity": {"value": 1, "minValue": null, "maxValue": null},
      "product2Id": "01t0Q000000SPpoQAG",
      "price": {
        "valuePerMonth": null,
        "value": 0,
        "formattedValuePerMonth": null,
        "formattedValue": "EUR 0.0",
        "currencySymbol": "EUR "
      },
      "name": "",
      "imageUrl": null,
      "imageAlternateText": null,
      "description": "MC"
    }],
    "extraItems": [{
      "quoteItemId": "a1D0Q000000DjOGUA0",
      "quantity": {"value": 0, "minValue": null, "maxValue": null},
      "product2Id": "01t0Q000000SPpeQAG",
      "price": {
        "valuePerMonth": null,
        "value": 12,
        "formattedValuePerMonth": null,
        "formattedValue": "EUR 12.0",
        "currencySymbol": "EUR "
      },
      "name": "",
      "imageUrl": null,
      "imageAlternateText": null,
      "description": "Paper Roll"
    }],
    "dirty": null,
    "description": "Product Bundle",
    "cpqObject": null
    }
  ]

const productsQinternal = [{
  "unitPrice": {
    "valuePerMonth": 66.6,
    "value": null,
    "formattedValuePerMonth": "EUR 66.6",
    "formattedValue": null,
    "currencySymbol": "EUR "
  },
  "totalPriceWithDiscount": {
    "valuePerMonth": 66.6,
    "value": 0,
    "formattedValuePerMonth": "EUR 66.6",
    "formattedValue": "EUR 0",
    "currencySymbol": "EUR"
  },
  "totalPriceBeforeDiscount": {
    "valuePerMonth": 66.6,
    "value": 0,
    "formattedValuePerMonth": "EUR 66.6",
    "formattedValue": "EUR 0",
    "currencySymbol": "EUR "
  },
  "terminal": {
    "quoteItemId": "a1D0Q000000CZmzUAG",
    "quantity": {"value": 1, "minValue": null, "maxValue": null},
    "product2Id": "01t0Q000000SWj0QAG",
    "price": {
      "valuePerMonth": null,
      "value": null,
      "formattedValuePerMonth": null,
      "formattedValue": "EUR null",
      "currencySymbol": "EUR "
    },
    "name": "",
    "imageUrl": null,
    "imageAlternateText": null,
    "description": "Verifone V200c"
  },
  "quoteValidTill": "2018-11-01",
  "quoteItemId": "01t0Q000000SvpeQAC",
  "quoteId": "01t0Q000000SvpeQAC",
  "quoteCard": "",
  "quantity": {"value": 1, "minValue": null, "maxValue": null},
  "product2Id": "01t0Q000000SvpeQAC",
  "name": "Verifone V200c Bundle",
  "includedItems": [{
    "quoteItemId": "a1D0Q000000CZn4UAG",
    "quantity": {"value": 3, "minValue": null, "maxValue": null},
    "product2Id": "01t0Q000000SPpeQAG",
    "price": {
      "valuePerMonth": null,
      "value": null,
      "formattedValuePerMonth": null,
      "formattedValue": "EUR null",
      "currencySymbol": "EUR "
    },
    "name": "",
    "imageUrl": null,
    "imageAlternateText": null,
    "description": "Paper Roll"
  }, {
    "quoteItemId": "a1D0Q000000CZn9UAG",
    "quantity": {"value": 1, "minValue": null, "maxValue": null},
    "product2Id": "01t0Q000000SPpUQAW",
    "price": {
      "valuePerMonth": null,
      "value": null,
      "formattedValuePerMonth": null,
      "formattedValue": "EUR null",
      "currencySymbol": "EUR "
    },
    "name": "",
    "imageUrl": null,
    "imageAlternateText": null,
    "description": "Multiport (power + LAN) cable V200c 1.8 m"
  }, {
    "quoteItemId": "a1D0Q000000CZnEUAW",
    "quantity": {"value": 1, "minValue": null, "maxValue": null},
    "product2Id": "01t0Q000000SPpZQAW",
    "price": {
      "valuePerMonth": null,
      "value": null,
      "formattedValuePerMonth": null,
      "formattedValue": "EUR null",
      "currencySymbol": "EUR "
    },
    "name": "",
    "imageUrl": null,
    "imageAlternateText": null,
    "description": "Guidebook & Marketing material"
  }],
  "features": [{
    "quoteItemId": "a1D0Q000000CZnJUAW",
    "quantity": {"value": 1, "minValue": null, "maxValue": null},
    "product2Id": "01t0Q000000SPpyQAG",
    "price": {
      "valuePerMonth": null,
      "value": null,
      "formattedValuePerMonth": null,
      "formattedValue": "EUR null",
      "currencySymbol": "EUR "
    },
    "name": "",
    "imageUrl": null,
    "imageAlternateText": null,
    "description": "Giro"
  }, {
    "quoteItemId": "a1D0Q000000CZnOUAW",
    "quantity": {"value": 1, "minValue": null, "maxValue": null},
    "product2Id": "01t0Q000000SPptQAG",
    "price": {
      "valuePerMonth": null,
      "value": null,
      "formattedValuePerMonth": null,
      "formattedValue": "EUR null",
      "currencySymbol": "EUR "
    },
    "name": "",
    "imageUrl": null,
    "imageAlternateText": null,
    "description": "Visa"
  }, {
    "quoteItemId": "a1D0Q000000CZnTUAW",
    "quantity": {"value": 1, "minValue": null, "maxValue": null},
    "product2Id": "01t0Q000000SPpoQAG",
    "price": {
      "valuePerMonth": null,
      "value": null,
      "formattedValuePerMonth": null,
      "formattedValue": "EUR null",
      "currencySymbol": "EUR "
    },
    "name": "",
    "imageUrl": null,
    "imageAlternateText": null,
    "description": "MC"
  }],
  "extraItems": [{
    "quoteItemId": "a1D0Q000000CZnYUAW",
    "quantity": {"value": 0, "minValue": null, "maxValue": null},
    "product2Id": "01t0Q000000SPpjQAG",
    "price": {
      "valuePerMonth": null,
      "value": null,
      "formattedValuePerMonth": null,
      "formattedValue": "EUR null",
      "currencySymbol": "EUR "
    },
    "name": "",
    "imageUrl": null,
    "imageAlternateText": null,
    "description": "Spacepole mounting"
  }, {
    "quoteItemId": "a1D0Q000000CZndUAG",
    "quantity": {"value": 0, "minValue": null, "maxValue": null},
    "product2Id": "01t0Q000000SPpeQAG",
    "price": {
      "valuePerMonth": null,
      "value": null,
      "formattedValuePerMonth": null,
      "formattedValue": "EUR null",
      "currencySymbol": "EUR "
    },
    "name": "",
    "imageUrl": null,
    "imageAlternateText": null,
    "description": "Paper Roll"
  }],
  "dirty": null,
  "description": "Product Bundle",
  "cpqObject": null
}, {
  "unitPrice": {
    "valuePerMonth": 66.6,
    "value": null,
    "formattedValuePerMonth": "EUR 66.6",
    "formattedValue": null,
    "currencySymbol": "EUR "
  },
  "totalPriceWithDiscount": {
    "valuePerMonth": 66.6,
    "value": 0,
    "formattedValuePerMonth": "EUR 66.6",
    "formattedValue": "EUR 0",
    "currencySymbol": "EUR "
  },
  "totalPriceBeforeDiscount": {
    "valuePerMonth": 66.6,
    "value": 0,
    "formattedValuePerMonth": "EUR 66.6",
    "formattedValue": "EUR 0",
    "currencySymbol": "EUR "
  },
  "terminal": {
    "quoteItemId": "a1D0Q000000DjO5UAK",
    "quantity": {"value": 1, "minValue": null, "maxValue": null},
    "product2Id": "01t0Q000000SWj0QAG",
    "price": {
      "valuePerMonth": null,
      "value": 800,
      "formattedValuePerMonth": null,
      "formattedValue": "EUR 800.0",
      "currencySymbol": "EUR "
    },
    "name": "",
    "imageUrl": null,
    "imageAlternateText": null,
    "description": "Verifone V200c"
  },
  "quoteValidTill": "2018-11-01",
  "quoteItemId": "01t0Q000000ZkIuQAK",
  "quoteId": "01t0Q000000ZkIuQAK",
  "quoteCard": "",
  "quantity": {"value": 1, "minValue": null, "maxValue": null},
  "product2Id": "01t0Q000000ZkIuQAK",
  "name": "Verifone V200c Bundle 2",
  "includedItems": [{
    "quoteItemId": "a1D0Q000000DjO0UAK",
    "quantity": {"value": 3, "minValue": null, "maxValue": null},
    "product2Id": "01t0Q000000SPpeQAG",
    "price": {
      "valuePerMonth": null,
      "value": 50,
      "formattedValuePerMonth": null,
      "formattedValue": "EUR 50.0",
      "currencySymbol": "EUR "
    },
    "name": "",
    "imageUrl": null,
    "imageAlternateText": null,
    "description": "Paper Roll"
  }, {
    "quoteItemId": "a1D0Q000000DjO6UAK",
    "quantity": {"value": 1, "minValue": null, "maxValue": null},
    "product2Id": "01t0Q000000SPpUQAW",
    "price": {
      "valuePerMonth": null,
      "value": 100,
      "formattedValuePerMonth": null,
      "formattedValue": "EUR 100.0",
      "currencySymbol": "EUR "
    },
    "name": "",
    "imageUrl": null,
    "imageAlternateText": null,
    "description": "Multiport (power + LAN) cable V200c 1.8 m"
  }, {
    "quoteItemId": "a1D0Q000000DjO1UAK",
    "quantity": {"value": 1, "minValue": null, "maxValue": null},
    "product2Id": "01t0Q000000SPpZQAW",
    "price": {
      "valuePerMonth": null,
      "value": 0,
      "formattedValuePerMonth": null,
      "formattedValue": "EUR 0.0",
      "currencySymbol": "EUR "
    },
    "name": "",
    "imageUrl": null,
    "imageAlternateText": null,
    "description": "Guidebook & Marketing material"
  }],
  "features": [{
    "quoteItemId": "a1D0Q000000DjO3UAK",
    "quantity": {"value": 1, "minValue": null, "maxValue": null},
    "product2Id": "01t0Q000000SPptQAG",
    "price": {
      "valuePerMonth": null,
      "value": 0,
      "formattedValuePerMonth": null,
      "formattedValue": "EUR 0.0",
      "currencySymbol": "EUR "
    },
    "name": "",
    "imageUrl": null,
    "imageAlternateText": null,
    "description": "Visa"
  }],
  "extraItems": [{
    "quoteItemId": "a1D0Q000000DjO2UAK",
    "quantity": {"value": 0, "minValue": null, "maxValue": null},
    "product2Id": "01t0Q000000SPpjQAG",
    "price": {
      "valuePerMonth": null,
      "value": 75,
      "formattedValuePerMonth": null,
      "formattedValue": "EUR 75.0",
      "currencySymbol": "EUR "
    },
    "name": "",
    "imageUrl": null,
    "imageAlternateText": null,
    "description": "Spacepole mounting"
  }, {
    "quoteItemId": "a1D0Q000000DjO7UAK",
    "quantity": {"value": 0, "minValue": null, "maxValue": null},
    "product2Id": "01t0Q000000SPpeQAG",
    "price": {
      "valuePerMonth": null,
      "value": 12,
      "formattedValuePerMonth": null,
      "formattedValue": "EUR 12.0",
      "currencySymbol": "EUR "
    },
    "name": "",
    "imageUrl": null,
    "imageAlternateText": null,
    "description": "Paper Roll"
  }],
  "dirty": null,
  "description": "Product Bundle",
  "cpqObject": null
}, {
  "unitPrice": {
    "valuePerMonth": 66.6,
    "value": null,
    "formattedValuePerMonth": "EUR 66.6",
    "formattedValue": null,
    "currencySymbol": "EUR "
  },
  "totalPriceWithDiscount": {
    "valuePerMonth": 66.6,
    "value": 0,
    "formattedValuePerMonth": "EUR 66.6",
    "formattedValue": "EUR 0",
    "currencySymbol": "EUR "
  },
  "totalPriceBeforeDiscount": {
    "valuePerMonth": 66.6,
    "value": 0,
    "formattedValuePerMonth": "EUR 66.6",
    "formattedValue": "EUR 0",
    "currencySymbol": "EUR "
  },
  "terminal": {
    "quoteItemId": "a1D0Q000000DjOEUA0",
    "quantity": {"value": 1, "minValue": null, "maxValue": null},
    "product2Id": "01t0Q000000SWj0QAG",
    "price": {
      "valuePerMonth": null,
      "value": 800,
      "formattedValuePerMonth": null,
      "formattedValue": "EUR 800.0",
      "currencySymbol": "EUR "
    },
    "name": "",
    "imageUrl": null,
    "imageAlternateText": null,
    "description": "Verifone V200c"
  },
  "quoteValidTill": "2018-11-01",
  "quoteItemId": "01t0Q000000ZkIzQAK",
  "quoteId": "01t0Q000000ZkIzQAK",
  "quoteCard": "",
  "quantity": {"value": 1, "minValue": null, "maxValue": null},
  "product2Id": "01t0Q000000ZkIzQAK",
  "name": "Verifone V200c Bundle 3",
  "includedItems": [{
    "quoteItemId": "a1D0Q000000DjO9UAK",
    "quantity": {"value": 3, "minValue": null, "maxValue": null},
    "product2Id": "01t0Q000000SPpeQAG",
    "price": {
      "valuePerMonth": null,
      "value": 50,
      "formattedValuePerMonth": null,
      "formattedValue": "EUR 50.0",
      "currencySymbol": "EUR "
    },
    "name": "",
    "imageUrl": null,
    "imageAlternateText": null,
    "description": "Paper Roll"
  }, {
    "quoteItemId": "a1D0Q000000DjOFUA0",
    "quantity": {"value": 1, "minValue": null, "maxValue": null},
    "product2Id": "01t0Q000000SPpUQAW",
    "price": {
      "valuePerMonth": null,
      "value": 100,
      "formattedValuePerMonth": null,
      "formattedValue": "EUR 100.0",
      "currencySymbol": "EUR "
    },
    "name": "",
    "imageUrl": null,
    "imageAlternateText": null,
    "description": "Multiport (power + LAN) cable V200c 1.8 m"
  }, {
    "quoteItemId": "a1D0Q000000DjOAUA0",
    "quantity": {"value": 1, "minValue": null, "maxValue": null},
    "product2Id": "01t0Q000000SPpZQAW",
    "price": {
      "valuePerMonth": null,
      "value": 0,
      "formattedValuePerMonth": null,
      "formattedValue": "EUR 0.0",
      "currencySymbol": "EUR "
    },
    "name": "",
    "imageUrl": null,
    "imageAlternateText": null,
    "description": "Guidebook & Marketing material"
  }],
  "features": [{
    "quoteItemId": "a1D0Q000000DjOHUA0",
    "quantity": {"value": 1, "minValue": null, "maxValue": null},
    "product2Id": "01t0Q000000SPpoQAG",
    "price": {
      "valuePerMonth": null,
      "value": 0,
      "formattedValuePerMonth": null,
      "formattedValue": "EUR 0.0",
      "currencySymbol": "EUR "
    },
    "name": "",
    "imageUrl": null,
    "imageAlternateText": null,
    "description": "MC"
  }],
  "extraItems": [{
    "quoteItemId": "a1D0Q000000DjOGUA0",
    "quantity": {"value": 0, "minValue": null, "maxValue": null},
    "product2Id": "01t0Q000000SPpeQAG",
    "price": {
      "valuePerMonth": null,
      "value": 12,
      "formattedValuePerMonth": null,
      "formattedValue": "EUR 12.0",
      "currencySymbol": "EUR "
    },
    "name": "",
    "imageUrl": null,
    "imageAlternateText": null,
    "description": "Paper Roll"
  }],
  "dirty": null,
  "description": "Product Bundle",
  "cpqObject": null
},
  {
    "unitPrice": {
      "valuePerMonth": 66.6,
      "value": null,
      "formattedValuePerMonth": "EUR 66.6",
      "formattedValue": null,
      "currencySymbol": "EUR "
    },
    "totalPriceWithDiscount": {
      "valuePerMonth": 66.6,
      "value": 0,
      "formattedValuePerMonth": "EUR 66.6",
      "formattedValue": "EUR 0",
      "currencySymbol": "EUR"
    },
    "totalPriceBeforeDiscount": {
      "valuePerMonth": 66.6,
      "value": 0,
      "formattedValuePerMonth": "EUR 66.6",
      "formattedValue": "EUR 0",
      "currencySymbol": "EUR "
    },
    "terminal": {
      "quoteItemId": "a1D0Q000000CZmzUAG",
      "quantity": {"value": 1, "minValue": null, "maxValue": null},
      "product2Id": "01t0Q000000SWj0QAG",
      "price": {
        "valuePerMonth": null,
        "value": null,
        "formattedValuePerMonth": null,
        "formattedValue": "EUR null",
        "currencySymbol": "EUR "
      },
      "name": "",
      "imageUrl": null,
      "imageAlternateText": null,
      "description": "Verifone V200c"
    },
    "quoteValidTill": "2018-11-01",
    "quoteItemId": "01t0Q000000SvpeQAC",
    "quoteId": "01t0Q000000SvpeQAC",
    "quoteCard": "",
    "quantity": {"value": 1, "minValue": null, "maxValue": null},
    "product2Id": "01t0Q000000SvpeQAC",
    "name": "Verifone V200c Bundle",
    "includedItems": [{
      "quoteItemId": "a1D0Q000000CZn4UAG",
      "quantity": {"value": 3, "minValue": null, "maxValue": null},
      "product2Id": "01t0Q000000SPpeQAG",
      "price": {
        "valuePerMonth": null,
        "value": null,
        "formattedValuePerMonth": null,
        "formattedValue": "EUR null",
        "currencySymbol": "EUR "
      },
      "name": "",
      "imageUrl": null,
      "imageAlternateText": null,
      "description": "Paper Roll"
    }, {
      "quoteItemId": "a1D0Q000000CZn9UAG",
      "quantity": {"value": 1, "minValue": null, "maxValue": null},
      "product2Id": "01t0Q000000SPpUQAW",
      "price": {
        "valuePerMonth": null,
        "value": null,
        "formattedValuePerMonth": null,
        "formattedValue": "EUR null",
        "currencySymbol": "EUR "
      },
      "name": "",
      "imageUrl": null,
      "imageAlternateText": null,
      "description": "Multiport (power + LAN) cable V200c 1.8 m"
    }, {
      "quoteItemId": "a1D0Q000000CZnEUAW",
      "quantity": {"value": 1, "minValue": null, "maxValue": null},
      "product2Id": "01t0Q000000SPpZQAW",
      "price": {
        "valuePerMonth": null,
        "value": null,
        "formattedValuePerMonth": null,
        "formattedValue": "EUR null",
        "currencySymbol": "EUR "
      },
      "name": "",
      "imageUrl": null,
      "imageAlternateText": null,
      "description": "Guidebook & Marketing material"
    }],
    "features": [{
      "quoteItemId": "a1D0Q000000CZnJUAW",
      "quantity": {"value": 1, "minValue": null, "maxValue": null},
      "product2Id": "01t0Q000000SPpyQAG",
      "price": {
        "valuePerMonth": null,
        "value": null,
        "formattedValuePerMonth": null,
        "formattedValue": "EUR null",
        "currencySymbol": "EUR "
      },
      "name": "",
      "imageUrl": null,
      "imageAlternateText": null,
      "description": "Giro"
    }, {
      "quoteItemId": "a1D0Q000000CZnOUAW",
      "quantity": {"value": 1, "minValue": null, "maxValue": null},
      "product2Id": "01t0Q000000SPptQAG",
      "price": {
        "valuePerMonth": null,
        "value": null,
        "formattedValuePerMonth": null,
        "formattedValue": "EUR null",
        "currencySymbol": "EUR "
      },
      "name": "",
      "imageUrl": null,
      "imageAlternateText": null,
      "description": "Visa"
    }, {
      "quoteItemId": "a1D0Q000000CZnTUAW",
      "quantity": {"value": 1, "minValue": null, "maxValue": null},
      "product2Id": "01t0Q000000SPpoQAG",
      "price": {
        "valuePerMonth": null,
        "value": null,
        "formattedValuePerMonth": null,
        "formattedValue": "EUR null",
        "currencySymbol": "EUR "
      },
      "name": "",
      "imageUrl": null,
      "imageAlternateText": null,
      "description": "MC"
    }],
    "extraItems": [{
      "quoteItemId": "a1D0Q000000CZnYUAW",
      "quantity": {"value": 0, "minValue": null, "maxValue": null},
      "product2Id": "01t0Q000000SPpjQAG",
      "price": {
        "valuePerMonth": null,
        "value": null,
        "formattedValuePerMonth": null,
        "formattedValue": "EUR null",
        "currencySymbol": "EUR "
      },
      "name": "",
      "imageUrl": null,
      "imageAlternateText": null,
      "description": "Spacepole mounting"
    }, {
      "quoteItemId": "a1D0Q000000CZndUAG",
      "quantity": {"value": 0, "minValue": null, "maxValue": null},
      "product2Id": "01t0Q000000SPpeQAG",
      "price": {
        "valuePerMonth": null,
        "value": null,
        "formattedValuePerMonth": null,
        "formattedValue": "EUR null",
        "currencySymbol": "EUR "
      },
      "name": "",
      "imageUrl": null,
      "imageAlternateText": null,
      "description": "Paper Roll"
    }],
    "dirty": null,
    "description": "Product Bundle",
    "cpqObject": null
  }]

export default productsQinternal

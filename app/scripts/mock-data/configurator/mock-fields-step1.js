export default {
  "fields": [{
    "validation": {"validate": true, "requiredError": "1", "required": true},
    "type": "selectable_items",
    "title": "page1.f4.title",
    "optionType": "imageLabel",
    "options": [{"value": "0", "label": "retail_goods", "image": "icon-goods-store"}, {
      "value": "1",
      "label": "retail_services",
      "image": "icon-reception-hotel"
    }, {"value": "2", "label": "food_drink_premises", "image": "icon-restaurant-eating-set"}, {
      "value": "3",
      "label": "food_drink_take_away",
      "image": "icon-fast-food-burger-drink"
    }, {"value": "4", "label": "health_services", "image": "icon-insurance-hands"}, {
      "value": "5",
      "label": "other_industry",
      "image": "icon-network-question"
    }],
    "name": "page1.f1",
    "label": "page1.f4.label",
    "defaultValue": "0"
  }, {
    "validation": {"validate": true, "requiredError": "1", "required": true},
    "type": "selectable_items",
    "options": [{"value": "0", "label": "total_card_turnover_001"}, {
      "value": "1",
      "label": "total_card_turnover_002"
    }, {"value": "2", "label": "total_card_turnover_003"}, {
      "value": "3",
      "label": "total_card_turnover_004"
    }, {"value": "4", "label": "total_card_turnover_005"}],
    "name": "page1.f2",
    "label": "page1.f2.label",
    "defaultValue": "0"
  }, {
    "type": "Slider",
    "options": [{"value": "0", "label": "MinValue"}, {"value": "25", "label": "InterValue1"}, {
      "value": "50",
      "label": "InterValue2"
    }, {"value": "75", "label": "InterValue3"}, {"value": "100", "label": "MaxValue"}],
    "name": "page1.f3",
    "label": "page1.f3.label",
    "defaultValue": "MinValue"
  }]
}

import { MOCK_REMOTE_ACTIONS } from './mock-actions'
import configuratorTranslations from './mock-translations'
import appFormTranslations from './application-form/mock-app-form-translations'
import configuratorStep1 from './configurator/mock-fields-step1'
import configuratorStep2 from './configurator/mock-fields-step2'
import packageCconfigureQuote from './configurator/mock-package-configurator'
import { ok as saveQuote_OK, error as saveQuote_ERR } from './configurator/mock-save-quote'
import { ok as validateDiscountOk, error as validateDiscountErr } from './configurator/mock-validate-discount'
import { ok as applyDiscountOk, error as applyDiscountErr } from './configurator/mock-apply-discount'

import appForm_metadata from './application-form/mock-appForm-metadata'
import { ok as updateComercialsTAC_OK, error as updateComercialsTAC_ERR } from './application-form/mock-update-comercials'
import { ok as validateForm_OK, error as validateForm_ERR } from './application-form/mock-validate-form'
import { success as submitSuccess, error as submitError } from './application-form/mock-submit-form'
import { success as validationSuccess, error as validationError } from './application-form/mock-validation-api'
import { success as ibanSuccessValidation, error as ibanErrorValidation } from './application-form/mock-iban-validation'
import {
  success as integrationStatusSuccess,
  error as integrationStatusError,
  pending as integrationStatusPending
} from './application-form/mock-integration-status'

export const mockResponse = action => {
  switch (action) {
    case MOCK_REMOTE_ACTIONS.getDictionaryMetadata:
      return appFormTranslations

    case MOCK_REMOTE_ACTIONS.getFieldsMetadata:
      return configuratorStep1

    case MOCK_REMOTE_ACTIONS.getProducts:
      return configuratorStep2

    case MOCK_REMOTE_ACTIONS.getQProducts:
      return configuratorStep2

    case MOCK_REMOTE_ACTIONS.getQuote:
      return packageCconfigureQuote

    case MOCK_REMOTE_ACTIONS.acceptQuote:
      return saveQuote_OK

    case MOCK_REMOTE_ACTIONS.validateDiscount:
      return validateDiscountOk

    case MOCK_REMOTE_ACTIONS.applyDiscount:
      return applyDiscountOk

    case MOCK_REMOTE_ACTIONS.getAppFormMetadata:
      return appForm_metadata

    case MOCK_REMOTE_ACTIONS.updateCommercialsTC:
      return updateComercialsTAC_OK

    case MOCK_REMOTE_ACTIONS.validateForm:
      return validateForm_OK

    case MOCK_REMOTE_ACTIONS.saveAndClose:
      return validateForm_OK

    case MOCK_REMOTE_ACTIONS.submitForm:
      return submitSuccess
    
    case MOCK_REMOTE_ACTIONS.verifyAddress:
      return validationSuccess
      
    case MOCK_REMOTE_ACTIONS.verifyIBAN:
      return ibanSuccessValidation

    case MOCK_REMOTE_ACTIONS.getIntegrationStatus:
      return integrationStatusSuccess

    default:
      return undefined
  }
}

export const options = [
  { value: '$optionValue', label: 'Sole owner', label2: `I own 100% of the company, and there's no other beneficial owners` },
  { value: '$optionValue2', label: 'Other owners have more than 25%', label2: `There is one or more additional owners who own more than 25% of
  my business` },
  { value: '$optionValue3', label: 'Other owners own less than 25%', label2: `There is one or more additional owners but they owe less than 25% of
  my business` },

]

export const verticalRadioBtnsMock = [
  {value: 'value1', label: 'Owner'},
  {value: 'value2', label: `I'm not the owner but I have the power of attorney`}
]

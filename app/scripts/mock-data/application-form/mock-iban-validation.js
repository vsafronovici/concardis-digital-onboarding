import { RESPONSE_STATUS } from '../../utils/constants'
export const success = {
  "ch3_s1_field4": "Germany",
  "ch6_s1_field2":"DE05175620761962557989",
  "ch6_s1_field3": "AARBDE5W200",
  status: RESPONSE_STATUS.OK
}

export const error = {
  status: RESPONSE_STATUS.ERR,
  errorMessage: 'This adress is invalid, please enter an valid adress'
}

import { RESPONSE_STATUS } from '../../utils/constants'
export const success = {
  ch3_s1_field1Section_05: "Zelinslii",
  ch3_s1_field2Section_05: "28005",
  ch3_s1_field3Section_05: "Chisinau",
  ch3_s1_field4Section_05: "Germany",
  status: RESPONSE_STATUS.OK
}

export const error = {
  ch3_s1_field1Section_05: "Zelinslii",
  ch3_s1_field2Section_05: "28005",
  ch3_s1_field3Section_05: "Chisinau",
  ch3_s1_field4Section_05: "Germany",
  status: RESPONSE_STATUS.ERR,
}

/**
 *
 * Use ES5 style for webpack.config.dev.js
 * */

const MOCK_REMOTE_ACTIONS = require('./mock-actions').MOCK_REMOTE_ACTIONS

module.exports = {
  remoteActions: {
    getDictionaryMetadata: MOCK_REMOTE_ACTIONS.getDictionaryMetadata,
    getApplicationDictionaryMetadata: MOCK_REMOTE_ACTIONS.getApplicationDictionaryMetadata,
    getFieldsMetadata: MOCK_REMOTE_ACTIONS.getFieldsMetadata,
    getProducts: MOCK_REMOTE_ACTIONS.getProducts,
    getQProducts: MOCK_REMOTE_ACTIONS.getQProducts,
    getDiscount: undefined,
    recalculatePrice: undefined,
    submitEmailGDPR: undefined,
    getQuote: MOCK_REMOTE_ACTIONS.getQuote,
    acceptQuote: MOCK_REMOTE_ACTIONS.acceptQuote,
    validateDiscount: MOCK_REMOTE_ACTIONS.validateDiscount,
    applyDiscount: MOCK_REMOTE_ACTIONS.applyDiscount,

    getAppFormMetadata: MOCK_REMOTE_ACTIONS.getAppFormMetadata,
    updateCommercialsTC: MOCK_REMOTE_ACTIONS.updateCommercialsTC,
    validateForm: MOCK_REMOTE_ACTIONS.validateForm,
    saveAndClose: MOCK_REMOTE_ACTIONS.saveAndClose,
    submitForm: MOCK_REMOTE_ACTIONS.submitForm,
    verifyAddress: MOCK_REMOTE_ACTIONS.verifyAddress,
    verifyIBAN: MOCK_REMOTE_ACTIONS.verifyIBAN,
    getSessionId: MOCK_REMOTE_ACTIONS.getSessionId,
    getIntegrationStatus: MOCK_REMOTE_ACTIONS.getIntegrationStatus
  },
  lang: 'en_US',
  statePageSetting: 1,
  isInternalUser: false,
  XHRs: {
    TAD: {
      en_US: 'https://concardis--smejoindev--c.cs109.visual.force.com/resource/1538657587000/POS_Service',
      de: 'https://concardis--smejoindev--c.cs109.visual.force.com/resource/1538657587000/POS_Service'
    }
  },
  resources: {
    TAD: {
      en_US: '/tac.html',
      de: '/tac.html'
    },
    imgs: {
      receipt: 'https://concardis--smejoindev--c.cs109.visual.force.com/resource/1538637322000/ReceiptPreview',
      product: 'https://concardis--smejoindev--c.cs109.visual.force.com/resource/1540466199000/ImageProduct',
      icons: 'https://concardis--smejoindev--c.cs109.visual.force.com/resource/1544442855000/digitalonboardingMedia'
    }
  }
}

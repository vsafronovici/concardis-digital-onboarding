/**
 *
 * Use ES5 style for webpack.config.dev.js
 * */

function createNamespace(prefix, keys) {
  return Object.keys(keys).reduce(
    function(newKeys, key) {
      newKeys[key] = prefix + '.' + key
      return newKeys
    }, {}
  )
}

module.exports = {
  MOCK_REMOTE_ACTIONS: createNamespace('MOCK_REMOTE_ACTIONS', {
    getDictionaryMetadata: undefined,
    getFieldsMetadata: undefined,
    getProducts: undefined,
    getDiscount: undefined,
    recalculatePrice: undefined,
    submitEmailGDPR: undefined,
    getQuote: undefined,
    acceptQuote: undefined,
    validateDiscount: undefined,
    applyDiscount: undefined,
    getQProducts: undefined,

    getApplicationDictionaryMetadata: undefined,
    getAppFormMetadata: undefined,
    updateCommercialsTC: undefined,
    validateForm: undefined,
    saveAndClose: undefined,
    submitForm: undefined,
    verifyAddress: undefined,
    verifyIBAN: undefined,
    getSessionId: undefined,
    getIntegrationStatus: undefined
  })
}
